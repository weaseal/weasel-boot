# Weasel-Boot

Weasel-Boot是一个基于Spring Boot的中后台管理系统，集成了国内很多好的第三方中间件、工具和类库。它提供了一套完整的解决方案，帮助开发者快速构建中后台管理系统。

## 技术栈

- [Groovy](https://spring.io/projects/spring-boot) 基于 Java 虚拟机的敏捷动态语言。语法简洁，无缝集成 所有已经存在的 Java 对象和类库。
- [Spring Boot](https://spring.io/projects/spring-boot)
- [Spring Cloud](https://spring.io/projects/spring-cloud)
- [Spring Cloud Alibaba](https://spring.io/projects/spring-cloud-alibaba)
- [Nacos](https://nacos.io/zh-cn/) 一个更易于构建云原生应用的动态服务发现、配置管理和服务管理平台。
- [Undertow](https://undertow.io/) 灵活性能的 Web 服务器，提供基于 NIO 的阻塞和非阻塞 API。用于替代Tomcat。
- [Lombok](https://undertow.io/) 以简单的注解形式来简化java代码，提高开发人员的开发效率。
- [Redis](https://redis.io/) JetCache是一个基于Java的缓存系统封装，提供统一的API和注解来简化缓存的使用。 JetCache提供了比SpringCache更加强大的注解，可以原生的支持TTL、两级缓存、分布式自动刷新，还提供了Cache接口用于手工缓存操作。 当前有四个实现，RedisCache、TairCache（此部分未在github开源）、CaffeineCache(in memory)和一个简易的LinkedHashMapCache(in memory)，要添加新的实现也是非常简单的。
- [JetCache](https://github.com/alibaba/jetcache)
- [MySQL](https://www.mysql.com/cn/)/[PostgreSQL](https://www.postgresql.org/)
- [Druid](https://github.com/alibaba/druid/wiki) Druid 是一个 JDBC 组件库，包含数据库连接池、SQL Parser 等组件。
- [MyBatis-Plus](https://www.baomidou.com/) MyBatis (opens new window)的增强工具，在 MyBatis 的基础上只做增强不做改变，为简化开发、提高效率而生。
- [Dynamic-Datasource](https://www.baomidou.com/) 基于springboot的快速集成多数据源的启动器。
- [ShardingSphere](https://shardingsphere.apache.org/document/current/cn/overview/) 一款分布式的数据库生态系统，可以将任意数据库转换为分布式数据库，并通过数据分片、弹性伸缩、加密等能力对原有数据库进行增强。
- [Stream-Query](https://dromara.gitee.io/stream-query/#/docs/get-started) 允许完全摆脱Mapper的mybatis-plus体验！，可以使用类似“工具类”这样的静态函数进行数据库操作。
- [Mybatis-Plus-Ext](https://www.yuque.com/dontang/codewiki) MPE对MP做了进一步的拓展封装，即保留MP原功能，又添加更多有用便捷的功能。增强功能具体体现在几个方面：自动建表（仅支持mysql）、数据自动填充（类似JPA中的审计）、关联查询（类似sql中的join）、冗余数据自动更新、动态条件等功能做了补充完善。
- [Mybatisplus-Plus](https://gitee.com/jeffreyning/mybatisplus-plus) mybatisplus-plus对mybatisplus的一些功能补充：根据多个字段联合主键增删改查、根据多个字段联合主键增删改查、自动填充优化功能 & 自动扫描Entity类构建ResultMap功能
- [Mybatis-Plus-Join](https://mybatisplusjoin.com/) MyBatis Plus多表联查
- [Mybatis-SqlHelper](https://gitee.com/bigsheller/mybatis-sqlhelper) MyBatis 多租户、逻辑删除、数据权限插件、多数据源
- [Bean-Searcher](https://bs.zhxu.cn/guide/latest/introduction.html) 一个轻量级 数据库 条件检索引擎，它的作用是从已有的数据库表中检索数据，它的目的是为了减少后端模板代码的开发，极大提高开发效率，节省开发时间，使得一行代码完成一个列表查询接口成为可能！
- [SensitiveBye](https://gitee.com/eternalstone/SensitiveBye) 专注于解决数据脱敏的Java和SpringBoot工具包, 能帮助您快速解决项目中的脱敏需求，支持对象字段，接口字段，数据库字段脱敏，json序列化脱敏，日志打印脱敏、敏感词条脱敏、Spring配置文件脱敏等功能。
- [Dict-Mapper](https://gitee.com/power4j/DictMapper) 用于前后端统一维护数据字典。
- [Easy-Trans](http://easy-trans.fhs-opensource.top/) 一款用于做数据翻译的代码辅助插件，利用MyBatis Plus/JPA/BeetlSQL 等ORM框架的能力自动查表，让开发者可以快速的把ID/字典码 翻译为前端需要展示的数据。
- [Sa-Token](https://sa-token.cc/doc.html#/) 轻量级 Java 权限认证框架，主要解决：登录认证、权限认证、单点登录、OAuth2.0、分布式Session会话、微服务网关鉴权 等一系列权限相关问题。
- [Hutool](https://doc.hutool.cn/pages/index/) Hutool是一个小而全的Java工具类库，通过静态方法封装，降低相关API的学习成本，提高工作效率，使Java拥有函数式语言般的优雅，让Java语言也可以“甜甜的”。
- [Mica](https://www.dreamlu.net) Spring Cloud 微服务开发核心工具集。基础工具类、验证码、http、redis、ip2region、xss 等组件开箱即用。
[//]: # (- [Yue-Library]&#40;https://ylyue.cn/#/README&#41; 基于SpringBoot封装的增强库，提供丰富的Java工具类库、优越的ORM框架、优雅的业务封装、优化的Spring环境配置、完善的规约限制、配套的代码生成平台、安稳贴切的开源架构方案等，只为打造更好的JavaWeb开发环境，提升大家的开发质量与效率，降低企业研发成本。)
- [Jap](https://justauth.plus/guide/) 基于 JustAuth 研发的升级产品或者称为专业级产品，JustAuth 解决的是第三方登录的问题，JAP 解决的是整个登录相关的问题。
- [Tianai-Captcha](http://doc.captcha.tianai.cloud/) 可能是java界最好的开源行为验证码 [滑块验证码、点选验证码、行为验证码、旋转验证码， 滑动验证码]。
- [Springdoc/Knife4j](https://doc.xiaominfo.com/docs/quick-start) Knife4j是一个集Swagger2 和 OpenAPI3为一体的增强解决方案。
- [Magic-Api](https://www.ssssssss.org/magic-api/pages/quick/intro/) 基于Java的接口快速开发框架，通过magic-api提供的UI界面完成编写接口，无需定义Controller、Service、Dao、Mapper、XML、VO等Java对象即可完成常见的HTTP API接口开发。
- [EasyExcel](https://easyexcel.opensource.alibaba.com/docs/current/) 快速、简洁、解决大文件内存溢出的java处理Excel工具。
- [X-File-Storage](https://x-file-storage.dromara.org) 在 SpringBoot 中通过简单的方式将文件存储到 本地、FTP、SFTP、WebDAV、谷歌云存储、阿里云OSS、华为云OBS、七牛云Kodo、腾讯云COS、百度云 BOS、又拍云USS、MinIO、 AWS S3、金山云 KS3、美团云 MSS、京东云 OSS、天翼云 OOS、移动云 EOS、沃云 OSS、 网易数帆 NOS、Ucloud US3、青云 QingStor、平安云 OBS、首云 OSS、IBM COS、其它兼容 S3 协议的平台。
- [Mzt-Biz-Log](https://github.com/mouzt/mzt-biz-log) 基于注解的可使用变量、可以自定义函数的通用操作日志组件。
- [Ballcat-Websocket](http://www.ballcat.cn/guide/feature/websocket.html#websocket-%E9%9B%86%E7%BE%A4) WebSocket集群方案。
- [Plumelog-Lite](https://gitee.com/plumeorg/plumelog/blob/master/plumelog-lite/README.md) 一个简单易用的java日志系统，解放你的日志查询困难问题，方便快速追踪问题，安装配置简单，性能优秀。
- [Dinger](https://answerail.gitee.io/docsify-jaemon/#/docs/Dinger-Home) SpringBoot集成钉钉/企业微信/飞书群机器人实现消息通知中间件。
- [Sms4j](https://wind.kim/doc/start/) 短信聚合框架。
- [Lock4j](https://gitee.com/baomidou/lock4j) 基于Spring AOP 的声明式和编程式分布式锁，支持RedisTemplate、Redisson、Zookeeper。
- [Mapstruct-Plus](https://mapstruct.plus/introduction/) Mapstruct增强版，一个注解即可实现自动转换，且具有多种增强特性。
- [Beetl](https://www.kancloud.cn/xiandafu/beetl3_guide/1992542) java模板引擎，具有功能齐全，语法直观,性能超高，以及编写的模板容易维护等特点。
- [Dynamic-Tp](https://dynamictp.cn/guide/use/quick-start.html) 轻量级动态线程池，内置监控告警功能，基于主流配置中心（已支持Nacos、Apollo、ZK，可通过SPI自定义实现）。
- [Vue-Vben-Admin](https://github.com/vbenjs/vue-vben-admin) 高颜值antdv中后台管理脚手架。

## 亮点

- 集成了优秀、易用的第三方中间件、工具和类库，减少了开发者的工作量。
- 项目配置支持本地文件和远程配置中心两种方式。
- 只需定义持久化对象，即可自动生成数据库表和dao层。
- 基于Groovy的动态语言特性编写的通用控制器，单表CRUD的Restful接口可以不用编码。
- 支持接口响应自定义格式。
- 基于JetCache的两级缓存特性，提升应用程序性能。
- 支持多租户。同时支持多租户功能一键开启/关闭，支持特权租户。
- 使用MyBatis作为ORM框架，并集成了相关的扩展库。基于扩展库，支持单表查询、多表联查、多数据源、多租户、逻辑删除、自动建表、动态封装查询条件等功能。
- 支持数据脱敏、加解密、码值翻译。
- 基于流式查询和多线程/虚拟线程，提升大数据量数据导出、导入效率。
- 集成了Sa-Token作为安全框架，提供了完善的权限控制功能，并支持在线用户查看、强退/封禁用户。
- 支持WebSocket集群，系统通知/站内信必达。
- 使用Lock4j，防止重复提交。
- 集成了Springdoc/Knife4j，方便接口文档的管理和测试。
- 支持通过界面新建restful接口。
- 统一的云厂商对象存储接口。
- 统一的云厂商短信发送接口。
- 统一、灵活的操作日志处理，可持久化至数据库、缓存、日志服务。
- 使用Druid作为数据库连接池，提高了数据库的性能和安全性。
- 使用Plumelog日志框架，提供了更好的日志管理和性能。
- 灵活的动态线程池配置，内置监控告警功能，线程池异常早知道。
- 系统异常及时推送到钉钉/企业微信/飞书群，有助于及时排查线上问题。
- 紧随依赖最新稳定版，及时更新。

## 使用说明

1. 克隆项目到本地。

   ```
   git clone https://github.com/weaselveehuang/weasel-boot.git
   ```

2. 导入项目到IDE中。

   ```
   File -> Open -> 选择项目文件夹
   ```

3. 配置数据库。

   ```
   打开src/main/resources/application.yml文件，修改数据库配置。
   ```

4. 启动项目。

   ```
   运行WeaselBootApplication类的main方法。
   ```

5. 访问系统。

   ```
   打开浏览器，输入http://localhost:3000，进入系统首页。
   ```

6. 登录系统。

   ```
   用户名：admin
   密码：123456
   ```

7. 使用系统。

   ```
   系统提供了完善的功能模块，包括用户管理、角色管理、权限管理、菜单管理、日志管理等。
   ```

## 参考

- [ruoyi-vue-plus](https://gitee.com/dromara/RuoYi-Vue-Plus)
- [ruoyi-vue-pro](https://gitee.com/zhijiantianya/ruoyi-vue-pro)
- [bootx-platform](https://gitee.com/bootx/bootx-platform)
- [ballcat](https://gitee.com/ballcat-projects/ballcat)
- [helio-boot](https://gitee.com/uncarbon97/helio-boot)
- [Easy-Vben](https://gitee.com/tcc/Easy-Vben)
- [「最强」Lettuce 已支持 Redis6 客户端缓存](https://juejin.cn/post/6859152753651318792)

## 版权声明

本项目采用MIT许可证。
