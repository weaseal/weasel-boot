/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package cloud.tianai.captcha.resource.common.model.dto;

import cloud.tianai.captcha.resource.ResourceProvider;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 改造适配框架自定义配置
 *
 * @Author: 天爱有情
 * @date 2021/8/7 15:15
 * @Description 资源对象
 */
@Data
@NoArgsConstructor
public class Resource {

    /**
     * 数据,传输给 {@link ResourceProvider} 的参数
     */
    public String data;
    /**
     * 类型.
     */
    private String type;
    /**
     * 标签.
     */
    private String tag;

    public Resource(String type, String data) {
        this.type = type;
        this.data = data;
    }

    public Resource(String type, String data, String tag) {
        this.type = type;
        this.data = data;
        this.tag = tag;
    }

}
