/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel


import org.springframework.boot.SpringApplication

//import com.github.jaemon.dinger.core.annatations.DingerScan
//import com.github.jaemon.dinger.core.entity.enums.DingerType
//import com.github.jaemon.dinger.multi.annotations.EnableMultiDinger
//import com.github.jaemon.dinger.multi.annotations.MultiDinger
//import com.weasel.config.DingTalkMultiHandler

import org.springframework.boot.autoconfigure.SpringBootApplication

import java.lang.management.ManagementFactory
import java.lang.management.RuntimeMXBean

@SpringBootApplication
//@MapperScan(basePackages = ['com.weasel.modules.sys.mapper', 'com.weasel.modules.dev.mapper', 'com.weasel.modules.sms.mapper'])
//@MapperScan(annotationClass = Mapper.class)
//@EnableChatbot
//@EnableGlobalRestHandlers
//@EnableCaching
//@DingerScan(basePackages = "com.weasel.notify")
//@EnableMultiDinger(
//        // 启用钉钉多机器人配置
//        [@MultiDinger(
//                dinger = DingerType.DINGTALK,
//                handler = DingTalkMultiHandler
//        )/*, @MultiDinger(
//                dinger = DingerType.WETALK,
//                handler = WeTalkMultiHandler.class
//        ), @MultiDinger(
//                dinger = DingerType.BYTETALK,
//                handler = ByteTalkMultiHandler
//        )*/]
//)
class WeaselBootApplication {

    static void main(String[] args) {
        RuntimeMXBean runtime = ManagementFactory.getRuntimeMXBean();

        System.out.println("JVM名称：" + runtime.getName());
        System.out.println("JVM版本号：" + runtime.getVmVersion());
        System.out.println("JVM供应商：" + runtime.getVmVendor());
        System.out.println("JVM参数：" + runtime.getInputArguments().toString());
        SpringApplication.run(WeaselBootApplication, args)
//        System.setProperty('mpw.key', '77ff28899d71dd9e')
        ExpandoMetaClass.enableGlobally()
    }

}
