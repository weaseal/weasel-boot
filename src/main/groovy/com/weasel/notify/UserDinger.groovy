/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.notify


//package com.weasel.notify
//
//
//import com.github.jaemon.dinger.core.annatations.DingerText
//import com.github.jaemon.dinger.core.annatations.Parameter
//import com.github.jaemon.dinger.core.entity.DingerResponse
//import com.github.jaemon.dinger.core.entity.enums.DingerType
//import com.github.jaemon.dinger.multi.annotations.MultiDinger
//import com.github.jaemon.dinger.multi.annotations.MultiHandler
//import com.weasel.config.DingTalkMultiHandler
//
//// 为UserDinger配置多机器人功能
//@MultiHandler(
//        @MultiDinger(
//                dinger = DingerType.DINGTALK,
//                handler = DingTalkMultiHandler.class)
//)
//public interface UserDinger {
//    @DingerText(value = "恭喜用户登录成功!")
//    DingerResponse success(@Parameter("loginName") String userName);
//
//    //@DingerMarkdown(
//    //        value = "#### 用户登录通知\n - 用户Id： ${userId}\n - 用户名： ${userName}",
//    //        title = "用户登录反馈"
//    //)
//    //DingerResponse failed(long userId, String userName);
//}
