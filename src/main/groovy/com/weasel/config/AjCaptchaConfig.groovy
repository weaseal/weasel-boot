package com.weasel.config
/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

//package com.weasel.config
//
//
//import com.anji.captcha.service.CaptchaCacheService
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.context.annotation.Bean
//import org.springframework.context.annotation.Configuration
//import org.springframework.data.redis.core.StringRedisTemplate
//
//@Configuration
//class AjCaptchaConfig {
//
//    @Autowired
//    private StringRedisTemplate stringRedisTemplate
//
//    @Bean
//    CaptchaCacheService ajCaptchaRedisCacheService() {
//        new CaptchaCacheService() {
//
//            @Override
//            String type() {
//                return "redis"
//            }
//
//            @Override
//            void set(String key, String value, long expiresInSeconds) {
//                stringRedisTemplate.opsForValue().set(key, value, expiresInSeconds, TimeUnit.SECONDS)
//            }
//
//            @Override
//            boolean exists(String key) {
//                return stringRedisTemplate.hasKey(key)
//            }
//
//            @Override
//            void delete(String key) {
//                stringRedisTemplate.delete(key)
//            }
//
//            @Override
//            String get(String key) {
//                return stringRedisTemplate.opsForValue().get(key)
//            }
//
//            @Override
//            Long increment(String key, long val) {
//                return stringRedisTemplate.opsForValue().increment(key, val)
//            }
//        }
//    }
//}
