/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config

import com.mybatisflex.core.FlexGlobalConfig
import com.mybatisflex.core.datasource.DataSourceKey
import com.mybatisflex.core.datasource.FlexDataSource
import com.mybatisflex.core.row.Db
import com.zaxxer.hikari.HikariDataSource
import groovy.util.logging.Slf4j
import org.dromara.hutool.core.data.id.IdUtil
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Configuration
import org.springframework.context.event.ContextRefreshedEvent

import javax.annotation.Resource

/**
 * @author weasel
 * @version 1.0
 * @date 2022/4/20 11:01
 */
@Slf4j
@Configuration
@ConditionalOnProperty(prefix = 'weasel.dynamic-data-source', name = 'load-from-jdbc', havingValue = 'true')
class DynamicDataSourceConfig implements ApplicationListener<ContextRefreshedEvent> {
    @Resource
    WeaselProperties weaselProperties

    @Override
    void onApplicationEvent(ContextRefreshedEvent event) {
        FlexDataSource flexDataSource = FlexGlobalConfig.defaultConfig.dataSource
        def dynamicDataSource = weaselProperties.dynamicDataSource
        if (dynamicDataSource.loadFromPrimary) {
            DataSourceKey.use(flexDataSource.defaultDataSourceKey)
            Db.selectListBySql(dynamicDataSource.sql).each {
                Map result = it.toCamelKeysMap();
                def hikariDataSource = new HikariDataSource(driverClassName: result.driverClassName, jdbcUrl: result.url, username: result.username, password: result.password)
                flexDataSource.addDataSource(result.name, hikariDataSource);
                log.info("FlexDataSource add datasource success: ${result.name}")
            }
        } else {
            // todo 待测试
            // 获取动态数据源的数据源，临时添加到FlexDataSource，用完即删
            def tempDatasourceName = "temp${IdUtil.fastSimpleUUID()}"
            try {
                def datasource = new HikariDataSource(dynamicDataSource.datasource)
                flexDataSource.addDataSource(tempDatasourceName, datasource);
                DataSourceKey.use(tempDatasourceName)
                Db.selectListBySql(dynamicDataSource.sql).each {
                    Map result = it.toCamelKeysMap();
                    def hikariDataSource = new HikariDataSource(driverClassName: result.driverClassName, jdbcUrl: result.url, username: result.username, password: result.password)
                    flexDataSource.addDataSource(result.name, hikariDataSource);
                }
            } finally {
                flexDataSource.removeDatasource(tempDatasourceName)
            }
        }
    }
}
