/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config

import cn.dev33.satoken.context.SaHolder
import cn.dev33.satoken.context.model.SaRequest
import cn.dev33.satoken.filter.SaServletFilter
import cn.dev33.satoken.fun.SaParamFunction
import cn.dev33.satoken.interceptor.SaInterceptor
import cn.dev33.satoken.oauth2.config.SaOAuth2Config
import cn.dev33.satoken.oauth2.logic.SaOAuth2Template
import cn.dev33.satoken.oauth2.model.SaClientModel
import cn.dev33.satoken.router.SaRouter
import cn.dev33.satoken.router.SaRouterStaff
import cn.dev33.satoken.stp.StpUtil
import cn.dev33.satoken.util.SaResult
import com.weasel.common.enums.DefaultRole
import org.dromara.hutool.core.text.StrUtil
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.view.RedirectView

import javax.annotation.Resource

@Configuration
class SaTokenConfig implements WebMvcConfigurer {
    @Resource
    WeaselProperties weaselProperties

    def auth = {
        /* 当代码权限校验注解的方法对应的path与配置中excludePath匹配时，遵循注解有限原则
           如某方法注解@SaCheckLogin @RequestMapping('cc.html')，excludePath包含/*.html时
        */
        def staff = SaRouter.match("/**")
        // 将路由拦截鉴权动态化，将调用 excludePaths() 的时机放在函数里 参考https://sa-token.cc/doc.html#/fun/dynamic-router-check
                .notMatch(weaselProperties.security.excludePath)
                .check({ r -> StpUtil.checkLogin() } as SaParamFunction<SaRouterStaff>)
        // 超级管理员拥有最高权限，不进行权限过滤
                .notMatch(StpUtil.hasRole(DefaultRole.SUPER_ADMIN.value))

        // DynamicController动态添加权限控制
        String dynamicRouterPrefix = weaselProperties.dynamicRouterPrefix
        dynamicRouterPrefix = '/' + (dynamicRouterPrefix - '/')
        SaRequest req = SaHolder.getRequest()
        String requestPath = req.getRequestPath()
        if (requestPath.startsWith(dynamicRouterPrefix)) {
            def (_, table) = StrUtil.removePrefix(requestPath, dynamicRouterPrefix).split('/')
            String path = '/' + (table - '/')

            // 操作权限。验证方式：配置非超管角色菜单权限，并以该角色下的账号登录执行相应操作
            // 统一处理，无须再在controller上加权限注解
            def checker = [:]
            // 某些环境不支持GET和POST以外的请求方式，所以这里将PUT和DELETE改为POST
            checker[["${dynamicRouterPrefix}${path}/list"]] = ["${path}/list"]
            checker[["${dynamicRouterPrefix}${path}/{id}"]] = ["${path}/{id}===INLINE"]
            checker[["${dynamicRouterPrefix}${path}/save"]] = ["${path}/save===TOOLBAR"]
            checker[["${dynamicRouterPrefix}${path}/saveBatch"]] = ["${path}/saveBatch===TOOLBAR"]
            checker[["${dynamicRouterPrefix}${path}/update"]] = ["${path}/update===INLINE"]
            checker[["${dynamicRouterPrefix}${path}/remove"]] = ["${path}/remove===INLINE"]
            checker[["${dynamicRouterPrefix}${path}/removeBatch"]] = ["${path}/removeBatch===TOOLBAR"]
            checker.each { k, v ->
                k.each {
                    staff.match(it)
                }
                staff.check({ r -> StpUtil.checkPermissionOr(v as String[]) } as SaParamFunction<SaRouterStaff>).stop()
            }
        }
    }

    // 注册Sa-Token的注解拦截器，打开注解式鉴权功能
    @Override
    void addInterceptors(InterceptorRegistry registry) {
        // 注册Sa-Token的路由拦截器
        def saInterceptor = new SaInterceptor()
        !weaselProperties.debug && saInterceptor.setAuth(auth as SaParamFunction<Object>)
        registry.addInterceptor saInterceptor addPathPatterns("/**") excludePathPatterns(weaselProperties.security.excludePath)
    }

    /**
     * 当前未验证出以下问题，存疑？
     * 注册 [Sa-Token全局过滤器]
     * 拦截器机制某些情况下无法自定义处理异常，无法进入全局@ExceptionHandler，改用过滤器setError处理
     * 演示步骤：注释掉本方法，打开类声明上的implements WebMvcConfigurer，未登录状态下访问一个需要登录验证的url
     */
//    @Bean
    SaServletFilter getSaServletFilter() {
        def filter = new SaServletFilter()
//        !weaselProperties.debug && filter.setAuth(auth)
        if (!weaselProperties.debug) {
            filter
                // 指定 拦截路由 与 放行路由
                .addInclude("/**").setExcludeList(weaselProperties.security.excludePath as List)    /* 排除掉 /favicon.ico */
                // 认证函数: 每次请求执行
                .setAuth(auth)
                // 异常处理函数：每次认证函数发生异常时执行此函数
                .setError(e -> {
                    System.out.println("---------- 进入Sa-Token异常处理 -----------")
//                        e.printStackTrace()
                    throw e
//                        return SaResult.error(e.getMessage())
                })
            // 前置函数：在每次认证函数之前执行（BeforeAuth 不受 includeList 与 excludeList 的限制，所有请求都会进入）
                    .setBeforeAuth(r -> {
                        // ---------- 设置一些安全响应头 ----------
                        SaHolder.getResponse()
                        // 服务器名称
                                .setServer("sa-server")
                        // 是否可以在iframe显示视图： DENY=不可以 | SAMEORIGIN=同域下可以 | ALLOW-FROM uri=指定域名下可以
                                .setHeader("X-Frame-Options", "SAMEORIGIN")
                        // 是否启用浏览器默认XSS防护： 0=禁用 | 1=启用 | 1; mode=block 启用, 并在检查到XSS攻击时，停止渲染页面
                                .setHeader("X-XSS-Protection", "1; mode=block")
                        // 禁用浏览器内容嗅探
                                .setHeader("X-Content-Type-Options", "nosniff")

                    })
        }
        filter
    }

    @Bean
    SaOAuth2Template saOAuth2Template() {
        return new SaOAuth2Template() {
            // 根据 id 获取 Client 信息
            @Override
            SaClientModel getClientModel(String clientId) {
                // 此为模拟数据，真实环境需要从数据库查询
                if ("1001".equals(clientId)) {
                    return new SaClientModel()
                            .setClientId("1001")
                            .setClientSecret("aaaa-bbbb-cccc-dddd-eeee")
                            .setAllowUrl("*")
                            .setContractScope("userinfo")
                            .setIsAutoMode(true)
                }
                return null
            }

            // 根据ClientId 和 LoginId 获取openid
            @Override
            String getOpenid(String clientId, Object loginId) {
                // 此为模拟数据，真实环境需要从数据库查询
                return "gr_SwoIN0MC1ewxHX_vfCW3BothWDZMMtx__"
            }
        }
    }

    @Bean
    SaOAuth2Config saOAuth2Config() {
        return new SaOAuth2Config().
//                // 未登录的视图
//                        setNotLoginView(()->{
//                    return new ModelAndView("login.html");
//                }).
//                // 登录处理函数
//                        setDoLoginHandle((name, pwd) -> {
//                    if("sa".equals(name) && "123456".equals(pwd)) {
//                        StpUtil.login(10001);
//                        return SaResult.ok();
//                    }
//                    return SaResult.error("账号名或密码错误");
//                }).
//                // 授权确认视图
//                        setConfirmView((clientId, scope)->{
//                    Map<String, Object> map = new HashMap<>();
//                    map.put("clientId", clientId);
//                    map.put("scope", scope);
//                    return new ModelAndView("confirm.html", map);
//                })
// 配置：未登录时返回的View
        setNotLoginView(() -> {
//                    '''当前会话在OAuth-Server端尚未登录，请先访问
//                        <a href='/oauth2/doLogin?name=sa&pwd=123456' target='_blank'> doLogin登录 </a>
//                        进行登录之后，刷新页面开始授权'''
    return new RedirectView('http://localhost:7816')
}).
                // 配置：登录处理函数
                        setDoLoginHandle((name, pwd) -> {
                    if ("sa".equals(name) && "123456".equals(pwd)) {
                        StpUtil.login(10001);
                        return SaResult.ok();
                    }
                    return SaResult.error("账号名或密码错误");
                }).
                // 配置：确认授权时返回的View
                        setConfirmView((clientId, scope) -> {
                    """<p>应用 ${clientId} 请求授权： ${scope} </p>
                    <p>请确认：<a href='/oauth2/doConfirm?client_id=${clientId}&scope=${scope}' target='_blank'> 确认授权 </a></p>
                    <p>确认之后刷新页面</p>
                    """
                })
    }
}
