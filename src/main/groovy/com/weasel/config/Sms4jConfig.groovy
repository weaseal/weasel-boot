/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config

import com.alibaba.fastjson2.JSONObject
import com.weasel.modules.sms.mapper.SmsConfigInfoMapper
import org.dromara.hutool.core.bean.BeanUtil
import org.dromara.hutool.core.reflect.ClassUtil
import org.dromara.sms4j.core.datainterface.SmsReadConfig
import org.dromara.sms4j.provider.config.BaseConfig
import org.springframework.beans.factory.InitializingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import javax.annotation.Resource

@Configuration
class Sms4jConfig implements InitializingBean {
    @Resource
    SmsConfigInfoMapper smsConfigInfoMapper
    Map<String, BaseConfig> configMap = [:]

    @Override
    void afterPropertiesSet() throws Exception {
        //获取所有的模型实现类
        def classes = ClassUtil.scanPackageBySuper("org.dromara.sms4j", BaseConfig.class);
        for (Class<?> aClass : classes) {
            //实例化
            Object o = aClass.newInstance()
            if (o instanceof BaseConfig) {
                configMap[o.supplier] = o
            }
        }
    }

    @Bean
    SmsReadConfig smsReadConfig() {
        new SmsReadConfig() {

            @Override
            BaseConfig getSupplierConfig(String s) {
                configMap[s]
            }

            @Override
            List<BaseConfig> getSupplierConfigList() {
                List<BaseConfig> configs = []
                smsConfigInfoMapper.selectList(null).each {
//        Db.list(SmsConfigInfo).each {
                    def config = configMap[it.supplier.toLowerCase()]
                    BeanUtil.copyProperties(JSONObject.parseObject(it.content), config, true)
                    configs << config
                }
                configs
            }
        }
    }

//    @Override
//    BaseConfig getSupplierConfig(String s) {
//        configMap[s]
//    }
//
//    @Override
//    List<BaseConfig> getSupplierConfigList() {
//        List<BaseConfig> configs = []
//        smsConfigInfoMapper.selectList(null).each {
////        Db.list(SmsConfigInfo).each {
//            def config = configMap[it.supplier.toLowerCase()]
//            BeanUtil.copyProperties(JSONObject.parseObject(it.content), config, true)
//            configs << config
//        }
//        configs
//    }
}
