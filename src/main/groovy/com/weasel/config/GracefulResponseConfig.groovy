/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config

import com.fasterxml.jackson.annotation.JsonIgnore
import com.feiniaojin.gracefulresponse.AbstractExceptionAliasRegisterConfig
import com.feiniaojin.gracefulresponse.EnableGracefulResponse
import com.feiniaojin.gracefulresponse.ExceptionAliasRegister
import com.feiniaojin.gracefulresponse.GracefulResponseProperties
import com.feiniaojin.gracefulresponse.api.ResponseFactory
import com.feiniaojin.gracefulresponse.data.Response
import com.feiniaojin.gracefulresponse.data.ResponseStatus
import com.feiniaojin.gracefulresponse.defaults.DefaultResponseFactory
import com.weasel.common.base.exception.UnauthorizedException
import com.yomahub.tlog.context.TLogContext
import com.yomahub.tlog.springboot.property.TLogProperty
import org.dromara.hutool.extra.spring.SpringUtil
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

import java.time.LocalDateTime

@Configuration
@EnableGracefulResponse
class GracefulResponseConfig extends AbstractExceptionAliasRegisterConfig {

    @Primary
    @Bean
    ResponseFactory responseFactory(WeaselProperties weaselProperties) {
        def responseWrapperProperties = weaselProperties.responseWrapper
        return new DefaultResponseFactory() {

            @Override
            Response newEmptyInstance() {
                def response = new ResponseImpl(codeField: responseWrapperProperties.codeField, msgField: responseWrapperProperties.msgField, dataField: responseWrapperProperties.dataField, successField: responseWrapperProperties.successField)
                return response
            }

            @Override
            Response newInstance(ResponseStatus responseStatus) {
                return super.newInstance(responseStatus)
            }

            @Override
            Response newInstance(ResponseStatus statusLine, Object data) {
                return super.newInstance(statusLine, data)
            }

            @Override
            Response newSuccessInstance() {
                return super.newSuccessInstance()
            }

            @Override
            Response newSuccessInstance(Object payload) {
                return super.newSuccessInstance(payload)
            }

            @Override
            Response newFailInstance() {
                return super.newFailInstance()
            }
        }
    }

    @Override
    protected void registerAlias(ExceptionAliasRegister register) {
        //注册异常别名
        register.doRegisterExceptionAlias(UnauthorizedException.class)
    }

    static class ResponseImpl extends HashMap<String, Object> implements Response {

        String codeField
        String msgField
        String dataField
        String successField

        // 与tlog的traceId一致
//        String requestId
//
//        LocalDateTime timestamp

        @Override
        void setStatus(ResponseStatus statusLine) {
            put(codeField, statusLine.getCode())
            put(msgField, statusLine.getMsg())
            put(successField, statusLine.getCode() == SpringUtil.getBean(GracefulResponseProperties).defaultSuccessCode)
            put('timestamp', LocalDateTime.now())
            put('requestId', SpringUtil.getBean(TLogProperty).pattern.replace('$spanId', TLogContext.spanId).replace('$traceId', TLogContext.traceId))
        }

        @Override
        @JsonIgnore
        ResponseStatus getStatus() {
            return null
        }

        @Override
        void setPayload(Object payload) {
            put(dataField, payload)
        }

        @Override
        @JsonIgnore
        Object getPayload() {
            return get(dataField)
        }

//        void setRequestId(String requestId) {
//            this.requestId = requestId
//        }
//
//        String getRequestId() {
//            return this.requestId
//        }
    }

}
