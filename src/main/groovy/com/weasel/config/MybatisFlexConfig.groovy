/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config

import cn.dev33.satoken.stp.StpUtil
import com.mybatisflex.annotation.InsertListener
import com.mybatisflex.annotation.KeyType
import com.mybatisflex.annotation.UpdateListener
import com.mybatisflex.core.FlexGlobalConfig
import com.mybatisflex.core.audit.AuditManager
import com.mybatisflex.core.audit.AuditMessage
import com.mybatisflex.core.dialect.IDialect
import com.mybatisflex.core.keygen.KeyGenerators
import com.mybatisflex.core.logicdelete.LogicDeleteProcessor
import com.mybatisflex.core.logicdelete.impl.PrimaryKeyLogicDeleteProcessor
import com.mybatisflex.core.mybatis.FlexConfiguration
import com.mybatisflex.core.query.QueryColumn
import com.mybatisflex.core.query.QueryTable
import com.mybatisflex.core.query.QueryWrapper
import com.mybatisflex.core.table.TableInfo
import com.mybatisflex.core.tenant.TenantFactory
import com.mybatisflex.core.tenant.TenantManager
import com.mybatisflex.spring.boot.MyBatisFlexCustomizer
import com.tangzc.mybatisflex.annotation.FieldFill
import com.tangzc.mybatisflex.annotation.handler.AutoFillHandler
import com.weasel.common.base.entity.BaseEntity
import com.weasel.common.util.AuthUtil
import com.weasel.modules.file.entity.FileDetail
import com.weasel.modules.file.entity.FilePartDetail
import com.weasel.modules.sys.service.SysTenantService
import groovy.util.logging.Slf4j
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Lazy

import javax.annotation.Resource
import java.lang.reflect.Field
import java.time.LocalDateTime

@Slf4j
@Configuration
class MybatisFlexConfig implements MyBatisFlexCustomizer {
    @Resource
    WeaselProperties weaselProperties
    @Resource
    AuthContext authContext
    @Resource
    @Lazy
    SysTenantService sysTenantService

    @Override
    void customize(FlexGlobalConfig flexGlobalConfig) {
        // sql审计、打印
        AuditManager.auditEnable = true
        AuditManager.messageCollector = (AuditMessage auditMessage) ->
                log.info("""
┏━━━━━ SQL [${auditMessage.dsName} | elapsed ${auditMessage.elapsedTime} ms] ━━━
┣ hostIp：${auditMessage.hostIp}
┣ sql：${auditMessage.fullSql}
┗━━━━━ SQL [${auditMessage.dsName} | elapsed ${auditMessage.elapsedTime} ms] ━━━
""")

        // 多租户
//        FlexGlobalConfig.getDefaultConfig().setDefaultRelationQueryDepth(1)
        FlexGlobalConfig.getDefaultConfig().setTenantColumn("tenant_id");
//        FlexGlobalConfig.getDefaultConfig().setKeyConfig(new FlexGlobalConfig.KeyConfig(keyType: KeyType.Generator, value: KeyGenerators.snowFlakeId));
        TenantManager.tenantFactory = new TenantFactory() {
            @Override
            Object[] getTenantIds() {
//                if (authContext.tenantId == weaselProperties.tenant.privilegedTenantId) {
//                    return null
//                }

                return sysTenantService.listChildrenIds(authContext.tenantId) << authContext.tenantId
            }

            @Override
            Object[] getTenantIds(String tableName) {
//                if (authContext.tenantId == weaselProperties.tenant.privilegedTenantId) {
//                    return null
//                }

                return sysTenantService.listChildrenIds(authContext.tenantId) << authContext.tenantId
            }
        }
        // 数据填充
        flexGlobalConfig.registerUpdateListener(new UpdateListener() {
            @Override
            void onUpdate(Object entity) {
                entity = entity as BaseEntity
                entity.updateBy = StpUtil.loginIdAsLong
                entity.updateTime = LocalDateTime.now()
            }
        }, BaseEntity)
        flexGlobalConfig.registerInsertListener(new InsertListener() {
            @Override
            void onInsert(Object entity) {
                entity = entity as BaseEntity
                entity.createBy = StpUtil.loginIdAsLong
                entity.createTime = LocalDateTime.now()
            }
        }, BaseEntity)
    }

    @Bean
    LogicDeleteProcessor logicDeleteProcessor() {
        new PrimaryKeyLogicDeleteProcessor() {
            @Override
            String buildLogicNormalCondition(String logicColumn, TableInfo tableInfo, IDialect dialect) {
                dialect.wrap(logicColumn) + " = ${getLogicNormalValue()}"
            }

            @Override
            void buildQueryCondition(QueryWrapper queryWrapper, TableInfo tableInfo, String joinTableAlias) {
                QueryTable queryTable = new QueryTable(tableInfo.getSchema(), tableInfo.getTableName()).as(joinTableAlias)
                QueryColumn queryColumn = new QueryColumn(queryTable, tableInfo.getLogicDeleteColumn())
                queryWrapper & queryColumn.eq(getLogicNormalValue())
            }

            @Override
            Object getLogicNormalValue() {
                0L
            }
        }
    }

//    @Bean
//    AutoFillHandler autoFillHandler() {
//        new AutoFillHandler() {
//            @Override
//            Object getVal(Object object, Class clazz, Field field) {
//                return 1
//            }
//        }
//    }

    static class WeaselAutoFillHandler implements AutoFillHandler {
        @Override
        Object getVal(Object object, Class clazz, Field field) {
            return 1
        }

    }

    @Bean
    TenantFactory tenantFactory(){
        new TenantFactory() {
            @Override
            Object[] getTenantIds() {
//                if (authContext.tenantId == weaselProperties.tenant.privilegedTenantId) {
//                    return null
//                }

                return sysTenantService.listChildrenIds(authContext.tenantId) << authContext.tenantId
            }

            @Override
            Object[] getTenantIds(String tableName) {
//                if (authContext.tenantId == weaselProperties.tenant.privilegedTenantId) {
//                    return null
//                }

                return sysTenantService.listChildrenIds(authContext.tenantId) << authContext.tenantId
            }
        }
    }
}
