/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config;

import cn.dev33.satoken.stp.StpInterface;
import cn.dev33.satoken.stp.StpUtil;
import com.mybatisflex.core.mybatis.Mappers;
import com.mybatisflex.core.query.QueryWrapper;
import com.weasel.common.consts.Consts;
import com.weasel.common.enums.DataScope;
import com.weasel.common.enums.DefaultRole;
import com.weasel.common.enums.MenuType;
import com.weasel.modules.sys.entity.SysMenu;
import com.weasel.modules.sys.entity.SysRole;
import com.weasel.modules.sys.entity.SysTenant;
import com.weasel.modules.sys.entity.SysUser;
import lombok.RequiredArgsConstructor;
import org.dromara.hutool.core.collection.ListUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.weasel.modules.sys.entity.table.SysMenuTableDef.SYS_MENU;
import static com.weasel.modules.sys.entity.table.SysRoleMenuTableDef.SYS_ROLE_MENU;
import static com.weasel.modules.sys.entity.table.SysRoleTableDef.SYS_ROLE;
import static com.weasel.modules.sys.entity.table.SysUserRoleTableDef.SYS_USER_ROLE;
import static com.weasel.modules.sys.entity.table.SysUserTableDef.SYS_USER;

/**
 * 自定义权限验证接口扩展
 */
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
@Component
public class AuthContext implements StpInterface {
    private final WeaselProperties weaselProperties;

    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        List<String> permissionList;
        if (isSuperAdmin(loginId, loginType)) {
//            sysMenus = sysMenuService.list(
//                    Wrappers.<SysMenu>lambdaQuery()
//                            .ne(SysMenu::getPermission, "")
//                            .isNotNull(SysMenu::getPermission)
//            );
            // 给定超级管理员特定权限码，这样就无需从数据库获取所有权限码，有新功能添加了也不需要配置超级管理员的权限
            permissionList = ListUtil.of(Consts.Auth.PERMIT_ALL);
        } else {
            QueryWrapper queryWrapper = QueryWrapper.create()
                    .select(SYS_MENU.PERMISSION)
                    .from(SYS_MENU)
                    .leftJoin(SYS_ROLE_MENU).on(SYS_ROLE_MENU.MENU_ID.eq(SYS_MENU.ID))
                    .leftJoin(SYS_USER_ROLE).on(SYS_USER_ROLE.ROLE_ID.eq(SYS_ROLE_MENU.ROLE_ID))
                    .leftJoin(SYS_USER).on(SYS_USER.ID.eq(SYS_USER_ROLE.USER_ID))
                    .leftJoin(SYS_ROLE).on(SYS_ROLE.ID.eq(SYS_USER_ROLE.ROLE_ID))
                    .eq(SysUser::getId, loginId)
                    .ne(SysUser::getDisabled, true)
                    .ne(SysRole::getDisabled, true)
                    .eq(SysMenu::getUiPlatform, weaselProperties.getUiPlatform())
                    .ne(SysMenu::getDisabled, true)
                    .ne(SysMenu::getPermission, "")
                    .isNotNull(SysMenu::getPermission);
            permissionList = Mappers.ofEntityClass(SysMenu.class).selectListByQuery(queryWrapper)
                    .stream().filter(Objects::nonNull).map(SysMenu::getPermission).distinct().collect(Collectors.toList());
        }
        if (weaselProperties.tenant.enabled && Objects.equals(getTenantId(), weaselProperties.tenant.privilegedTenantId)) {
            permissionList.add(Consts.Auth.PERMIT_PRIVILEGED_TENANT);
        }
        return permissionList;
    }

    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        QueryWrapper queryWrapper = QueryWrapper.create()
                .select(SYS_ROLE.CODE)
                .from(SYS_ROLE)
                .leftJoin(SYS_USER_ROLE).on(SYS_USER_ROLE.ROLE_ID.eq(SYS_ROLE.ID))
                .leftJoin(SYS_USER).on(SYS_USER_ROLE.USER_ID.eq(SYS_USER.ID))
                .eq(SysUser::getId, loginId);
        List<SysRole> roles = Mappers.ofEntityClass(SysRole.class).selectListByQuery(queryWrapper);
        return roles.stream().map(SysRole::getCode).collect(Collectors.toList());
    }

    /**
     * 返回一个账号所拥有的权限菜单实体集合
     */
    public List<SysMenu> getPermissionMenuList(Object loginId, String loginType) {
        List<SysMenu> sysMenus;
        if (isSuperAdmin(loginId, loginType)) {
            QueryWrapper queryWrapper = QueryWrapper.create()
                    .eq(SysMenu::getUiPlatform, weaselProperties.getUiPlatform())
                    .ne(SysMenu::getPermission, "")
                    .isNotNull(SysMenu::getPermission);
            sysMenus = Mappers.ofEntityClass(SysMenu.class).selectListByQuery(queryWrapper);
        } else {
            QueryWrapper queryWrapper = QueryWrapper.create()
                    .select(SYS_MENU.ALL_COLUMNS)
                    .from(SYS_MENU)
                    .leftJoin(SYS_ROLE_MENU).on(SYS_ROLE_MENU.MENU_ID.eq(SYS_MENU.ID))
                    .leftJoin(SYS_USER_ROLE).on(SYS_USER_ROLE.ROLE_ID.eq(SYS_ROLE_MENU.ROLE_ID))
                    .leftJoin(SYS_USER).on(SYS_USER.ID.eq(SYS_USER_ROLE.USER_ID))
                    .eq(SysUser::getId, loginId)
                    .eq(SysMenu::getUiPlatform, weaselProperties.getUiPlatform())
                    .ne(SysMenu::getPermission, "")
                    .isNotNull(SysMenu::getPermission);
            sysMenus = Mappers.ofEntityClass(SysMenu.class).selectListByQuery(queryWrapper);
        }
        return sysMenus.stream().filter(Objects::nonNull).distinct().collect(Collectors.toList());
    }

    /**
     * 返回数据权限
     */
    public List<DataScope> getDataScopes(Object loginId, String loginType) {
        QueryWrapper queryWrapper = QueryWrapper.create()
                .select(SYS_ROLE.DATA_SCOPE)
                .from(SYS_ROLE)
                .leftJoin(SYS_USER_ROLE).on(SYS_USER_ROLE.ROLE_ID.eq(SYS_ROLE.ID))
                .leftJoin(SYS_USER).on(SYS_USER_ROLE.USER_ID.eq(SYS_USER.ID))
                .eq(SysUser::getId, loginId);
        List<SysRole> roles = Mappers.ofEntityClass(SysRole.class).selectListByQuery(queryWrapper);
        return roles.stream().map(SysRole::getDataScope)
                .collect(ArrayList::new, (list, dataScope) -> list.add(DataScope.ALL), ArrayList::addAll);
    }

    /**
     * 返回数据权限
     */
    public List<DataScope> getDataScopes() {
        return getDataScopes(StpUtil.getLoginId(), StpUtil.getLoginType());
    }

    /**
     * 返回一个账号所拥有的菜单集合
     */
    public List<SysMenu> getMenuList(Object loginId, String loginType) {
        List<SysMenu> sysMenus;
        if (isSuperAdmin(loginId, loginType)) {
            QueryWrapper queryWrapper = QueryWrapper.create()
                    .eq(SysMenu::getUiPlatform, weaselProperties.getUiPlatform())
                    .ne(SysMenu::getType, MenuType.BUTTON);
            sysMenus = Mappers.ofEntityClass(SysMenu.class).selectListByQuery(queryWrapper);
        } else {
            QueryWrapper queryWrapper = QueryWrapper.create()
                    .select(SYS_MENU.ALL_COLUMNS)
                    .from(SYS_MENU)
                    .leftJoin(SYS_ROLE_MENU).on(SYS_ROLE_MENU.MENU_ID.eq(SYS_MENU.ID))
                    .leftJoin(SYS_USER_ROLE).on(SYS_USER_ROLE.ROLE_ID.eq(SYS_ROLE_MENU.ROLE_ID))
                    .leftJoin(SYS_USER).on(SYS_USER.ID.eq(SYS_USER_ROLE.USER_ID))
                    .ne(SysMenu::getType, MenuType.BUTTON)
                    .eq(SysMenu::getUiPlatform, weaselProperties.getUiPlatform())
                    .eq(SysUser::getId, StpUtil.getLoginId())
                    .orderBy(SysMenu::getWeight, true)
                    .groupBy(SysMenu::getId);
            sysMenus = Mappers.ofEntityClass(SysMenu.class).selectListByQuery(queryWrapper);
        }
        return sysMenus.stream().filter(Objects::nonNull).distinct().collect(Collectors.toList());
    }

    /**
     * 返回一个账号所拥有的菜单集合
     */
    public List<SysMenu> getMenuList() {
        return getMenuList(StpUtil.getLoginId(), StpUtil.getLoginType());
    }

    /**
     * 是否超级管理员，超级管理员默认拥有所有权限
     */
    public boolean isSuperAdmin(Object loginId, String loginType) {
        return getRoleList(loginId, loginType).contains(DefaultRole.SUPER_ADMIN.getValue());
    }

    /**
     * 是否超级管理员，超级管理员默认拥有所有权限
     */
    public boolean isSuperAdmin() {
        return isSuperAdmin(StpUtil.getLoginId(), StpUtil.getLoginType());
    }

    public SysTenant getTenant() {
        return StpUtil.getSession().getModel(Consts.Session.CURRENT_TENANT, SysTenant.class);
    }

    public Long getTenantId() {
        return StpUtil.isLogin() ? getTenant().getId() : null;
    }
}
