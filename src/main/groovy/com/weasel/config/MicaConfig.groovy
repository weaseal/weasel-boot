package com.weasel.config
/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

//package com.weasel.config
//
//import groovy.util.logging.Slf4j
//import net.dreamlu.mica.core.constant.MicaConstant
//import net.dreamlu.mica.core.exception.ServiceException
//import net.dreamlu.mica.core.log.LogPrintStream
//import net.dreamlu.mica.core.result.R
//import net.dreamlu.mica.core.result.SystemCode
//import net.dreamlu.mica.core.utils.BeanUtil
//import net.dreamlu.mica.core.utils.StringUtil
//import net.dreamlu.mica.core.utils.SystemUtil
//import net.dreamlu.mica.lite.error.MicaErrorAttributes
//import net.dreamlu.mica.lite.launch.StartedEventListener
//import org.dromara.hutool.core.text.StrUtil
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
//import org.springframework.boot.autoconfigure.web.ServerProperties
//import org.springframework.boot.web.context.WebServerApplicationContext
//import org.springframework.boot.web.context.WebServerInitializedEvent
//import org.springframework.boot.web.error.ErrorAttributeOptions
//import org.springframework.context.annotation.Bean
//import org.springframework.context.annotation.Configuration
//import org.springframework.context.event.EventListener
//import org.springframework.core.Ordered
//import org.springframework.core.annotation.Order
//import org.springframework.core.env.Environment
//import org.springframework.lang.Nullable
//import org.springframework.scheduling.annotation.Async
//import org.springframework.util.ClassUtils
//import org.springframework.util.StringUtils
//import org.springframework.web.context.request.RequestAttributes
//import org.springframework.web.context.request.WebRequest
//import org.ssssssss.magicapi.core.config.MagicAPIProperties
//import org.ssssssss.magicapi.utils.PathUtils
//
//import javax.servlet.RequestDispatcher
//import java.util.stream.Stream
//
//@Slf4j
//@Configuration
//@EnableAutoConfiguration(exclude = StartedEventListener)
//class MicaConfig {
//
//    @Bean
//    MicaErrorAttributes errorAttributes() {
//        return new MicaErrorAttributes() {
//            @Override
//            Map<String, Object> getErrorAttributes(WebRequest webRequest, ErrorAttributeOptions options) {
//                // 请求地址
//                String requestUrl = this.getAttr(webRequest, RequestDispatcher.ERROR_REQUEST_URI)
//                if (StringUtil.isBlank(requestUrl)) {
//                    requestUrl = this.getAttr(webRequest, RequestDispatcher.FORWARD_REQUEST_URI)
//                }
//                // status code
//                Integer status = this.getAttr(webRequest, RequestDispatcher.ERROR_STATUS_CODE)
//                // error
//                Throwable error = getError(webRequest)
//                log.error("URL:{} error status:{}", requestUrl, status, error)
//                R<Object> result
//                if (error instanceof ServiceException) {
//                    result = ((ServiceException) error).getResult()
//                    result = Optional.ofNullable(result).orElse(R.fail(SystemCode.FAILURE))
//                } else {
//                    // 这里调整为打印错误信息 | edit by weasel
//                    //result = R.fail(SystemCode.FAILURE, "System error status:" + status);
//                    result = R.fail(SystemCode.FAILURE, error?.message)
//                }
//                return BeanUtil.toMap(result)
//            }
//
//            @Nullable
//            private <T> T getAttr(WebRequest webRequest, String name) {
//                return (T) webRequest.getAttribute(name, RequestAttributes.SCOPE_REQUEST)
//            }
//        }
//    }
//
//    //@Bean
//    @ConditionalOnProperty(name = "weasel.show-url", havingValue = "true", matchIfMissing = true)
//    StartedEventListener startedEventListener() {
//        return new StartedEventListener() {
//
//            @Autowired
//            private ServerProperties serverProperties
//
//            @Autowired
//            private WeaselProperties weaselProperties
//
//            @Autowired
//            private MagicAPIProperties magicAPIProperties
//
//            @Async
//            @Order(Ordered.LOWEST_PRECEDENCE)
//            @EventListener(WebServerInitializedEvent.class)
//            void afterStart(WebServerInitializedEvent event) {
//                String ip = "IP"
//                try {
//                    ip = InetAddress.localHost.hostAddress
//                } catch (UnknownHostException e) {
//                    println "当前服务地址获取失败"
//                }
//                String magicWebPath = magicAPIProperties.web
//                String schema = "http://"
//                WebServerApplicationContext context = event.getApplicationContext()
//                Environment environment = context.getEnvironment()
//                String appName = environment.getRequiredProperty(MicaConstant.SPRING_APP_NAME_KEY)
//                int port = event.getWebServer().getPort()
//                String contextPath = serverProperties.servlet.contextPath ?: ''
//                String profile = StringUtils.arrayToCommaDelimitedString(environment.getActiveProfiles())
//                System.err.printf("---[%s]---启动完成，当前使用的端口:[%d]，环境变量:[%s]---%n", appName, port, profile)
//                String localUrl = "$schema${PathUtils.replaceSlash("localhost:$port/$contextPath/${magicAPIProperties.prefix ?: ''}/")}"
//                String externUrl = "$schema${PathUtils.replaceSlash("$ip:$port/$contextPath/${magicAPIProperties.prefix ?: ''}/")}"
//                println """********************************************当前服务相关地址********************************************
//服务启动成功! Access URLs:
//    接口本地地址:          $localUrl
//    接口外部地址:          $externUrl"""
//                // 如果有 swagger，打印开发阶段的 swagger ui 地址
//                if (hasOpenApi()) {
//                    println """    接口文档swagger地址:   ${localUrl}swagger-ui/index.html
//    接口文档knife4j地址:   ${localUrl}doc.html"""
//                }
//                // 如果有 plumelog，打印开发阶段的 plumelog 地址
//                if (hasPlumelog()) {
//                    println """    plumelog监控日志地址:  ${localUrl}plumelog/#/"""
//                }
//                // 如果有 druid，打印开发阶段的 druid 地址
//                if (hasDruid()) {
//                    println """    druid数据库监控日志地址:${localUrl}druid/index.html"""
//                }
//                // 如果有 magic-api，打印开发阶段的 magic-api 地址
//                if (hasMagicApi()) {
//                    println """${if (magicWebPath) "    接口配置平台:          ${localUrl}${StrUtil.replaceFirst(magicAPIProperties.web, '/', '', false)}/index.html"}"""
//                }
//                // linux 上将全部的 System.err 和 System.out 替换为log
//                if (SystemUtil.isLinux()) {
//                    System.setOut(LogPrintStream.log(false))
//                    // 去除 error 的转换，因为 error 会打印成很 N 条
//                    // System.setErr(LogPrintStream.log(true));
//                }
//                println """    可通过配置关闭输出:  weasel.show-url=false
//********************************************当前服务相关地址********************************************"""
//            }
//
//            private static boolean hasOpenApi() {
//                return Stream.of("springfox.documentation.spring.web.plugins.Docket", "io.swagger.v3.oas.models.OpenAPI")
//                        .anyMatch(clazz -> ClassUtils.isPresent(clazz, null))
//            }
//
//            private static boolean hasPlumelog() {
//                ClassUtils.isPresent("com.plumelog.lite.autoconfigure.PlumelogLiteConfiguration", null)
//            }
//
//            private static boolean hasDruid() {
//                ClassUtils.isPresent("com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure", null)
//            }
//
//            private static boolean hasMagicApi() {
//                ClassUtils.isPresent("org.ssssssss.magicapi.spring.boot.starter.MagicAPIAutoConfiguration", null)
//            }
//
//        }
//    }
//}
