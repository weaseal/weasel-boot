///*
// * Copyright (c) 2023-present weasel
// *    weasel-boot is licensed under Mulan PSL v2.
// *    You can use this software according to the terms and conditions of the Mulan PSL v2.
// *    You may obtain a copy of Mulan PSL v2 at:
// *                http://license.coscl.org.cn/MulanPSL2
// *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// *    See the Mulan PSL v2 for more details.
// */
//
//package com.weasel.config
//
//import org.springframework.boot.autoconfigure.cache.CacheProperties
//import org.springframework.cache.annotation.EnableCaching
//import org.springframework.context.annotation.Bean
//import org.springframework.context.annotation.Configuration
//import org.springframework.data.redis.cache.RedisCacheConfiguration
//import org.springframework.data.redis.serializer.RedisSerializationContext
//import org.springframework.data.redis.serializer.RedisSerializer
//
//@EnableCaching
//@Configuration
//class RedisCacheConfig {
//    @Bean
//    RedisCacheConfiguration redisCacheConfiguration(CacheProperties cacheProperties) {
//
//        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig();
//
//        config = config.serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(RedisSerializer.string()));
//        config = config.serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(RedisSerializer.json()));
//
//        CacheProperties.Redis redisProperties = cacheProperties.getRedis();
//
//        if (redisProperties.getTimeToLive() != null) {
//            config = config.entryTtl(redisProperties.getTimeToLive());
//        }
//        if (!redisProperties.isCacheNullValues()) {
//            config = config.disableCachingNullValues();
//        }
//        if (!redisProperties.isUseKeyPrefix()) {
//            config = config.disableKeyPrefix();
//        }
//        if (redisProperties.getKeyPrefix() != null) {
//            config = config.prefixCacheNameWith(redisProperties.getKeyPrefix());
//        }
////        config = config.computePrefixWith(name -> name + ":");// 覆盖默认key双冒号  CacheKeyPrefix#prefixed
//        return config;
//    }
//}
