/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config


import com.mybatisflex.core.datasource.FlexDataSource
import com.mybatisflex.spring.boot.ConditionalOnMybatisFlexDatasource
import org.casbin.adapter.JDBCAdapter
import org.casbin.annotation.CasbinDataSource
import org.casbin.jcasbin.persist.Adapter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import javax.sql.DataSource

/**
 * @author weasel
 * @version 1.0
 * @date 2022/4/20 11:01
 */
@Configuration
class CasbinConfig {

    /**
     * 整合dynamic-datasource,将dynamic-datasource中的主数据源转化为casbin的数据源
     *
     * @return
     */
//    @Bean
    @CasbinDataSource
    @ConditionalOnMybatisFlexDatasource
    DataSource casbinDataSource(FlexDataSource dataSource) {
        dataSource
    }

    /**
     * 自动创建casbin_rule表
     *
     * @return
     */
    @Bean
    Adapter autoConfigJdbcAdapter(/*@CasbinDataSource*/ DataSource casbinDataSource) {
        return new JDBCAdapter(casbinDataSource)
    }
}
