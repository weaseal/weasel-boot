/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config

import com.baomidou.lock.DefaultLockFailureStrategy
import com.baomidou.lock.DefaultLockKeyBuilder
import com.baomidou.lock.LockFailureStrategy
import com.baomidou.lock.LockKeyBuilder
import com.baomidou.lock.exception.LockFailureException
import com.weasel.common.util.AuthUtil
import org.aopalliance.intercept.MethodInvocation
import org.springframework.beans.factory.BeanFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import java.lang.reflect.Method

@Configuration
class Lock4jConfig {
//    @Resource
//    AuthContext authContext

    @Bean
    LockKeyBuilder defaultLockKeyBuilder(BeanFactory beanFactory) {
        new DefaultLockKeyBuilder(beanFactory) {
            @Override
            String buildKey(MethodInvocation invocation, String[] definitionKeys) {
                // 支持多租户
                return "${AuthUtil.tenantId}:${super.buildKey(invocation, definitionKeys)}"
            }
        }
    }

    @Bean
    LockFailureStrategy lockFailureStrategy() {
        return new DefaultLockFailureStrategy() {
            @Override
            void onLockFailure(String key, Method method, Object[] arguments) {
                throw new LockFailureException("系统繁忙，请稍后重试")
            }
        }
    }
}
