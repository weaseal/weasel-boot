package com.weasel.config;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mybatisflex.core.query.QueryWrapper;
import com.weasel.modules.file.entity.FileDetail;
import com.weasel.modules.file.entity.FilePartDetail;
import com.weasel.modules.file.mapper.FileDetailMapper;
import com.weasel.modules.file.mapper.FilePartDetailMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.dromara.x.file.storage.core.FileInfo;
import org.dromara.x.file.storage.core.hash.HashInfo;
import org.dromara.x.file.storage.core.recorder.FileRecorder;
import org.dromara.x.file.storage.core.upload.FilePartInfo;
import org.dromara.x.file.storage.spring.EnableFileStorage;
import org.dromara.x.file.storage.spring.SpringFileStorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Map;

@Configuration
@EnableFileStorage
@AutoConfigureBefore(GracefulResponseConfig.class)
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class XFileStorageConfig implements WebMvcConfigurer {
    private final ObjectMapper objectMapper;
    private final FileDetailMapper fileDetailMapper;

    private final FilePartDetailMapper filePartDetailMapper;
    private final SpringFileStorageProperties fileStorageProperties;

    /**
     * 整合graceful-response会导致已上传的资源无法访问，所以这里配置静态资源访问路径
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        for (SpringFileStorageProperties.SpringLocalConfig local : fileStorageProperties.getLocal()) {
            if (local.getEnableStorage() && local.getEnableAccess()) {
                registry.addResourceHandler(local.getPathPatterns())
                        .addResourceLocations("file:" + local.getBasePath());
            }
        }
        for (SpringFileStorageProperties.SpringLocalPlusConfig local : fileStorageProperties.getLocalPlus()) {
            if (local.getEnableStorage() && local.getEnableAccess()) {
                registry.addResourceHandler(local.getPathPatterns())
                        .addResourceLocations("file:" + local.getStoragePath());
            }
        }
    }

    @Bean
    public FileRecorder fileRecorder() {
        return new FileRecorder() {
            @Override
            @SneakyThrows
            public boolean save(FileInfo fileInfo) {
                FileDetail detail = toFileDetail(fileInfo);
                int b = fileDetailMapper.insert(detail);
                if (b > 0) {
                    fileInfo.setId(detail.getId().toString());
                }

                return b > 0;
            }

            @Override
            @SneakyThrows
            public void update(FileInfo fileInfo) {
                FileDetail detail = toFileDetail(fileInfo);
                QueryWrapper qw = QueryWrapper.create().eq(FileDetail::getUrl, detail.getUrl(), detail.getUrl() != null).eq(FileDetail::getId, detail.getId(), detail.getId() != null);
                fileDetailMapper.updateByQuery(detail, qw);
            }

            @Override
            @SneakyThrows
            public FileInfo getByUrl(String url) {
                return toFileInfo(fileDetailMapper.selectOneByQuery(QueryWrapper.create().eq(FileDetail::getUrl, url)));
            }

            @Override
            public boolean delete(String url) {
                fileDetailMapper.deleteByQuery(QueryWrapper.create().eq(FileDetail::getUrl, url));
                return true;
            }

            @Override
            @SneakyThrows
            public void saveFilePart(FilePartInfo filePartInfo) {
                FilePartDetail detail = toFilePartDetail(filePartInfo);
                if (filePartDetailMapper.insert(detail) > 0) {
                    filePartInfo.setId(detail.getId().toString());
                }

            }

            @Override
            public void deleteFilePartByUploadId(String uploadId) {
                filePartDetailMapper.deleteByQuery(QueryWrapper.create().eq(FilePartDetail::getUploadId, uploadId));
            }

            /**
             * 将 FileInfo 转为 FileDetail
             */
            public FileDetail toFileDetail(FileInfo info) throws JsonProcessingException {
                FileDetail detail = BeanUtil.copyProperties(info, FileDetail.class, "metadata", "userMetadata", "thMetadata", "thUserMetadata", "attr", "hashInfo");

                // 这里手动获 元数据 并转成 json 字符串，方便存储在数据库中
                detail.setMetadata(valueToJson(info.getMetadata()));
                detail.setUserMetadata(valueToJson(info.getUserMetadata()));
                detail.setThMetadata(valueToJson(info.getThMetadata()));
                detail.setThUserMetadata(valueToJson(info.getThUserMetadata()));
                // 这里手动获 取附加属性字典 并转成 json 字符串，方便存储在数据库中
                detail.setAttr(valueToJson(info.getAttr()));
                // 这里手动获 哈希信息 并转成 json 字符串，方便存储在数据库中
                detail.setHashInfo(valueToJson(info.getHashInfo()));
                return detail;
            }

            /**
             * 将 FileDetail 转为 FileInfo
             */
            public FileInfo toFileInfo(FileDetail detail) throws JsonProcessingException {
                FileInfo info = BeanUtil.copyProperties(detail, FileInfo.class, "metadata", "userMetadata", "thMetadata", "thUserMetadata", "attr", "hashInfo");

                // 这里手动获取数据库中的 json 字符串 并转成 元数据，方便使用
                info.setMetadata(jsonToMetadata(detail.getMetadata()));
                info.setUserMetadata(jsonToMetadata(detail.getUserMetadata()));
                info.setThMetadata(jsonToMetadata(detail.getThMetadata()));
                info.setThUserMetadata(jsonToMetadata(detail.getThUserMetadata()));
                // 这里手动获取数据库中的 json 字符串 并转成 附加属性字典，方便使用
                info.setAttr(jsonToDict(detail.getAttr()));
                // 这里手动获取数据库中的 json 字符串 并转成 哈希信息，方便使用
                info.setHashInfo(jsonToHashInfo(detail.getHashInfo()));
                return info;
            }

            /**
             * 将指定值转换成 json 字符串
             */
            public String valueToJson(Object value) throws JsonProcessingException {
                if (value == null) return null;
                return objectMapper.writeValueAsString(value);
            }

            /**
             * 将 json 字符串转换成元数据对象
             */
            public Map<String, String> jsonToMetadata(String json) throws JsonProcessingException {
                if (StrUtil.isBlank(json)) return null;
                return objectMapper.readValue(json, new TypeReference<Map<String, String>>() {
                });
            }

            /**
             * 将 json 字符串转换成字典对象
             */
            public Dict jsonToDict(String json) throws JsonProcessingException {
                if (StrUtil.isBlank(json)) return null;
                return objectMapper.readValue(json, Dict.class);
            }

            /**
             * 将 json 字符串转换成哈希信息对象
             */
            public HashInfo jsonToHashInfo(String json) throws JsonProcessingException {
                if (StrUtil.isBlank(json)) return null;
                return objectMapper.readValue(json, HashInfo.class);
            }

            /**
             * 将 FilePartInfo 转成 FilePartDetail
             * @param info 文件分片信息
             */
            public FilePartDetail toFilePartDetail(FilePartInfo info) throws JsonProcessingException {
                FilePartDetail detail = new FilePartDetail();
                detail.setPlatform(info.getPlatform());
                detail.setUploadId(info.getUploadId());
                detail.setETag(info.getETag());
                detail.setPartNumber(info.getPartNumber());
                detail.setPartSize(info.getPartSize());
                detail.setHashInfo(valueToJson(info.getHashInfo()));
                detail.setCreateTime(LocalDateTime.ofInstant(info.getCreateTime().toInstant(), ZoneId.systemDefault()));
                return detail;
            }

        };
    }
}
