/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config

import cn.dev33.satoken.exception.NotLoginException
import cn.dev33.satoken.stp.StpUtil
import com.mzt.logapi.beans.CodeVariableType
import com.mzt.logapi.beans.LogRecord
import com.mzt.logapi.beans.Operator
import com.mzt.logapi.service.ILogRecordService
import com.mzt.logapi.service.IOperatorGetService
import com.mzt.logapi.starter.annotation.EnableLogRecord
import com.weasel.common.consts.Consts
import com.weasel.modules.sys.entity.SysUser
import groovy.util.logging.Slf4j
import org.dromara.hutool.core.date.DateUtil
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

import javax.annotation.Resource

@Slf4j
@Configuration
@EnableLogRecord(tenant = 'com.weasel')
class BizlogConfig {
    @Resource
    WeaselProperties weaselProperties
    @Resource
    AuthContext authContext

    @Bean
    @Primary
    ILogRecordService logRecordService() {
        new ILogRecordService() {
            @Override
            void "record"(LogRecord logRecord) {
                log.info("""
┏━━━━━ LogRecord [${logRecord.codeVariable[CodeVariableType.ClassName].name}.${logRecord.codeVariable[CodeVariableType.MethodName]}] ━━━
┣ id：        ${logRecord.id}
┣ tenant：    ${logRecord.tenant}
┣ type：      ${logRecord.type}
┣ subType：   ${logRecord.subType}
┣ bizNo：     ${logRecord.bizNo}
┣ operator：  ${logRecord.operator}
┣ fail：      ${logRecord.fail}
┣ createTime：${DateUtil.formatDateTime(logRecord.createTime)}
┣ extra：     ${logRecord.extra}
┣ action：    ${logRecord.action}
┗━━━━━ LogRecord [${logRecord.codeVariable[CodeVariableType.ClassName].name}.${logRecord.codeVariable[CodeVariableType.MethodName]}] ━━━
""")
            }

            @Override
            List<LogRecord> queryLog(String bizNo, String type) {
                return null
            }

            @Override
            List<LogRecord> queryLogByBizNo(String bizNo, String type, String subType) {
                return null
            }
        }
    }

    @Bean
    IOperatorGetService operatorGetService() {
        () ->
                weaselProperties.debug ?
                        new Operator("未登录调试模式") :
                        Optional.of(StpUtil.session[Consts.Session.CURRENT_USER])
                                .map(user -> {
                                    def tenant = authContext.getTenant()
                                    new Operator("tenant:${tenant.id}(${tenant.name}),username:${(user as SysUser).username}")
                                })
                                .orElseThrow(() -> new NotLoginException(NotLoginException.NOT_TOKEN_MESSAGE, null, NotLoginException.NOT_TOKEN))
    }
}
