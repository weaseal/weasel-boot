///*
// * Copyright (c) 2023-present weasel
// *    weasel-boot is licensed under Mulan PSL v2.
// *    You can use this software according to the terms and conditions of the Mulan PSL v2.
// *    You may obtain a copy of Mulan PSL v2 at:
// *                http://license.coscl.org.cn/MulanPSL2
// *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// *    See the Mulan PSL v2 for more details.
// */
//
//package com.weasel.config
//
//import cn.dev33.satoken.stp.StpUtil
//import com.alibaba.druid.DbType
//import com.alibaba.druid.sql.SQLUtils
//import com.alibaba.druid.sql.ast.SQLExpr
//import com.mybatisflex.core.logicdelete.LogicDeleteManager
//import com.mybatisflex.core.row.RowMapper
//import com.mybatisflex.core.table.TableInfoFactory
//import com.mybatisflex.core.tenant.TenantManager
//import com.weasel.common.enums.DataScope
//import io.github.heykb.sqlhelper.handler.InjectColumnInfoHandler
//import io.github.heykb.sqlhelper.handler.abstractor.ConditionInjectInfoHandler
//import io.github.heykb.sqlhelper.handler.abstractor.LogicDeleteInfoHandler
//import io.github.heykb.sqlhelper.handler.abstractor.TenantInfoHandler
//import io.github.heykb.sqlhelper.handler.dynamic.DynamicFindInjectInfoHandler
//import io.github.heykb.sqlhelper.interceptor.SqlHelperPlugin
//import org.springframework.context.annotation.Bean
//import org.springframework.context.annotation.Configuration
//import org.springframework.context.annotation.Lazy
//import org.springframework.util.AntPathMatcher
//
//import javax.annotation.Resource
//
///**
// * @author weasel
// * @date 2022/4/14 9:35
// * @version 1.0
// */
//@Configuration
//class MybatisSqlHelperConfig {
//    @Lazy
//    @Resource
//    AuthContext authContext
//    @Resource
//    WeaselProperties weaselProperties
//
//    @Bean
//    SqlHelperPlugin sqlHelperPlugin() {
//        WeaselProperties.TenantProperties tenantProperties = weaselProperties.tenant
//        List<InjectColumnInfoHandler> injectColumnInfoHandlers = []
//        SqlHelperPlugin sqlHelperPlugin = new SqlHelperPlugin()
//        def logicDeleteInfoHandler = new LogicDeleteInfoHandler() {
//
//            @Override
//            String getDeleteSqlDemo() {
//                System.err.println("逻辑删除sql示例：")
//                "UPDATE xx SET deleted1 = id"
//            }
//
//            @Override
//            String getNotDeletedValue() {
//                '0'
//            }
//
//            @Override
//            String getColumnName() {
//                'deleted'
//            }
//
////            @Override
////            boolean isExistOverride() {
////                return false
////            }
//
//            @Override
//            boolean checkTableName(String tableName) {
//                def tableInfo = TableInfoFactory.ofTableName(tableName)
//                tableInfo?.logicDeleteColumn && LogicDeleteManager.getLogicDeleteColumn(tableInfo?.logicDeleteColumn)
//            }
//
//            @Override
//            boolean checkMapperId(String mapperId) {
//                AntPathMatcher antPathMatcher = new AntPathMatcher()
//                antPathMatcher.match("${RowMapper.class.package.name}.**", mapperId)
//            }
//        }
//        injectColumnInfoHandlers << logicDeleteInfoHandler
//        if (tenantProperties.enabled) {
//            /**
//             * 多租户插件：支持 com.mybatisflex.core.row.Db 中的查询方法
//             */
//            ConditionInjectInfoHandler tenantConditionInfoHandler = new ConditionInjectInfoHandler() {
////                @Override
////                SQLExpr toConditionSQLExpr(String tableAlias, DbType dbType, Map<String, String> columnAliasMap, boolean isMapUnderscoreToCamelCase) {
////                    if (authContext.tenantId == weaselProperties.tenant.privilegedTenantId) {
//////                        return SQLUtils.toSQLExpr("1=1")
//////                        return SQLUtils.toSQLExpr("select 1")
////                        return SQLUtils.toSQLExpr('')
////                    }
////                    return super.toConditionSQLExpr(tableAlias, dbType, columnAliasMap, isMapUnderscoreToCamelCase)
////                }
//
//                @Override
//                String getColumnName() {
//                    tenantProperties.column
//                }
//
//                @Override
//                String getValue() {
//                    if (authContext.tenantId == weaselProperties.tenant.privilegedTenantId) {
//                        return ''
//                    }
//                    return weaselProperties?.debug ? '(0,1)' : authContext.tenantId
//                }
//
//                @Override
//                String op() {
//                    if (authContext.tenantId == weaselProperties.tenant.privilegedTenantId) {
//                        return ' is not null '
//                    }
//                    return ' in '
//                }
//
//                @Override
//                boolean checkTableName(String tableName) {
//                    !(tableName in tenantProperties.ignoredTables) && TableInfoFactory.ofTableName(tableName)?.tenantIdColumn && TenantManager.getTenantIds(tableName)
//                }
//
//                @Override
//                boolean checkMapperId(String mapperId) {
//                    AntPathMatcher antPathMatcher = new AntPathMatcher()
//                    antPathMatcher.match("${RowMapper.class.package.name}.**", mapperId)
//                }
//            }
//            injectColumnInfoHandlers << tenantConditionInfoHandler
//            /**
//             * 多租户插件：支持 com.mybatisflex.core.row.Db 中的非查询方法
//             */
//            TenantInfoHandler tenantInfoHandler = new TenantInfoHandler() {
//                @Override
//                boolean isExistOverride() {
//                    if (authContext.tenantId != weaselProperties.tenant.privilegedTenantId) {
//                        return true
//                    }
//                    return false
//                }
////                @Override
////                SQLExpr toSQLExpr(DbType dbType) {
//////                    if (authContext.tenantId == weaselProperties.tenant.privilegedTenantId) {
//////                        return new SQLAllExpr()
//////                    }
////                    return super.toSQLExpr(dbType)
////                }
//
//                @Override
//                String getTenantIdColumn() {
//                    tenantProperties.column
//                }
//
//                @Override
//                String getTenantId() {
//                    weaselProperties?.debug ? '0' : authContext.getTenantId()
//                }
//
//                @Override
//                boolean checkTableName(String tableName) {
//                    !(tableName in tenantProperties.ignoredTables) && TableInfoFactory.ofTableName(tableName)?.tenantIdColumn && TenantManager.getTenantIds(tableName)
//                }
//
//                @Override
//                boolean checkMapperId(String mapperId) {
//                    AntPathMatcher antPathMatcher = new AntPathMatcher()
//                    antPathMatcher.match("${RowMapper.class.package.name}.**", mapperId)
//                }
//
//                @Override
//                int getInjectTypes() {
//                    return INSERT | UPDATE
//                }
//            }
////            InjectColumnInfoHandler tenantInfoHandler = new InjectColumnInfoHandler() {
////                @Override
////                String getColumnName() {
////                    tenantProperties.column
////                }
////
////                @Override
////                String getValue() {
////                    weaselProperties?.debug ? '0' : authContext.getTenantId()
////                }
////
////                @Override
////                int getInjectTypes() {
////                    return INSERT | UPDATE
////                }
////
////                @Override
////                String op() {
////                    return "=";
////                }
////
////                @Override
////                boolean isExistOverride() {
////                    return false;
////                }
////
////                @Override
////                boolean checkTableName(String tableName) {
////                    !(tableName in tenantProperties.ignoredTables)
////                }
////
////                @Override
////                boolean checkMapperId(String mapperId) {
////                    // 支持mybatis-plus手动设置拦截器忽略执行策略
////                    !InterceptorIgnoreHelper.willIgnoreTenantLine(mapperId)
////                }
////
//////                @Override
//////                SQLExpr toSQLExpr(DbType dbType) {
//////                    return super.toSQLExpr(dbType)
//////                }
////            }
//            injectColumnInfoHandlers << tenantInfoHandler
//        }
//
//        sqlHelperPlugin.injectColumnInfoHandlers = injectColumnInfoHandlers
//
//        DynamicFindInjectInfoHandler dynamicFindInjectInfoHandler = new DynamicFindInjectInfoHandler() {
//            @Override
//            List<InjectColumnInfoHandler> findInjectInfoHandlers() {
//
//                // 数据权限插件
//                ConditionInjectInfoHandler conditionInjectInfoHandler = new ConditionInjectInfoHandler() {
//                    List<DataScope> dataScopes
//
//                    @Override
//                    String getColumnName() {
//                        'create_by'
//                    }
//
//                    @Override
//                    String getValue() {
//                        def sql
//                        dataScopes = dataScopes ?: authContext.getDataScopes()
//                        switch (dataScopes) {
//                            case { DataScope.LEVEL_CHILDREN in dataScopes || (DataScope.LEVEL in dataScopes && DataScope.SELF_CHILDREN in dataScopes) }:
//                                sql = "select user_id from sys_user_dept where dept_id in (select dept_id from sys_user_dept where user_id=${StpUtil.getLoginIdAsString()})"
//                                break
//                            case { DataScope.LEVEL in dataScopes }:
//                                sql = "select user_id from sys_user_dept where dept_id in (select dept_id from sys_user_dept where user_id=${StpUtil.getLoginIdAsString()})"
//                                break
//                            case { DataScope.SELF_CHILDREN in dataScopes }:
//                                sql = "select user_id from sys_user_dept where dept_id in (select dept_id from sys_user_dept where user_id=${StpUtil.getLoginIdAsString()})"
//                                break
//                            case { DataScope.SELF in dataScopes }:
//                                sql = StpUtil.getLoginIdAsString()
//                                break
////                            case { DataScope.ALL in dataScopes }:
////                                sql = "(${true})"
////                                break
//                        }
//                        "(${sql})"
//                    }
//
//                    @Override
//                    String op() {
//                        'in'
//                    }
//
//                    @Override
//                    boolean checkTableName(String tableName) {
//                        tableName == 'sys_config1'
////                        !tableName.startsWithIgnoreCase("sys_") &&tableName.startsWithIgnoreCase("sys_config") &&  !tableName.startsWithIgnoreCase("social_") && !tableName.startsWithIgnoreCase("dev_")
//                    }
//
//                    @Override
//                    SQLExpr toConditionSQLExpr(String tableAlias, DbType dbType, Map<String, String> columnAliasMap, boolean isMapUnderscoreToCamelCase) {
//                        dataScopes = dataScopes ?: authContext.getDataScopes()
//                        DataScope.ALL in dataScopes ? SQLUtils.toSQLExpr('true') : super.toConditionSQLExpr(tableAlias, dbType, columnAliasMap, isMapUnderscoreToCamelCase)
//                    }
//                }
//                [conditionInjectInfoHandler]
//            }
//        }
//        sqlHelperPlugin.dynamicFindInjectInfoHandler = dynamicFindInjectInfoHandler
//        return sqlHelperPlugin
//    }
//}
