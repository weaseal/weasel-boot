/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config

import com.plumelog.core.TraceId
import com.yomahub.tlog.context.TLogContext
import com.yomahub.tlog.springboot.property.TLogProperty
import org.springframework.context.annotation.Configuration
import org.springframework.web.method.HandlerMethod
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

import javax.annotation.Resource
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Configuration
//@AutoConfigureAfter(TLogWebAutoConfiguration)
//@Order(Ordered.HIGHEST_PRECEDENCE)
class PlumelogConfig implements WebMvcConfigurer {
    @Resource
    WeaselProperties weaselProperties
    @Resource
    TLogProperty tLogProperty

    @Override
    void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new HandlerInterceptor() {
            @Override
            boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
                // 排除静态资源及上传的文件
                if (!(handler in HandlerMethod)) {
                    return true
                }
//                String traceId = TraceId.logTraceID.get();
//                if (StringUtils.isEmpty(traceId)) {
////                    TraceId.set();
//                    TraceId.logTraceID.set(tLogProperty.pattern.replace('$spanId', TLogContext.spanId).replace('$traceId', TLogContext.traceId));
//                } else {
//                    TraceId.logTraceID.set(traceId);
//                }
//                def header = response.getHeader('tlogTraceId')
//                String traceId = header
                String traceId = tLogProperty.pattern.replace('$spanId', TLogContext.spanId).replace('$traceId', TLogContext.traceId)
                TraceId.logTraceID.set(traceId);
                // 整合tlog
//                String traceId = tLogProperty.pattern.replace('$spanId', TLogContext.spanId).replace('$traceId', TLogContext.traceId)
////                System.err.println("---------")
////                System.err.println(TLogContext.spanId)
////                System.err.println(TLogContext.traceId)
////                String traceId = tLogProperty.pattern.replace(['$spanId': TLogContext.spanId, '$traceId': TLogContext.traceId])
//                if (StringUtils.isEmpty(traceId)) {
//                    TraceId.set();
//                } else {
//                    TraceId.logTraceID.set(traceId);
//                }
                return true;
            }

//            @Override
//            void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
////                super.postHandle(request, response, handler, modelAndView)
////                TraceId.logTraceID.remove()
//            }
//
//            @Override
//            void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
////                super.afterCompletion(request, response, handler, ex)
////                TraceId.logTraceID.remove()
//            }
//
//            @Override
//            void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
////                super.afterConcurrentHandlingStarted(request, response, handler)
////                TraceId.logTraceID.remove()
//            }
        })/*.order(Ordered.HIGHEST_PRECEDENCE)*/.excludePathPatterns(weaselProperties.security.excludePath)
    }

//    @Bean
//    FilterRegistrationBean<TraceIdFilter> traceIdFilterFilterRegistrationBean() {
//        FilterRegistrationBean<TraceIdFilter> filterRegistrationBean = new FilterRegistrationBean<>()
//        // 设置比常规过滤器更高的优先级，防止输入流被更早读取
////        filterRegistrationBean.setOrder(-999)
//        filterRegistrationBean.setFilter(new TraceIdFilter())
//        return filterRegistrationBean
//    }
}
