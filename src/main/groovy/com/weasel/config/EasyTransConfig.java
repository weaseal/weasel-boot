///*
// * Copyright (c) 2023-present weasel
// *    weasel-boot is licensed under Mulan PSL v2.
// *    You can use this software according to the terms and conditions of the Mulan PSL v2.
// *    You may obtain a copy of Mulan PSL v2 at:
// *                http://license.coscl.org.cn/MulanPSL2
// *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// *    See the Mulan PSL v2 for more details.
// */
//
//package com.weasel.config;
//
//import com.fhs.trans.service.impl.DictionaryTransService;
//import com.weasel.common.consts.Consts;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.boot.autoconfigure.AutoConfigureAfter;
//import org.springframework.context.annotation.Configuration;
//
//import javax.annotation.Resource;
//
///**
// * @author weasel
// * @version 1.0
// * @date 2022/5/10 15:14
// */
//@Configuration
//@AutoConfigureAfter(RedisCacheConfig.class)
//public class EasyTransConfig/* implements InitializingBean*/ {
//
//    @Resource
//    private DictionaryTransService dictionaryTransService;
//
//    public EasyTransConfig() {
//        Consts.AppCache.CACHE_MAP.forEach((k, v) -> {
//            dictionaryTransService.refreshCache(k, v);
//        });
//    }
//
////    @Override
////    public void afterPropertiesSet() throws Exception {
////        Consts.AppCache.CACHE_MAP.forEach((k, v) -> {
////            dictionaryTransService.refreshCache(k, v);
////        });
////    }
//}
