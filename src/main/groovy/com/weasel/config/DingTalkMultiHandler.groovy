package com.weasel.config
/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

//package com.weasel.config
//
//
//import com.github.jaemon.dinger.multi.DingerConfigHandler
//import com.github.jaemon.dinger.multi.algorithm.AlgorithmHandler
//import com.github.jaemon.dinger.core.DingerConfig
//import com.github.jaemon.dinger.multi.algorithm.RoundRobinHandler
//
//class DingTalkMultiHandler implements DingerConfigHandler {
//    @Override
//    List<DingerConfig> dingerConfigs() {
//        //List<com.github.jaemon.dinger.core.DingerConfig> dingerConfigs = []
//        //// 注意这里的dingerConfig根据实际情况配置
//        //dingerConfigs << com.github.jaemon.dinger.core.DingerConfig.instance(/*DingerType.BYTETALK, */"eaec18d2d022ec7cbebea22c6ceac622573ec91cbb59e55233b621ee30901c8b", "SEC1ffdfcebe09826ee7370082ef1e44258aac3f0914bf133e307c3a7adfee61685")
//        ////dingerConfigs << com.github.jaemon.dinger.core.DingerConfig.instance(/*DingerType.BYTETALK, */"30c1be92b1dc8dd6308f480208edbe62c0a55cd1e6e30dc8c55454b5e0536752", "SECd6c469428810c432f27cd48309cc3255de05e569b73ca2c3980335982ee4e6c8")
//        ////dingerConfigs << _DingerConfig.instance("tokenId1", "secret1")
//        //
//        //return dingerConfigs
//        List<DingerConfig> dingerConfigs = new ArrayList<>();
//        // 注意这里的dingerConfig根据实际情况配置
//
//        def instance = DingerConfig.instance("eaec18d2d022ec7cbebea22c6ceac622573ec91cbb59e55233b621ee30901c8b", "SEC1ffdfcebe09826ee7370082ef1e44258aac3f0914bf133e307c3a7adfee61685")
//        instance.metaClass.id = 1
//        dingerConfigs.add(
//                instance
//        );
//
//        def instance1 = DingerConfig.instance("30c1be92b1dc8dd6308f480208edbe62c0a55cd1e6e30dc8c55454b5e0536752", "SECd6c469428810c432f27cd48309cc3255de05e569b73ca2c3980335982ee4e6c8")
//        instance.metaClass.id = 2
//        dingerConfigs.add(
//                instance1
//        );
//        //dingerConfigs.add(
//        //        DingerConfig.instance("tokenId2", "secret2")
//        //);
//        //dingerConfigs.add(
//        //        DingerConfig.instance("tokenId3", "secret3")
//        //);
//        return dingerConfigs;
//    }
//
//    @Override
//    Class<? extends AlgorithmHandler> algorithmHandler() {
//        // 使用轮询算法
//        return RoundRobinHandler.class
//        //return (List<DingerConfig> dingerConfigs, DingerConfig defaultDingerConfig) -> {
//        //    dingerConfigs.find { it.id % 2 == 0 }
//        //} as AlgorithmHandler
//        //return TenantAlgorithmHandler.class
//    }
//
//    static class TenantAlgorithmHandler implements AlgorithmHandler {
//
//        @Override
//        DingerConfig handler(List<DingerConfig> dingerConfigs, DingerConfig defaultDingerConfig) {
//            dingerConfigs.find { it.id % 2 == 0 }
//        }
//    }
//}
