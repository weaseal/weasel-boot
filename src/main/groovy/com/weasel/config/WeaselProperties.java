/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config;

import cloud.tianai.captcha.resource.common.model.dto.Resource;
import cloud.tianai.captcha.resource.common.model.dto.ResourceMap;
import com.weasel.common.enums.CaptchaType;
import com.weasel.common.enums.UiPlatform;
import com.zaxxer.hikari.HikariConfig;
import lombok.Data;
import org.dromara.hutool.extra.spring.SpringUtil;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author weasel
 * @version 1.0
 * @date 2022/4/11 14:06
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "weasel")
public class WeaselProperties {
    /**
     * 基础包名，多处使用到
     */
    String basePackage = SpringUtil.getApplicationContext().getBeansWithAnnotation(SpringBootApplication.class).values().toArray()[0].getClass().getPackage().getName();
    /**
     * 是否调试模式；兼容单元测试时未登录的情况
     */
    Boolean debug = false;
    /**
     * ui框架
     */
    UiPlatform uiPlatform;
    /**
     * 启动完成之后打印地址
     */
    Boolean showUrl = false;
    /**
     * 验证码类型
     */
    CaptchaType captchaType = CaptchaType.TIANAI;
    /**
     * 天爱验证码配置
     */
    TianaiCaptchaProperties tianai;
    /**
     * 动态路由前缀
     */
    String dynamicRouterPrefix = "/_d";
    /**
     * 动态数据源加载配置
     */
    SecurityProperties security;
    /**
     * 动态数据源加载配置
     */
    DynamicDataSourceProperties dynamicDataSource;
    /**
     * 多租户配置
     */
    TenantProperties tenant;
    /**
     * 响应体封装配置
     */
    ResponseWrapperProperties responseWrapper;

    @Data
    public static class SecurityProperties {
        /**
         * 权限认证排除路径
         */
        String[] excludePath;
    }

    @Data
    public static class TianaiCaptchaProperties {

        /**
         * 模板
         */
        Map<String, List<ResourceMap>> templates;
        /**
         * 资源
         */
        Map<String, List<Resource>> resources;
    }

    @Data
    public static class DynamicDataSourceProperties {
        /**
         * 是否从数据库中获取数据源
         */
        Boolean loadFromJdbc = false;
        /**
         * 是否从配置文件中的主数据源获取数据源
         */
        Boolean loadFromPrimary = true;
        /**
         * 数据源
         */
        @NestedConfigurationProperty
        HikariConfig datasource;
        /**
         * sql
         */
        String sql;
    }

    @Data
    public static class TenantProperties {

        /**
         * 是否启用多租户
         */
        Boolean enabled = false;
        /**
         * 租户字段名
         */
        String column = "tenant_id";
        /**
         * 哪些表忽略租户隔离，仅对【行级】隔离级别有效
         */
        String[] ignoredTables;
        /**
         * 特权租户ID，该租户可以无视租户SQL拦截器；
         * 目前仅对【行级】隔离级别有效
         */
        Long privilegedTenantId;
    }

    @Data
    public static class LogicDeleteProperties {

        /**
         * 是否启用逻辑删除
         */
        Boolean enabled = false;
        /**
         * 租户字段名
         */
        String column = "deleted";
    }

    @Data
    public static class ResponseWrapperProperties {

        private boolean enabled = Boolean.FALSE;

        private String successField = "success";

        private String codeField = "code";

        private Serializable codeSuccessValue;

        private String msgField = "msg";

        private String timestamp = "timestamp";

        private String dataField = "data";

        /**
         * 当前页
         */
        private String previousPage = "previousPage";
        /**
         * 下一页
         */
        private String nextPage = "nextPage";

        /**
         * 总页数
         */
        private String pageSize = "pageSize";

        private String totalPageSize = "totalPageSize";

        /**
         * 是否有下一页
         */
        private String hasNext = "hasNext";

        /**
         * 是否包装分页结果到data
         */
        private Boolean pageWrap = Boolean.TRUE;

        private String pageData = "pageData";

        /**
         * 排除不需要统一返回的restFull
         */
//        private String[] excludePackages;
        private String[] excludePath;
        /**
         * 添加需要统一返回的restFull
         */
        private String[] includePackages;
    }
}
