/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config

import com.fasterxml.jackson.databind.ser.std.DateSerializer
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer
import net.dreamlu.mica.core.utils.DateUtil
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

import java.text.DateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

@Configuration
class JacksonConfig implements WebMvcConfigurer {

    @Bean
    Jackson2ObjectMapperBuilderCustomizer longJackson2ObjectMapperBuilderCustomizer() {
        return (builder) -> {
            builder.serializerByType(Long, ToStringSerializer.instance)

            //super(MicaJavaTimeModule.class.getName());
            //this.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateUtil.DATETIME_FORMATTER));
            //this.addDeserializer(LocalDate.class, new LocalDateDeserializer(DateUtil.DATE_FORMATTER));
            //this.addDeserializer(LocalTime.class, new LocalTimeDeserializer(DateUtil.TIME_FORMATTER));
            builder.serializerByType(Date.class, new DateSerializer(false, DateFormat.getDateTimeInstance()));
            builder.serializerByType(LocalDateTime.class, new LocalDateTimeSerializer(DateUtil.DATETIME_FORMATTER));
            builder.serializerByType(LocalDate.class, new LocalDateSerializer(DateUtil.DATE_FORMATTER));
            builder.serializerByType(LocalTime.class, new LocalTimeSerializer(DateUtil.TIME_FORMATTER));
            //builder.modules(SpiUtil.loadList(Module).toList())
        }
    }
}

