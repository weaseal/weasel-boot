/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config

import cn.dev33.satoken.spring.SpringMVCUtil
import cn.dev33.satoken.stp.StpUtil
import com.alicp.jetcache.CacheValueHolder
import com.alicp.jetcache.anno.KeyConvertor
import com.alicp.jetcache.anno.config.EnableMethodCache
import com.alicp.jetcache.autoconfigure.JetCacheAutoConfiguration
import com.fasterxml.jackson.databind.ObjectMapper
import org.dromara.hutool.extra.spring.SpringUtil
import org.springframework.boot.autoconfigure.AutoConfigureBefore
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import java.util.function.Function

@Configuration
@EnableMethodCache(basePackages = "com.weasel")
class JetCacheConfig {

    @AutoConfigureBefore(JetCacheAutoConfiguration)
    class BeforeJetCacheAutoConfiguration {
        @Bean('tenantJacksonKeyConvertor')
        KeyConvertor tenantJacksonKeyConvertor() {
            return { originalKey ->
                WeaselProperties.TenantProperties tenantProperties = SpringUtil.getBean(WeaselProperties.class).tenant
                if (tenantProperties.enabled && SpringMVCUtil.isWeb() && StpUtil.login) {
                    "${SpringUtil.getBean(AuthContext.class).tenantId}:$originalKey" as String
                } else {
                    originalKey as String
                }
            }
        }

        @Bean('jacksonValueEncoder')
        Function<Object, byte[]> jacksonValueEncoder(ObjectMapper objectMapper) {
            return { Object o ->
                objectMapper.writeValueAsBytes(o)
            }
        }

        @Bean('jacksonValueDecoder')
        Function<byte[], Object> jacksonValueDecoder(ObjectMapper objectMapper) {
            return { byte[] bytes ->
                objectMapper.readValue(bytes, CacheValueHolder.class)
            }
        }
    }
}
