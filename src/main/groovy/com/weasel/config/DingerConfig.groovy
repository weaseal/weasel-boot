package com.weasel.config
/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

//package com.weasel.config
//
//import com.github.jaemon.dinger.core.DingerConfig as _DingerConfig
//import com.github.jaemon.dinger.core.entity.enums.DingerType
//import com.github.jaemon.dinger.multi.DingerConfigHandler
//import com.github.jaemon.dinger.multi.algorithm.AlgorithmHandler
//import com.github.jaemon.dinger.multi.algorithm.RoundRobinHandler
//import com.github.jaemon.dinger.support.DingerAsyncCallback
//import groovy.util.logging.Slf4j
//import org.springframework.context.annotation.Bean
//import org.springframework.context.annotation.Configuration
//
//@Slf4j
//@Configuration
////@DingerScan(basePackages = "com.weasel.notify")
///*@EnableMultiDinger(
//        // 启用钉钉多机器人配置
//        [@MultiDinger(
//                dinger = DingerType.DINGTALK,
//                handler = DingTalkMultiHandler
//        )*//*, @MultiDinger(
//                dinger = DingerType.WETALK,
//                handler = WeTalkMultiHandler.class
//        ), @MultiDinger(
//                dinger = DingerType.BYTETALK,
//                handler = ByteTalkMultiHandler
//        )*//*]
//)*/
//class DingerConfig {
//    ///**
//    // * 使用飞书群机器人签名校验安全设置时，需要重新设置签名算法 {@link DingerType#BYTETALK}
//    // * @return 签名算法
//    // */
//    //@Bean
//    //ByteTalkSignAlgorithm byteTalkSignAlgorithm() {
//    //    new ByteTalkSignAlgorithm()
//    //}
//    //@Bean
//    //DingTalkSignAlgorithm dingTalkSignAlgorithm() {
//    //    new DingTalkSignAlgorithm()
//    //}
//
//    /**
//     * 默认异步执行回调实例
//     *
//     * @return 回调实例
//     */
//    @Bean
//    DingerAsyncCallback dingerAsyncCallback() {
//        (String dingerId, String result) -> {
//            log.info("dingerId=[{}], result=[{}].", dingerId, result)
//        }
//    }
//
//    //static class DingTalkMultiHandler implements DingerConfigHandler {
//    //    @Override
//    //    List<_DingerConfig> dingerConfigs() {
//    //        List<_DingerConfig> dingerConfigs = []
//    //        // 注意这里的dingerConfig根据实际情况配置
//    //        dingerConfigs << _DingerConfig.instance("tokenId1", "secret1")
//    //        dingerConfigs << _DingerConfig.instance("tokenId1", "secret1")
//    //
//    //        return dingerConfigs
//    //    }
//    //
//    //    @Override
//    //    Class<? extends AlgorithmHandler> algorithmHandler() {
//    //        // 使用轮询算法
//    //        return RoundRobinHandler.class
//    //    }
//    //}
//    //
//    //static class WeTalkMultiHandler implements DingerConfigHandler {
//    //    @Override
//    //    List<_DingerConfig> dingerConfigs() {
//    //        List<_DingerConfig> dingerConfigs = []
//    //        // 注意这里的dingerConfig根据实际情况配置
//    //        dingerConfigs << _DingerConfig.instance("tokenId1", "secret1")
//    //        dingerConfigs << _DingerConfig.instance("tokenId1", "secret1")
//    //
//    //        return dingerConfigs
//    //    }
//    //
//    //    @Override
//    //    Class<? extends AlgorithmHandler> algorithmHandler() {
//    //        // 使用轮询算法
//    //        return RoundRobinHandler.class
//    //    }
//    //}
//
//    static class ByteTalkMultiHandler implements DingerConfigHandler {
//        @Override
//        List<_DingerConfig> dingerConfigs() {
//            List<_DingerConfig> dingerConfigs = []
//            // 注意这里的dingerConfig根据实际情况配置
//            dingerConfigs << _DingerConfig.instance(DingerType.BYTETALK, "35b84315-90ab-4fb0-bc21-642f9f502c36", "d8dcXo46qfu8Qv5pl2kScf")
//            //dingerConfigs << _DingerConfig.instance("tokenId1", "secret1")
//
//            return dingerConfigs
//        }
//
//        @Override
//        Class<? extends AlgorithmHandler> algorithmHandler() {
//            // 使用轮询算法
//            return RoundRobinHandler.class
//        }
//    }
//}
