package com.weasel.config
/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

//package com.weasel.config
//
//import com.github.jaemon.dinger.core.entity.enums.DingerType
//import com.github.jaemon.dinger.multi.DingerConfigHandler
//import com.github.jaemon.dinger.multi.algorithm.AlgorithmHandler
//import com.github.jaemon.dinger.multi.algorithm.RoundRobinHandler
//
//class ByteTalkMultiHandler implements DingerConfigHandler {
//    @Override
//    List<com.github.jaemon.dinger.core.DingerConfig> dingerConfigs() {
//        List<com.github.jaemon.dinger.core.DingerConfig> dingerConfigs = []
//        // 注意这里的dingerConfig根据实际情况配置
//        dingerConfigs << com.github.jaemon.dinger.core.DingerConfig.instance(/*DingerType.BYTETALK, */"35b84315-90ab-4fb0-bc21-642f9f502c36", "d8dcXo46qfu8Qv5pl2kScf")
//        //dingerConfigs << _DingerConfig.instance("tokenId1", "secret1")
//
//        return dingerConfigs
//    }
//
//    @Override
//    Class<? extends AlgorithmHandler> algorithmHandler() {
//        // 使用轮询算法
//        return RoundRobinHandler.class
//    }
//}
