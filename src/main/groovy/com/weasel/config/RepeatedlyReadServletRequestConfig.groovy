/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config

import groovy.util.logging.Slf4j
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.filter.OncePerRequestFilter

import javax.servlet.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletRequestWrapper
import javax.servlet.http.HttpServletResponse

/**
 * <pre>
 * 包装HttpServletRequest实现输入流可重复读取，防止文件上传失败
 * 验证方法在 com.weasel.modules.file.controller.FileDetailController#upload(HttpServletRequest request)
 * 代码参考yue-library com.yue.lib.web.filter.RepeatedlyReadServletRequestFilter
 * </pre>
 * @author ylyue
 * @since 2020年9月3日
 */
@Slf4j
@Configuration
class RepeatedlyReadServletRequestConfig {
    /**
     * 配置输入流可反复读取的HttpServletRequest
     */
    @Bean
    FilterRegistrationBean<RepeatedlyReadServletRequestFilter> registerRepeatedlyReadRequestFilter() {
        FilterRegistrationBean<RepeatedlyReadServletRequestFilter> filterRegistrationBean = new FilterRegistrationBean<>()
        filterRegistrationBean.setFilter(new RepeatedlyReadServletRequestFilter())
        log.info("【初始化配置-HttpServletRequest】默认配置为true，当前环境为true：默认启用输入流可反复读取的HttpServletRequest ... 已初始化完毕。")
        return filterRegistrationBean
    }

    @Slf4j
    static class RepeatedlyReadServletRequestFilter extends OncePerRequestFilter {

        @Override
        protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
                throws ServletException, IOException {
            log.debug("传递输入流可反复读取的HttpServletRequest ...")

            /*
             * 在request被包装之前，先进行一次参数解析处理，避免后续出现使用未包装的request导致：FileUploadException: Stream closed.
             * 如：调用 request#getParts() 将使用已解析过的结果（parse multipart content and store the parts）.
             */

            ServletRequest requestWrapper = new RepeatedlyReadServletRequestWrapper(request)
            filterChain.doFilter(requestWrapper, response)
        }

    }

    @Slf4j
    static class RepeatedlyReadServletRequestWrapper extends HttpServletRequestWrapper {/**
     * 流中的数据
     */
        private final byte[] body;

        public RepeatedlyReadServletRequestWrapper(HttpServletRequest request) {
            super(request);
            // 获取流中的数据放到字节数组中
            body = request.getInputStream().bytes
        }

        @Override
        public BufferedReader getReader() throws IOException {
            return new BufferedReader(new InputStreamReader(getInputStream()));
        }

        @Override
        public ServletInputStream getInputStream() throws IOException {
            // 缓存数据
            final ByteArrayInputStream body = new ByteArrayInputStream(this.body);

            return new ServletInputStream() {

                @Override
                public boolean isFinished() {
                    return false;
                }

                @Override
                public boolean isReady() {
                    return false;
                }

                @Override
                public void setReadListener(ReadListener readListener) {

                }

                @Override
                public int read() throws IOException {
                    // 从缓存的数据中读取数据
                    return body.read();
                }

            };
        }

    }
}
