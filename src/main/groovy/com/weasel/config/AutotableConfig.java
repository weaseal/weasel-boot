/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.config;

import com.tangzc.autotable.core.dynamicds.IDataSourceHandler;
import com.tangzc.autotable.springboot.EnableAutoTable;
import com.tangzc.mybatisflex.autotable.AutoTableDynamicDatasourceHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.Set;
import java.util.function.BiConsumer;

/**
 * @author weasel
 * @version 1.0
 * @date 2022/3/31 14:05
 */
@Slf4j
@Configuration
@EnableAutoTable
public class AutotableConfig {
    public AutotableConfig() {
//        AutoTableGlobalConfig.addStrategy(new ShardingSphereStrategy());
    }

    //    @Bean
    @Primary
    public IDataSourceHandler dataSourceHandler() {
        return new AutoTableDynamicDatasourceHandler() {

            @Override
            public void handleAnalysis(Set<Class<?>> classList, BiConsumer<String, Set<Class<?>>> consumer) {
                super.handleAnalysis(classList, consumer);
            }

            @Override
            public String getDatabaseDialect(String dataSource) {
                if ("shardingsphere".equals(dataSource)) {
                    return "ShardingSphere";
                }
                return super.getDatabaseDialect(dataSource);
            }
        };
    }

}
