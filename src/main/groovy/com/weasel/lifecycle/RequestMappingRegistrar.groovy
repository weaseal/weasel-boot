package com.weasel.lifecycle
/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

//package com.weasel.runner
//
//
//import com.baomidou.mybatisplus.core.metadata.TableInfoHelper
//import com.weasel.common.base.controller.CC
//import com.weasel.common.base.com.weasel.common.base.controller.DynamicController
//import com.weasel.modules.sys.entity.SysUser
//import groovy.util.logging.Slf4j
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.boot.CommandLineRunner
//import org.springframework.core.annotation.AnnotatedElementUtils
//import org.springframework.expression.EvaluationContext
//import org.springframework.expression.Expression
//import org.springframework.expression.ExpressionParser
//import org.springframework.expression.common.TemplateParserContext
//import org.springframework.expression.spel.standard.SpelExpressionParser
//import org.springframework.expression.spel.support.StandardEvaluationContext
//import org.springframework.stereotype.Component
//import org.springframework.web.bind.annotation.RequestMapping
//import org.springframework.web.servlet.mvc.method.RequestMappingInfo
//import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping
//
//import java.lang.reflect.Method
//
///**
// * RequestMapping动态注册【尚未启用】
// *
// * @author weasel
// * @date 2023-3-14 10:25:59
// * @since 1.0.0
// */
//@Slf4j
////@Component
//class RequestMappingRegistrar implements CommandLineRunner {
//    @Autowired
//    private RequestMappingHandlerMapping requestMappingHandlerMapping;
//
//    @Override
//    void run(String... args) throws Exception {
//        log.info("动态注册RequestMapping");
////        RequestMappingInfo.BuilderConfiguration config = new RequestMappingInfo.BuilderConfiguration();
////        config.setPatternParser(requestMappingHandlerMapping.getPatternParser());
////
////        RequestMappingInfo requestMappingInfo = RequestMappingInfo
////                .paths("/list1")
////                .methods(RequestMethod.GET)
////                .produces(MediaType.APPLICATION_JSON_VALUE)
//////                .consumes(MediaType.APPLICATION_JSON_VALUE)
////                .options(config)
////                .build();
////
////        requestMappingHandlerMapping.registerMapping(requestMappingInfo, new CC(), ReflectUtil.getMethodByName(CC.class,"list1"));
//////
//////        //注销接口
//////        //requestMappingHandlerMapping.unregisterMapping( requestMappingInfo );
//////
//////        //获取所有的handler
//////        Map<RequestMappingInfo, HandlerMethod> handlerMethods = requestMappingHandlerMapping.getHandlerMethods();
////////        log.error(handlerMethods)
//
////        Mapping.create(requestMappingHandlerMapping).registerController(new com.weasel.common.base.controller.DynamicController())
//        registerController(new CC())
//        registerController(new CC())
//    }
//
//    public void registerController(Object target) {
//        Method[] methods = target.getClass().getDeclaredMethods();
//        TableInfoHelper.getTableInfos().each { tableInfo ->
//            for (Method method : methods) {
//                RequestMapping requestMapping = AnnotatedElementUtils.findMergedAnnotation(method, RequestMapping.class);
//                if (requestMapping != null) {
////                    def paths = pathsInClass.collect { pathInClass ->
////                        requestMapping.value().collect { path -> ('/' + (pathInClass - '/')) + ('/' + (path - '/'))
////                        }
////                    }.flatten()
//                    String[] paths = requestMapping.value().collect { it.replace('{table}', tableInfo.tableName) } as String[]
//                    requestMappingHandlerMapping.registerMapping(RequestMappingInfo
//                            .paths(paths)
//                            .methods(requestMapping.method())
//                            .produces(requestMapping.produces())
//                            .consumes(requestMapping.consumes())
//                            .headers(requestMapping.headers())
//                            .params(requestMapping.params())
//                            .mappingName(requestMapping.name())
//                            .build(),
//                            target,
//                            method);
//                }
//            }
//        }
//        println requestMappingHandlerMapping.getHandlerMethods();
//    }
//
//    static void main(String[] args) {
//        def pathsInClass = ['d', '/cc']
//        def paths = ['dfd', 'g']
//        paths = pathsInClass.collect { pathInClass ->
//            paths.collect { path -> ('/' + (pathInClass - '/')) + ('/' + (path - '/'))
//            }
//        }.flatten()
//
//        println paths
//
////        SysUser user = new SysUser(); user.setUsername("张三"); ExpressionParser parser = new SpelExpressionParser();
//        /** * 创建一个 StandardEvaluationContext实例,指定一个跟对象作为求值目标对象,这样在求值表达式中就可以引用根 * 对象属性.在求值内部可以使用反射机制从注册对象中获取相应的属性. */
////        EvaluationContext context = new StandardEvaluationContext(user);
////        String userName = (String) parser.parseExpression('${username}').getValue(context); System.out.println(userName);
//
//        //测试SpringEL解析器
//
//        String template = '${$name}，早上好';//设置文字模板,其中#{}表示表达式的起止，#user是表达式字符串，表示引用一个变量。
//
//        ExpressionParser paser = new SpelExpressionParser();//创建表达式解析器
//
//        //通过evaluationContext.setVariable可以在上下文中设定变量。
//
//        EvaluationContext context = new StandardEvaluationContext();
//
//        context.setVariable("name","Alex");
//
//        //解析表达式，如果表达式是一个模板表达式，需要为解析传入模板解析器上下文。
//
//        Expression expression = paser.parseExpression(template,new TemplateParserContext());
//
//        //使用Expression.getValue()获取表达式的值，这里传入了Evalution上下文，第二个参数是类型参数，表示返回值的类型。
//
//        System.out.println(expression.getValue(context,String.class));
//    }
//}
//
