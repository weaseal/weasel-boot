/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.lifecycle

import com.weasel.config.WeaselProperties
import net.dreamlu.mica.core.constant.MicaConstant
import net.dreamlu.mica.core.log.LogPrintStream
import net.dreamlu.mica.core.utils.SystemUtil
import org.dromara.hutool.core.text.StrUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.web.ServerProperties
import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import org.springframework.util.ClassUtils
import org.springframework.util.StringUtils
import org.ssssssss.magicapi.core.config.MagicAPIProperties
import org.ssssssss.magicapi.utils.PathUtils

import javax.annotation.Resource
import java.util.stream.Stream

/**
 * 输出服务访问地址
 *
 * @author weasel
 * @date 2023-3-13 12:08:59
 * @since 1.0.0
 */
@Component
@ConditionalOnProperty(name = "weasel.show-url", havingValue = "true", matchIfMissing = true)
class ApplicationUriPrinter implements CommandLineRunner {

    @Resource
    private Environment environment

    @Resource
    private ServerProperties serverProperties

    @Autowired
    private WeaselProperties weaselProperties

    @Autowired
    private MagicAPIProperties magicAPIProperties

    @Override
    void run(String... args) throws Exception {
        String ip = "IP"
        try {
            ip = InetAddress.localHost.hostAddress
        } catch (UnknownHostException e) {
            println "当前服务地址获取失败"
        }
        String magicWebPath = magicAPIProperties.web
        String schema = "http://"
        String appName = environment.getRequiredProperty(MicaConstant.SPRING_APP_NAME_KEY)
        int port = serverProperties.port
        String contextPath = serverProperties.servlet.contextPath ?: ''
        String profile = StringUtils.arrayToCommaDelimitedString(environment.getActiveProfiles())
        System.err.printf("---[%s]---启动完成，当前使用的端口:[%d]，环境变量:[%s]---%n", appName, port, profile)
        String localUrl = "$schema${PathUtils.replaceSlash("localhost:$port/$contextPath/${magicAPIProperties.prefix ?: ''}/")}"
        String externUrl = "$schema${PathUtils.replaceSlash("$ip:$port/$contextPath/${magicAPIProperties.prefix ?: ''}/")}"
        println """👌********************************************当前服务相关地址********************************************😀
👌服务启动成功! Access URLs:
    接口本地地址:          $localUrl
    接口外部地址:          $externUrl"""
        // 如果有 swagger，打印开发阶段的 swagger ui 地址
        if (hasOpenApi()) {
            println """    接口文档swagger地址:   ${localUrl}swagger-ui/index.html
    接口文档knife4j地址:   ${localUrl}doc.html"""
        }
        // 如果有 plumelog，打印开发阶段的 plumelog 地址
        if (hasPlumelog()) {
            println """    plumelog监控日志地址:  ${localUrl}plumelog/#/"""
        }
        // 如果有 druid，打印开发阶段的 druid 地址
        if (hasDruid()) {
            println """    druid数据库监控日志地址:${localUrl}druid/index.html"""
        }
        // 如果有 magic-api，打印开发阶段的 magic-api 地址
        if (hasMagicApi()) {
            println """${if (magicWebPath) "    接口配置平台:          ${localUrl}${StrUtil.replaceFirst(magicAPIProperties.web, '/', '', false)}/index.html"}"""
        }
        // 如果有 JimuReport，打印开发阶段的 JimuReport 地址
        if (hasJimuReport()) {
//            println """${if (magicWebPath) "    接口配置平台:          ${localUrl}${StrUtil.replaceFirst(magicAPIProperties.web, '/', '', false)}/index.html"}"""
            println """    积木报表地址:          ${localUrl}jmreport/list"""
        }
        // linux 上将全部的 System.err 和 System.out 替换为log
        if (SystemUtil.isLinux()) {
            System.setOut(LogPrintStream.log(false))
            // 去除 error 的转换，因为 error 会打印成很 N 条
            // System.setErr(LogPrintStream.log(true));
        }
        println """    可通过配置关闭输出:     weasel.show-url=false
👌********************************************当前服务相关地址********************************************😀"""
    }

    private static boolean hasOpenApi() {
        return Stream.of("springfox.documentation.spring.web.plugins.Docket", "io.swagger.v3.oas.models.OpenAPI")
                .anyMatch(clazz -> ClassUtils.isPresent(clazz, null))
    }

    private static boolean hasPlumelog() {
        ClassUtils.isPresent("com.plumelog.lite.autoconfigure.PlumelogLiteConfiguration", null)
    }

    private static boolean hasDruid() {
        ClassUtils.isPresent("com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure", null)
    }

    private static boolean hasMagicApi() {
        ClassUtils.isPresent("org.ssssssss.magicapi.spring.boot.starter.MagicAPIAutoConfiguration", null)
    }

    private static boolean hasJimuReport() {
        ClassUtils.isPresent("org.jeecg.modules.jmreport.config.init.JimuReportConfiguration", null)
    }
}
