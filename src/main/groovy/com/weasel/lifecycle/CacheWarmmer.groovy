/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.lifecycle

import com.fhs.trans.service.impl.DictionaryTransService
import com.weasel.common.consts.Consts
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component

import javax.annotation.Resource

@Component
class CacheWarmmer implements CommandLineRunner {

    @Resource
    private DictionaryTransService dictionaryTransService;

    @Override
    void run(String... args) throws Exception {
        Map<String, String> transMap = new HashMap<>();
        transMap.put("true", "已禁用");
        transMap.put("false", "已启用");
//        transMap.put("1","已禁用");
//        transMap.put("0","已启用");
        dictionaryTransService.refreshCache(Consts.AppCache.KEY_DISABLED, transMap)
//        Consts.AppCache.CACHE_MAP.forEach((k, v) -> {
//            dictionaryTransService.refreshCache(k, v);
//        });
    }
}
