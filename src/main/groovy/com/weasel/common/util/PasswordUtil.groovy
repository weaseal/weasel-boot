/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.util


import org.dromara.hutool.crypto.digest.DigestUtil

final class PasswordUtil {
    /** 摘要加密，支持md2、md5、sha1、sha256、sha512、sm3
     *  示例：digest("salt", 'password', 'sm3')
     * @param salt 盐
     * @param password 密码
     * @param algorithm 算法
     * @return
     */
    static String digest(String salt, String password, String algorithm) {
        DigestUtil.digester("${algorithm}").setSalt(salt.bytes).digestHex(password)
    }
}
