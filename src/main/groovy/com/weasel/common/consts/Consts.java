/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.consts;

import org.dromara.hutool.core.map.MapUtil;

import java.util.Map;

/**
 * @author weasel
 * @version 1.0
 * @date 2022/4/14 14:07
 */
public interface Consts {
    interface Common {
    }

    interface Profile {
        String DEV = "dev";
        String TEST = "test";
        String PROD = "prod";
    }

    interface Session {
        String CURRENT_USER = "CURRENT_USER";
        String CURRENT_TENANT = "CURRENT_TENANT";
    }

    interface Auth {
        String PERMIT_ALL = "*:*:*";
        String PERMIT_PRIVILEGED_TENANT = "privilegedTenant";
    }

    interface ValidateGroup {

        interface CreateGroup {
        }

        interface UpdateGroup {
        }

        interface DeleteGroup {
        }
    }

    interface DefaultSetting {

        Long DEFALUT_CREATOR = 1L;
    }

    interface LogRecordType {

        String LOGIN = "login";
        String CREATE = "create";
        String RETRIEVE = "retrieve";
        String UPDATE = "update";
        String DELETE = "delete";
    }

    interface AppCache {
        String KEY_SYS_CONFIG = "sysConfig:";
        String KEY_SYS_CONFIG$CAPTCHA_TYPE = "captchaType";
        String KEY_SYS_CONFIG$PASSWORD_RULE = "passwordRule";
        String KEY_DISABLED = "disabled";
        String KEY_FOREVER = "forever";
        String KEY_WEB_SOCKET_MESSAGE_CONFIG = "webSocketMessage:";

        Map<String, Map<String, String>> CACHE_MAP =
                MapUtil.<String, Map<String, String>>builder()
                        .put(
                                KEY_DISABLED,
                                MapUtil.<String, String>builder()
                                        .put("true", "已禁用")
                                        .put("false", "已启用")
                                        .build())
                        .put(
                                KEY_FOREVER,
                                MapUtil.<String, String>builder()
                                        .put("true", "永久")
                                        .put("false", "限期")
                                        .build())
                        .build();
    }
}
