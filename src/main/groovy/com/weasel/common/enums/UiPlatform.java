/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.enums;

import com.power4j.kit.common.data.dict.annotation.DictValue;
import com.power4j.kit.common.data.dict.annotation.Label;
import com.power4j.kit.common.data.dict.annotation.MapDict;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * UI框架
 *
 * @author weasel
 * @version 1.0
 * @date 2022/4/6 16:57
 */
@Getter
@AllArgsConstructor
@MapDict(code = "uiPlatform", name = "UI框架")
public enum UiPlatform {
    /**
     * 无需适配
     */
    @Label("无需适配")
    NONE("None"),
    /**
     * Soybean-Admin
     */
    @Label("Soybean-Admin")
    SOYBEAN("Soybean-Admin"),

    /**
     * Vben-Admin
     */
    @Label("Vben-Admin")
    VBEN("Vben-Admin"),

    /**
     * Pure-Admin
     */
    @Label("Pure-Admin")
    PURE("Pure-Admin"),

    /**
     * Pure-Admin
     */
    @Label("SCUI")
    SCUI("SCUI");

    //    @EnumValue
    @DictValue
    private final String value;
}
