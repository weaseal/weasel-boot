/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.enums;

import com.power4j.kit.common.data.dict.annotation.DictValue;
import com.power4j.kit.common.data.dict.annotation.Label;
import com.power4j.kit.common.data.dict.annotation.MapDict;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author weasel
 * @version 1.0
 * @date 2022/4/6 16:57
 * @descrption 多账号认证
 * @see <a href="https://sa-token.cc/doc.html#/up/many-account">sa-token</a>
 */
@Getter
@AllArgsConstructor
@MapDict(code = "loginType", name = "登录类型")
public enum LoginType {
    /**
     * 运维
     */
    @Label("运维")
    OPERATOR("OPERATOR", "运维"),

    /**
     * 用户
     */
    @Label("用户")
    USER("USER", "用户"),

    /**
     * 全部
     */
    @Label("会员")
    MEMBER("MEMBER", "会员");

    @DictValue
    private final String value;

    private final String description;

//    @Override
//    public String toString() {
//        return this.value;
//    }
}
