/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.enums;

import com.power4j.kit.common.data.dict.annotation.DictValue;
import com.power4j.kit.common.data.dict.annotation.Label;
import com.power4j.kit.common.data.dict.annotation.MapDict;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 数据权限范围
 *
 * @author weasel
 * @version 1.0
 * @date 2022/4/6 16:57
 */
@Getter
@AllArgsConstructor
@MapDict(code = "dataScope", name = "数据权限范围")
public enum DataScope {
    /**
     * 全部
     */
    @Label("全部")
    ALL("ALL", "全部"),

    /**
     * 本人
     */
    @Label("本人")
    SELF("SELF", "本人"),

    /**
     * 本人及子级
     */
    @Label("本人及子级")
    SELF_CHILDREN("SELF_CHILDREN", "本人及子级"),

    /**
     * 本级
     */
    @Label("本级")
    LEVEL("LEVEL", "本级"),

    /**
     * 本级及子级
     */
    @Label("本级及子级")
    LEVEL_CHILDREN("LEVEL_CHILDREN", "本级及子级"),

    /**
     * 自定义
     */
    @Label("自定义")
    CUSTOM("CUSTOM", "自定义");

    @DictValue
    private final String value;

    private final String description;

}
