/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.enums;

import com.power4j.kit.common.data.dict.annotation.DictValue;
import com.power4j.kit.common.data.dict.annotation.Label;
import com.power4j.kit.common.data.dict.annotation.MapDict;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@MapDict(code = "homePathUsing", name = "首页使用")
public enum HomePathUsing {
    /**
     * 租户
     */
    @Label("租户")
    TENANT("TENANT", "租户"),

    /**
     * 角色
     */
    @Label("角色")
    ROLE("ROLE", "角色"),

    /**
     * 部门
     */
    @Label("部门")
    DEPT("DEPT", "部门"),

    /**
     * 用户
     */
    @Label("用户")
    USER("USER", "用户");

    @DictValue
    private final String value;

    private final String description;

//    @Override
//    public String toString() {
//        return this.value;
//    }
}
