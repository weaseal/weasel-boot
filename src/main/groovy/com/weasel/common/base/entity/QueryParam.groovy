/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.base.entity

import com.mybatisflex.core.paginate.Page
/**
 * 查询参数，eg：
 * <pre>
<code>
 {
   "page":[{//排序支持ASC和DESC
       "pageNumber":1,
       "pageSize":10
   }],
   "sorters":[{//排序支持ASC和DESC
       "property":"userId",
       "direction":"DESC"
   }],
   "queries":[{//过滤条件 where sex=男 and (name=张三 or name=李四 )
         "property":"name", // po字段名
         "operator":"=",//操作符
         "value":"张三",//操作值
         "relation":"OR",//关联关系AND OR
         "group":"nameGroup"//相同的group 外层会加括号
   },
   {
         "property":"name",
         "operator":"=",
         "value":"李四",
         "relation":"OR",
         "group":"nameGroup"
   },{
         "property":"sex", //使用了默认的关联关系AND 以及默认操作符 =
         "value":"男"
   }]
 }
</code>
 *</pre>
 * @return
 */
class QueryParam<T> {
    Page<T> page
    List<Sorter> sorters = new ArrayList<>()
    List<Query> queries = new ArrayList<>()

    static class Sorter {
        String property
        String direction
    }

    static class Query {
        String property
        String operator = "="
        Object value
        String relation
        String group
    }
}
