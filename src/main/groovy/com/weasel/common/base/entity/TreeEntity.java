/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.base.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.*;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.RelationManyToOne;
import com.mybatisflex.annotation.RelationOneToMany;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author weasel
 * @version 1.0
 * @date 2022/7/26 9:21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@FieldNameConstants
// 与以下注解 JsonBackReference JsonManagedReference 必须同时存在，防止树形结构递归
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = BaseEntity.Fields.id)
public class TreeEntity<T extends TreeEntity<T>> extends BaseEntity {
    @ColumnDefine(comment = "父节点id")
    private Long parentId;
    //    @Column(ignore = true)
    @ExcelProperty("排序")
    @ColumnDefine(comment = "排序", notNull = true)
    @NotNull(message = "排序不能为空!")
    private Integer weight;
    @Column(ignore = true)
    private Integer level;
    @RelationManyToOne(selfField = "parentId", targetField = "id")
    @JsonBackReference
    private T parent;
    //    @Column(ignore = true)
    @RelationOneToMany(selfField = "id", targetField = "parentId")
    @JsonManagedReference
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<T> children;
}
