/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.base.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.constant.TransType;
import com.mybatisflex.annotation.Column;
import com.tangzc.autotable.annotation.Index;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.modules.sys.entity.SysTenant;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

/**
 * @author weasel
 * @version 1.0
 * @date 2022/7/26 9:21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@FieldNameConstants
public class TreeTenantEntity<T extends TreeEntity<T>> extends TreeEntity<T> {

    @ColumnDefine(comment = "租户id", notNull = true)
    @Index
    @Trans(type = TransType.SIMPLE, target = SysTenant.class, ref = "tenantName", fields = "name")
    private Long tenantId;

    @ExcelProperty("租户名称")
    @Column(ignore = true)
    private String tenantName;
}
