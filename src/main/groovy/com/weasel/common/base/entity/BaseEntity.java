/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.base.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.constant.TransType;
import com.fhs.core.trans.vo.TransPojo;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.core.keygen.KeyGenerators;
import com.tangzc.autotable.annotation.Index;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.common.consts.Consts;
import com.weasel.modules.sys.entity.SysUser;
import io.github.linpeilie.annotations.AutoMapping;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * @author weasel
 * @version 1.0
 * @date 2022/3/24 17:04
 */
@Data
@FieldNameConstants
public class BaseEntity/* extends com.tangzc.mpe.base.BaseEntity<Long, LocalDateTime>*/ implements TransPojo, Serializable {
    @ExcelIgnore
    @Id(keyType = KeyType.Generator, value = KeyGenerators.snowFlakeId)
    @NotNull(groups = {Consts.ValidateGroup.UpdateGroup.class})
    private Long id;
    @ExcelProperty("备注")
    @ColumnDefine(comment = "备注", length = 500)
    private String description;
    @ExcelIgnore
    @Column(version = true)
    @ColumnDefine(comment = "乐观锁版本号", defaultValue = "0", notNull = true)
    private Long version;
    @ExcelIgnore
    @Column(isLogicDelete = true)
    @ColumnDefine(comment = "是否删除", defaultValue = "0")
    @JsonIgnore
    @Index
    private Long deleted;
    @ExcelIgnore
    @ColumnDefine(comment = "创建人", notNull = true)
    @Trans(type = TransType.SIMPLE, target = SysUser.class, ref = "createByName", fields = "username")
    private Long createBy;
    @ExcelProperty("创建人")
    @Column(ignore = true)
    private String createByName;
    @ExcelProperty("创建时间")
    @Column(comment = "创建时间", onInsertValue = "now()")
    @ColumnDefine(comment = "创建时间", notNull = true)
    private LocalDateTime createTime;
    @ExcelIgnore
    @ColumnDefine(comment = "更新人")
//    @UpdateFillData(MybatisFlexConfig.WeaselAutoFillHandler.class)
    @Trans(type = TransType.SIMPLE, target = SysUser.class, ref = "updateByName", fields = "username")
    private Long updateBy;
    @ExcelProperty("更新人")
    @Column(ignore = true)
    private String updateByName;
    @ExcelProperty("更新时间")
    @Column(comment = "更新时间", onUpdateValue = "now()")
    @ColumnDefine(comment = "更新时间")
//    @UpdateFillTime
    private LocalDateTime updateTime;
    @Column(ignore = true)
    @ExcelIgnore
    @JsonIgnore
    @AutoMapping(ignore = true)
    private transient Map<String, Object> transMap = new HashMap<>();
}
