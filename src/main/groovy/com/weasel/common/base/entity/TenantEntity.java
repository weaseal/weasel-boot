/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.base.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.constant.TransType;
import com.mybatisflex.annotation.Column;
import com.tangzc.autotable.annotation.Index;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.modules.sys.entity.SysTenant;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

/**
 * @author weasel
 * @version 1.0
 * @date 2022/3/24 17:04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@FieldNameConstants
public class TenantEntity extends BaseEntity {
    // 不开启多租户时，租户id可为空
    @ExcelIgnore
    @Column(tenantId = true)
    @ColumnDefine(comment = "租户id", notNull = true)
    @Index
    @Trans(type = TransType.SIMPLE, target = SysTenant.class, fields = "name", ref = "tenantName")
    private Long tenantId;
    @ExcelProperty(value = "租户")
    @Column(ignore = true)
//    @RelationOneToOne(
//            selfField = "tenantId",
//            targetTable = "sys_tenant",
//            targetField = "id",
//            valueField = "name"
//    )
    private String tenantName;
//    @RelationOneToOne(
//            selfField = "tenantId",
//            targetField = "id"
//    )
//    //该处可以定义其他属性名，不一定要是目标对象的字段名
//    private String idNumberCustomFieldName;
}
