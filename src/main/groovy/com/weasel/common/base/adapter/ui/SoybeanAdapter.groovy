/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.base.adapter.ui


import com.weasel.common.enums.MenuType
import com.weasel.common.enums.UiPlatform
import com.weasel.config.AuthContext
import com.weasel.modules.sys.entity.SysMenu
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component

import javax.annotation.Resource

@ConditionalOnProperty(name = "weasel.ui-platform", havingValue = "soybean")
@Component
class SoybeanAdapter implements UiAdapter {
    @Resource
    AuthContext authContext

    @Override
    UiPlatform platform() {
        return UiPlatform.SOYBEAN
    }

    @Override
    Object getUserRoutes() {
        def routes = buildRoutes(authContext.getMenuList())
        [routes: routes, home: "home"]
//        [
//                [
//                        'name'     : 'exception',
//                        'path'     : '/exception',
//                        'component': 'layout.base',
//                        'meta'     : [
//                                'title'  : 'exception',
//                                'i18nKey': 'route.exception',
//                                'icon'   : 'ant-design:exception-outlined',
//                                'order'  : 7
//                        ],
//                        'children' : [
//                                [
//                                        'name'     : 'exception_403',
//                                        'path'     : '/exception/403',
//                                        'component': 'view.403',
//                                        'meta'     : [
//                                                'title'  : 'exception_403',
//                                                'i18nKey': 'route.exception_403',
//                                                'icon'   : 'ic:baseline-block'
//                                        ]
//                                ],
//                                [
//                                        'name'     : 'exception_404',
//                                        'path'     : '/exception/404',
//                                        'component': 'view.404',
//                                        'meta'     : [
//                                                'title'  : 'exception_404',
//                                                'i18nKey': 'route.exception_404',
//                                                'icon'   : 'ic:baseline-web-asset-off'
//                                        ]
//                                ],
//                                [
//                                        'name'     : 'exception_500',
//                                        'path'     : '/exception/500',
//                                        'component': 'view.500',
//                                        'meta'     : [
//                                                'title'  : 'exception_500',
//                                                'i18nKey': 'route.exception_500',
//                                                'icon'   : 'ic:baseline-wifi-off'
//                                        ]
//                                ]
//                        ]
//                ],
//                [
//                        'name'     : 'about',
//                        'path'     : '/about',
//                        'component': 'layout.base$view.about',
//                        'meta'     : [
//                                'title'  : 'about',
//                                'i18nKey': 'route.about',
//                                'icon'   : 'fluent:book-information-24-regular',
//                                'order'  : 10
//                        ]
//                ],
//                [
//                        'name'     : 'function',
//                        'path'     : '/function',
//                        'component': 'layout.base',
//                        'meta'     : [
//                                'title'  : 'function',
//                                'i18nKey': 'route.function',
//                                'icon'   : 'icon-park-outline:all-application',
//                                'order'  : 6
//                        ],
//                        'children' : [
//                                [
//                                        'name'    : 'function_hide-child',
//                                        'path'    : '/function/hide-child',
//                                        'meta'    : [
//                                                'title'  : 'function_hide-child',
//                                                'i18nKey': 'route.function_hide-child',
//                                                'icon'   : 'material-symbols:filter-list-off',
//                                                'order'  : 2
//                                        ],
//                                        'redirect': '/function/hide-child/one',
//                                        'children': [
//                                                [
//                                                        'name'     : 'function_hide-child_one',
//                                                        'path'     : '/function/hide-child/one',
//                                                        'component': 'view.function_hide-child_one',
//                                                        'meta'     : [
//                                                                'title'     : 'function_hide-child_one',
//                                                                'i18nKey'   : 'route.function_hide-child_one',
//                                                                'icon'      : 'material-symbols:filter-list-off',
//                                                                'hideInMenu': true,
//                                                                'activeMenu': 'function_hide-child'
//                                                        ]
//                                                ],
//                                                [
//                                                        'name'     : 'function_hide-child_three',
//                                                        'path'     : '/function/hide-child/three',
//                                                        'component': 'view.function_hide-child_three',
//                                                        'meta'     : [
//                                                                'title'     : 'function_hide-child_three',
//                                                                'i18nKey'   : 'route.function_hide-child_three',
//                                                                'hideInMenu': true,
//                                                                'activeMenu': 'function_hide-child'
//                                                        ]
//                                                ],
//                                                [
//                                                        'name'     : 'function_hide-child_two',
//                                                        'path'     : '/function/hide-child/two',
//                                                        'component': 'view.function_hide-child_two',
//                                                        'meta'     : [
//                                                                'title'     : 'function_hide-child_two',
//                                                                'i18nKey'   : 'route.function_hide-child_two',
//                                                                'hideInMenu': true,
//                                                                'activeMenu': 'function_hide-child'
//                                                        ]
//                                                ]
//                                        ]
//                                ],
//                                [
//                                        'name'     : 'function_multi-tab',
//                                        'path'     : '/function/multi-tab',
//                                        'component': 'view.function_multi-tab',
//                                        'meta'     : [
//                                                'title'     : 'function_multi-tab',
//                                                'i18nKey'   : 'route.function_multi-tab',
//                                                'icon'      : 'ic:round-tab',
//                                                'multiTab'  : true,
//                                                'hideInMenu': true,
//                                                'activeMenu': 'function_tab'
//                                        ]
//                                ],
//                                [
//                                        'name'     : 'function_request',
//                                        'path'     : '/function/request',
//                                        'component': 'view.function_request',
//                                        'meta'     : [
//                                                'title'  : 'function_request',
//                                                'i18nKey': 'route.function_request',
//                                                'icon'   : 'carbon:network-overlay',
//                                                'order'  : 3
//                                        ]
//                                ],
//                                [
//                                        'name'     : 'function_super-page',
//                                        'path'     : '/function/super-page',
//                                        'component': 'view.function_super-page',
//                                        'meta'     : [
//                                                'title'  : 'function_super-page',
//                                                'i18nKey': 'route.function_super-page',
//                                                'icon'   : 'ic:round-supervisor-account',
//                                                'order'  : 5
//                                        ]
//                                ],
//                                [
//                                        'name'     : 'function_tab',
//                                        'path'     : '/function/tab',
//                                        'component': 'view.function_tab',
//                                        'meta'     : [
//                                                'title'  : 'function_tab',
//                                                'i18nKey': 'route.function_tab',
//                                                'icon'   : 'ic:round-tab',
//                                                'order'  : 1
//                                        ]
//                                ],
//                                [
//                                        'name'     : 'function_toggle-auth',
//                                        'path'     : '/function/toggle-auth',
//                                        'component': 'view.function_toggle-auth',
//                                        'meta'     : [
//                                                'title'  : 'function_toggle-auth',
//                                                'i18nKey': 'route.function_toggle-auth',
//                                                'icon'   : 'ic:round-construction',
//                                                'order'  : 4
//                                        ]
//                                ]
//                        ]
//                ],
//                [
//                        'name'     : 'home',
//                        'path'     : '/home',
//                        'component': 'layout.base$view.home',
//                        'meta'     : [
//                                'title'  : 'home',
//                                'i18nKey': 'route.home',
//                                'icon'   : 'mdi:monitor-dashboard',
//                                'order'  : 1
//                        ]
//                ],
//                [
//                        'name'     : 'manage',
//                        'path'     : '/manage',
//                        'component': 'layout.base',
//                        'meta'     : [
//                                'title'  : 'manage',
//                                'i18nKey': 'route.manage',
//                                'icon'   : 'carbon:cloud-service-management',
//                                'order'  : 9
//                        ],
//                        'children' : [
//                                [
//                                        'name'     : 'manage_menu',
//                                        'path'     : '/manage/menu',
//                                        'component': 'view.manage_menu',
//                                        'meta'     : [
//                                                'title'    : 'manage_menu',
//                                                'i18nKey'  : 'route.manage_menu',
//                                                'icon'     : 'material-symbols:route',
//                                                'order'    : 3,
//                                                'keepAlive': true
//                                        ]
//                                ],
//                                [
//                                        'name'     : 'manage_role',
//                                        'path'     : '/manage/role',
//                                        'component': 'view.manage_role',
//                                        'meta'     : [
//                                                'title'  : 'manage_role',
//                                                'i18nKey': 'route.manage_role',
//                                                'icon'   : 'carbon:user-role',
//                                                'order'  : 2
//                                        ]
//                                ],
//                                [
//                                        'name'     : 'manage_user',
//                                        'path'     : '/manage/user',
//                                        'component': 'view.manage_user',
//                                        'meta'     : [
//                                                'title'  : 'manage_user',
//                                                'i18nKey': 'route.manage_user',
//                                                'icon'   : 'ic:round-manage-accounts',
//                                                'order'  : 1
//                                        ]
//                                ],
//                                [
//                                        'name'     : 'manage_user-detail',
//                                        'path'     : '/manage/user-detail/:id',
//                                        'component': 'view.manage_user-detail',
//                                        'props'    : true,
//                                        'meta'     : [
//                                                'title'     : 'manage_user-detail',
//                                                'i18nKey'   : 'route.manage_user-detail',
//                                                'hideInMenu': true,
//                                                'activeMenu': 'manage_user'
//                                        ]
//                                ]
//                        ]
//                ],
//                [
//                        'name'     : 'multi-menu',
//                        'path'     : '/multi-menu',
//                        'component': 'layout.base',
//                        'meta'     : [
//                                'title'  : 'multi-menu',
//                                'i18nKey': 'route.multi-menu',
//                                'order'  : 8
//                        ],
//                        'children' : [
//                                [
//                                        'name'    : 'multi-menu_first',
//                                        'path'    : '/multi-menu/first',
//                                        'meta'    : [
//                                                'title'  : 'multi-menu_first',
//                                                'i18nKey': 'route.multi-menu_first',
//                                                'order'  : 1
//                                        ],
//                                        'children': [
//                                                [
//                                                        'name'     : 'multi-menu_first_child',
//                                                        'path'     : '/multi-menu/first/child',
//                                                        'component': 'view.multi-menu_first_child',
//                                                        'meta'     : [
//                                                                'title'  : 'multi-menu_first_child',
//                                                                'i18nKey': 'route.multi-menu_first_child'
//                                                        ]
//                                                ]
//                                        ]
//                                ],
//                                [
//                                        'name'    : 'multi-menu_second',
//                                        'path'    : '/multi-menu/second',
//                                        'meta'    : [
//                                                'title'  : 'multi-menu_second',
//                                                'i18nKey': 'route.multi-menu_second',
//                                                'order'  : 2
//                                        ],
//                                        'children': [
//                                                [
//                                                        'name'    : 'multi-menu_second_child',
//                                                        'path'    : '/multi-menu/second/child',
//                                                        'meta'    : [
//                                                                'title'  : 'multi-menu_second_child',
//                                                                'i18nKey': 'route.multi-menu_second_child'
//                                                        ],
//                                                        'children': [
//                                                                [
//                                                                        'name'     : 'multi-menu_second_child_home',
//                                                                        'path'     : '/multi-menu/second/child/home',
//                                                                        'component': 'view.multi-menu_second_child_home',
//                                                                        'meta'     : [
//                                                                                'title'  : 'multi-menu_second_child_home',
//                                                                                'i18nKey': 'route.multi-menu_second_child_home'
//                                                                        ]
//                                                                ]
//                                                        ]
//                                                ]
//                                        ]
//                                ]
//                        ]
//                ],
//                [
//                        'name'     : 'user-center',
//                        'path'     : '/user-center',
//                        'component': 'layout.base$view.user-center',
//                        'meta'     : [
//                                'title'     : 'user-center',
//                                'i18nKey'   : 'route.user-center',
//                                'hideInMenu': true
//                        ]
//                ]
//        ]
    }

    @Override
    Object getConstantRoutes() {
        [
                [
                        "name"     : "login",
                        "path"     : "/login/:module(pwd-login|code-login|register|reset-pwd|bind-wechat)?",
                        "component": 'layout.blank$view.login',
                        "props"    : true,
                        "meta"     : [
                                "title"     : "login",
                                "i18nKey"   : "route.login",
                                "constant"  : true,
                                "hideInMenu": true
                        ]
                ],
                [
                        "name"     : "403",
                        "path"     : "/403",
                        "component": 'layout.blank$view.403',
                        "meta"     : [
                                "title"     : "403",
                                "i18nKey"   : "route.403",
                                "constant"  : true,
                                "hideInMenu": true
                        ]
                ],
                [
                        "name"     : "404",
                        "path"     : "/404",
                        "component": 'layout.blank$view.404',
                        "meta"     : [
                                "title"     : "404",
                                "i18nKey"   : "route.404",
                                "constant"  : true,
                                "hideInMenu": true
                        ]
                ],
                [
                        "name"     : "500",
                        "path"     : "/500",
                        "component": 'layout.blank$view.500',
                        "meta"     : [
                                "title"     : "500",
                                "i18nKey"   : "route.500",
                                "constant"  : true,
                                "hideInMenu": true
                        ]
                ]
        ]
    }

    static buildRoute(SysMenu it) {
        def route = [:]
        route << [
                name     : it?.title,
                path     : it?.path,
                component: it?.component,
//                id        : it?.id,
//                createTime: it?.createTime,
//                permission: it?.permission,
                children : it?.children,
//                redirect  : it?.redirect,
                meta     : [
                        order     : it?.weight,
                        title     : it?.title,
                        i18nKey   : it?.i18nKey,
//                        dynamicLevel       : it?.dynamicLevel,
                        keepAlive : !it?.ignoreKeepAlive,
                        icon      : it?.icon,
//                        frameSrc           : it?.frameSrc,
//                        hideBreadcrumb     : it?.hideBreadcrumb,
                        hideInMenu: it?.hideMenu,
//                        hideTab            : it?.hideTab,
//                        isLink             : it?.isLink,
//                        ignoreRoute        : it?.ignoreRoute,
//                        redirect           : it?.redirect,
//                        carryParam         : it?.carryParam,
//                        hidePathForChildren: it?.hidePathForChildren,
                        activeMenu: it?.currentActiveMenu,
                        multiTab  : it?.multiTab,
                ]
        ]
        route
    }

    static buildRoutes(List<SysMenu> menus) {
        def routes = []
        menus.findAll {
            it.type == MenuType.CATALOG
        }.each {
            it.children = getChildren4Route(it, menus)
            routes << buildRoute(it)
        }
        routes
    }

    /**
     * 递归查询子节点
     *
     * @param root 根节点
     * @param all 所有节点
     * @return 根节点信息
     */
    static getChildren4Route(SysMenu root, List<SysMenu> all) {
        def routes = []
        all.findAll {
            it.type == MenuType.MENU && it.parentId == root.id
        }.each {
            it.children = getChildren4Route(it, all)
            routes << buildRoute(it)
        }
        routes
    }
}
