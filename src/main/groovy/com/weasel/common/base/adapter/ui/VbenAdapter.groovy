/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.base.adapter.ui

import com.weasel.common.enums.UiPlatform
import com.weasel.config.AuthContext
import com.weasel.modules.sys.util.MenuUtil
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component

import javax.annotation.Resource

@ConditionalOnProperty(name = "weasel.ui-platform", havingValue = "vben")
@Component
class VbenAdapter implements UiAdapter {
    @Resource
    AuthContext authContext

    @Override
    UiPlatform platform() {
        return UiPlatform.VBEN
    }

    @Override
    Object getUserRoutes() {
        MenuUtil.buildRoutes(authContext.getMenuList())
    }

    @Override
    Object getConstantRoutes() {
        []
    }
}
