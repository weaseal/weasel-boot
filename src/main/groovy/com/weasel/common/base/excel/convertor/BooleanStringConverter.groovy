/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.base.excel.convertor

import com.alibaba.excel.metadata.GlobalConfiguration
import com.alibaba.excel.metadata.data.ReadCellData
import com.alibaba.excel.metadata.data.WriteCellData
import com.alibaba.excel.metadata.property.ExcelContentProperty
import com.fhs.trans.service.impl.DictionaryTransService
import org.dromara.hutool.core.util.BooleanUtil
import org.dromara.hutool.extra.spring.SpringUtil

/**
 * @author weasel
 * @version 1.0
 * @date 2022/5/12 9:29
 */
class BooleanStringConverter extends com.alibaba.excel.converters.booleanconverter.BooleanStringConverter {

    @Override
    Boolean convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        DictionaryTransService dictionaryTransService = SpringUtil.getBean(DictionaryTransService.class)
        String mapKey = dictionaryTransService.getMapKey(contentProperty.getField().getName(), cellData.getStringValue())
        return BooleanUtil.toBoolean(dictionaryTransService.getDictionaryTransMap().get(mapKey))
    }

    @Override
    WriteCellData<?> convertToExcelData(Boolean value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        DictionaryTransService dictionaryTransService = SpringUtil.getBean(DictionaryTransService.class)
        String mapKey = dictionaryTransService.getMapKey(contentProperty.getField().getName(), value.toString())
        return new WriteCellData(dictionaryTransService.getDictionaryTransMap().get(mapKey))
    }
}
