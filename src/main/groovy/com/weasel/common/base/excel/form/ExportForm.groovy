/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.base.excel.form

import com.alibaba.excel.support.ExcelTypeEnum

/**
 *
 * @author weasel
 * @date 2023/7/28 17:23
 * @version 1.0
 */
class ExportForm {
    List<Long> ids
    String[] onlySelect
    ScopeType scopeType
    String filename
    ExcelTypeEnum bookType

    enum ScopeType {
        all("全部"),
        filtered("已筛选"),
        selected("已勾选"),
        page("当前页");

        final String value

        ScopeType(String value) {
            this.value = value
        }
    }
}
