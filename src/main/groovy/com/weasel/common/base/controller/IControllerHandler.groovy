/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.base.controller

import com.mybatisflex.annotation.RelationOneToMany
import com.mybatisflex.annotation.RelationOneToOne
import com.mybatisflex.core.mybatis.Mappers
import com.mybatisflex.core.query.QueryChain
import com.mybatisflex.core.query.QueryWrapper
import com.mybatisflex.core.relation.RelationManager
import com.mybatisflex.core.table.TableInfoFactory
import com.tangzc.autotable.annotation.enums.IndexTypeEnum
import com.tangzc.autotable.core.utils.TableBeanUtils
import com.weasel.common.base.entity.BaseEntity
import com.weasel.common.base.entity.QueryParam
import com.weasel.common.base.exception.DataHasChangedException
import com.weasel.common.base.exception.DataHasRelatedException
import com.weasel.common.base.exception.DataUniqueUncheckedException
import org.dromara.hutool.core.lang.Assert
import org.dromara.hutool.core.util.ObjUtil
import org.dromara.hutool.extra.pinyin.PinyinUtil

import java.lang.reflect.Field
import java.util.function.Consumer

/**
 * @author weasel
 * @date 2022/7/25 8:50
 * @version 1.0
 */
trait IControllerHandler<T extends BaseEntity> {

    void initEntity(T entity) {

    }

    void initSaveEntity(T entity) {
        initEntity entity
        if (entity.hasProperty('code') && entity.hasProperty('name') && !entity.code && entity.name) {
            entity.code = PinyinUtil.getPinyin(entity.name, '')
        }
    }

    void initUpdateEntity(T entity) {
        initEntity entity
    }

    void checkUnique(T entity) {
        def tableInfo = TableInfoFactory.ofEntityClass(entity.class)
        def propertyColumnMapping = tableInfo.propertyColumnMapping
        QueryWrapper queryWrapper = QueryWrapper.create(entity.class.newInstance())
        TableBeanUtils.getTableIndexes(entity.class).findAll { it.type() == IndexTypeEnum.UNIQUE }.each { tableIndex ->
            for (final def tableIndexField in tableIndex.fields()) {
                def uniqueField = propertyColumnMapping.find { property, column ->
                    property == tableIndexField
                }
                // 唯一索引字段为空，则跳过
                if (uniqueField.key!== 'tenantId' &&uniqueField.key!== 'deleted' &&!entity[uniqueField.key]) {
                    return
                }
                queryWrapper.eq(uniqueField.value, entity[uniqueField.key])
            }
            tableInfo.primaryKeyList
                    && tableInfo.primaryKeyList.each { queryWrapper.ne(it.column, entity[it.property]) }
//            def count = Db.selectCountByQuery(queryWrapper)
            def count = Mappers.ofEntityClass(entity.class).selectCountByQuery(queryWrapper)
            Assert.isTrue(!count, () -> new DataUniqueUncheckedException())
        }
    }

    void checkUniques(List<T> entities) {
        Assert.notEmpty(entities)

        def entityType = entities[0].class
        def tableInfo = TableInfoFactory.ofEntityClass(entityType)
        def propertyColumnMapping = tableInfo.propertyColumnMapping
        def tableIndices = TableBeanUtils.getTableIndexes(entityType).findAll { it.type() == IndexTypeEnum.UNIQUE }

        QueryWrapper queryWrapper = QueryWrapper.create(entityType.newInstance())
        entities.each { entity ->
            queryWrapper.or({ wrapper ->
                tableIndices.each { tableIndex ->
                    for (final def tableIndexField in tableIndex.fields()) {
                        def uniqueField = propertyColumnMapping.find { property, column ->
                            property == tableIndexField
                        }
                        // 唯一索引字段为空，则跳过
                        if (uniqueField.key!== 'tenantId' &&uniqueField.key!== 'deleted' &&!entity[uniqueField.key]) {
                            return
                        }
                        wrapper.eq(uniqueField.value, entity[uniqueField.key])
                    }
                }
                tableInfo.primaryKeyList
                        && tableInfo.primaryKeyList.each { wrapper.ne(it.column, entity[it.property]) }

            } as Consumer<QueryWrapper>)

        }
//            def count = Db.selectCountByQuery(queryWrapper)
        def count = Mappers.ofEntityClass(entityType).selectCountByQuery(queryWrapper)
        Assert.isTrue(!count, () -> new DataUniqueUncheckedException())
    }

    void beforeQuery(QueryParam<T> queryParam) {
    }

    void beforeSave(T entity) {
        checkUnique(entity)
        initSaveEntity(entity)
    }

    void afterSave(T entity) {
    }

    void beforeSaveBatch(List<T> entities) {
        checkUniques(entities)
        entities.each { initSaveEntity(it) }
    }

    void afterSaveBatch(List<T> entities) {
        entities.each { afterSave(it) }
    }

    void beforeUpdate(T entity) {
        initUpdateEntity(entity)
        def tableInfo = TableInfoFactory.ofEntityClass(entity.class)
//        T dbEntity = Db.selectOneById(tableInfo.tableName, 'id', tableInfo.getPkValue(entity))
        T dbEntity = QueryChain.of(entity.class).eq('id', tableInfo.getPkValue(entity)).one()
        Long dbEntityVersion = dbEntity.getVersion()
//        GracefulResponse.wrapAssert { Assert.isTrue(ObjUtil.equals(dbEntityVersion, entity.getVersion()), "该数据已被更改，请刷新页面后重试！") }
//        GracefulResponse.wrapAssert { Assert.isTrue(ObjUtil.equals(dbEntityVersion, entity.getVersion()), () -> new DataHasChangedException()) }
        Assert.isTrue(ObjUtil.equals(dbEntityVersion, entity.getVersion()), () -> new DataHasChangedException())
//        Assert.isTrue(ObjUtil.equals(dbEntityVersion, entity.getVersion()), "该数据已被他人更改，请刷新页面后重试！")
        checkUnique(entity)
    }

    void afterUpdate(T entity) {
    }

    void beforeSaveOrUpdateBatch(List<T> entities) {
    }

    void afterSaveOrUpdateBatch(List<T> entities) {
    }

    void beforeRemove(T entity) {
        def mapper = Mappers.ofEntityClass(entity.class)
        List<Field> columnFields = TableInfoFactory.getColumnFields(entity.class)
        columnFields.findAll { it.getAnnotation(RelationOneToMany) || it.getAnnotation(RelationOneToOne) }.each { field ->
            RelationManager.maxDepth = 1
            def relationsById = mapper.selectOneWithRelationsById(entity.id)
            RelationManager.clearMaxDepth()
            Assert.isNull(relationsById[field.name], () -> new DataHasRelatedException())
        }
    }

    void afterRemove(T entity) {
    }

    void beforeRemoveBatch(List<T> entities) {
        if (entities) {
            def entity = entities.get(0)
            def entityType = entity.class
            def mapper = Mappers.ofEntityClass(entityType)
            def columnFields = TableInfoFactory.getColumnFields(entityType)
            columnFields.findAll { it.getAnnotation(RelationOneToMany) || it.getAnnotation(RelationOneToOne) }.each { field ->
                RelationManager.maxDepth = 1
                def list = mapper.selectListWithRelationsByQuery(QueryWrapper.create().eq(BaseEntity.Fields.id, entities*.id))
                RelationManager.clearMaxDepth()
                Assert.isNull(list*."${field.name}", () -> new DataHasRelatedException())
            }
        }
    }

    void afterRemoveBatch(List<T> entities) {
    }
}
