///*
// * Copyright (c) 2023-present weasel
// *    weasel-boot is licensed under Mulan PSL v2.
// *    You can use this software according to the terms and conditions of the Mulan PSL v2.
// *    You may obtain a copy of Mulan PSL v2 at:
// *                http://license.coscl.org.cn/MulanPSL2
// *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// *    See the Mulan PSL v2 for more details.
// */
//
//package com.weasel.common.base.controller
//
//import com.baomidou.mybatisplus.core.metadata.TableInfoHelper
//import com.mzt.logapi.starter.annotation.LogRecord
//import com.weasel.common.base.controller.IController
//import com.weasel.common.base.controller.IControllerHandler
//import com.weasel.common.base.excel.form.ExportForm
//import com.weasel.common.consts.Consts
//import groovy.util.logging.Slf4j
//import io.swagger.v3.oas.annotations.Operation
//import io.swagger.v3.oas.annotations.tags.Tag
//import org.dromara.hutool.core.bean.BeanUtil
//import org.dromara.hutool.core.convert.Convert
//import org.dromara.hutool.core.reflect.TypeReference
//import org.dromara.hutool.extra.spring.SpringUtil
//import org.springframework.core.ResolvableType
//import org.springframework.web.bind.annotation.*
//import org.springframework.web.multipart.MultipartFile
//
//import javax.servlet.http.HttpServletResponse
//
///**
// * 数据操作
// *
// * @author weasel
// * @date 2022/7/22 14:42
// * @version 1.0
// */
//@Tag(name = '数据操作')
//@Slf4j
//@RestController
//@RequestMapping('${weasel.dynamicRouterPrefix:/_d}')
//class DynamicController {
//
//    /**
//     * 查询列表
//     *
//     * @param table 表名
//     */
//    @Operation(summary = '查询列表', description = '根据筛选条件查询多条数据，支持分页')
//    @LogRecord(
//            success = "查询{{#table}},结果:{{#_ret}}",
//            type = Consts.LogRecordType.RETRIEVE, bizNo = "{{#table}}")
//    @PostMapping(['/{table}/list'])
//    def list(@PathVariable String table, @RequestBody params) {
//        def entityType = getEntityType(table)
//        list(entityType, params)
//    }
//
//    @GetMapping('/{table}/{id}')
//    def getById(@PathVariable String table, @PathVariable Long id) {
//        def entityType = getEntityType(table)
//        getById(entityType, id)
//    }
//
//    @PostMapping(['/{table}', '/{table}/save'])
//    def save(@PathVariable String table) {
//        def entityType = getEntityType(table)
//        def entity = getEntity(entityType)
//        IControllerHandler controllerHandler = getControllerHandler(entityType)
//        save(controllerHandler, entity)
//    }
//
//    @PostMapping('/{table}/saveBatch')
//    def saveBatch(@PathVariable String table, @RequestBody entities) {
//        def entityType = getEntityType(table)
//        IControllerHandler controllerHandler = getControllerHandler(entityType)
//        entities = Convert.toList(entityType, entities)
//        saveBatch(controllerHandler, entities)
//    }
//
//    @PutMapping('/{table}')
//    @PostMapping('/{table}/update')
//    def update(@PathVariable String table) {
//        def entityType = getEntityType(table)
//        def entity = getEntity(entityType)
//        IControllerHandler controllerHandler = getControllerHandler(entityType)
//        update(controllerHandler, entity)
//    }
//
//    @PostMapping('/{table}/saveOrUpdateBatch')
//    def saveOrUpdateBatch(@PathVariable String table, @RequestBody entities) {
//        def entityType = getEntityType(table)
//        IControllerHandler controllerHandler = getControllerHandler(entityType)
//        entities = Convert.toList(entityType, entities)
//        saveOrUpdateBatch(entityType, controllerHandler, entities)
//    }
//
//    @DeleteMapping('/{table}')
//    @PostMapping('/{table}/remove')
//    def remove(@PathVariable String table) {
//        def entityType = getEntityType(table)
//        def entity = getEntity(entityType)
//        IControllerHandler controllerHandler = getControllerHandler(entityType)
//        remove(controllerHandler, entity)
//    }
//
//    @DeleteMapping('/{table}/batch')
//    @PostMapping('/{table}/removeBatch')
//    def removeBatch(@PathVariable String table, @RequestBody def entities) {
//        def entityType = getEntityType(table)
//        IControllerHandler controllerHandler = getControllerHandler(entityType)
//        entities = Convert.toList(entityType, entities)
//        removeBatch(entityType, controllerHandler, entities)
//    }
//
//    @PostMapping('/{table}/export')
//    def export(@PathVariable String table, @RequestBody ExportForm exportForm, HttpServletResponse response) {
//        def entityType = getEntityType(table)
//        export(entityType, exportForm, response)
//    }
//
//    //private void writeData(def entityType, ExportForm exportForm, ExcelWriter excelWriter, WriteSheet writeSheet) {
//    //    def interval = StopWatch.of()
//    //    //def beanSearcher = SpringUtil.getBean(BeanSearcher)
//    //    BeanSearcherProperties.Params.Pagination pagination = beanSearcherProperties.getParams().getPagination()
//    //    String pageField = pagination.getPage()
//    //    String sizeField = pagination.getSize()
//    //    int page = pagination.getStart()
//    //    int size = pagination.maxAllowedSize
//    //    Map<String, Object> searchParams = [:]
//    //    searchParams.put('onlySelect', exportForm.onlySelect)
//    //    int exportedCount = 0
//    //    List list = []
//    //    Long lastId
//    //    do {
//    //        switch (exportForm.scopeType) {
//    //            case ExportForm.ScopeType.all:
//    //                searchParams.put(pageField, page)
//    //                searchParams.put(sizeField, size)
//    //
//    //                //QueryWrapper query = Wrappers.query().setEntityClass(entityType)
//    //                if (list) {
//    //                    lastId = list?.last()?.gmtCreate
//    //                    searchParams.put('gmtCreate', lastId)
//    //                    searchParams.put('gmtCreate-op', 'gt')
//    //                    //query.gt('gmtCreate', lastId)
//    //                }
//    //                list = beanSearcher.searchList(entityType, searchParams)
//    //                //list = Database.page(new Page(page, size).setSearchCount(false), query).records
//    //                //List list = []
//    //                //for (i in 0..size) {
//    //                //    def device = new DevDevice()
//    //                //    device.setDeviceName(RandomUtil.randomString(6))
//    //                //    device.setGmtCreate(RandomUtil.randomLong())
//    //                //    list << device
//    //                //}
//    //                //if (CollUtil.isNotEmpty(list)) {
//    //                //transService.transMore(list)
//    //                excelWriter.write(list, writeSheet)
//    //                //page++
//    //                //    //writeData(entityType, exportForm, excelWriter, writeSheet, page)
//    //                //} //else {
//    //                //    break
//    //                //}
//    //                break
//    //            case ExportForm.ScopeType.filtered:
//    //                searchParams.putAll(buildSearchParams(entityType))
//    //                searchParams.put(pageField, page)
//    //                searchParams.put(sizeField, size)
//    //                list = beanSearcher.searchList(entityType, searchParams)
//    //                if (CollUtil.isNotEmpty(list)) {
//    //                    transService.transMore(list)
//    //                    excelWriter.write(list, writeSheet)
//    //                    page++
//    //                    writeData(entityType, exportForm, excelWriter, writeSheet, page)
//    //                }
//    //                break
//    //            case ExportForm.ScopeType.selected:
//    //                def ids = getParamMap().ids
//    //                searchParams.put('id', ids)
//    //                searchParams.put('id-op', 'il')
//    //                list = beanSearcher.searchList(entityType, searchParams)
//    //                if (CollUtil.isNotEmpty(list)) {
//    //                    transService.transMore(list)
//    //                    excelWriter.write(list, writeSheet)
//    //                }
//    //                break
//    //            case ExportForm.ScopeType.page:
//    //                searchParams.putAll(buildSearchParams(entityType))
//    //                searchParams.put(pageField, page)
//    //                searchParams.put(sizeField, size)
//    //                list = beanSearcher.searchList(entityType, searchParams)
//    //                if (CollUtil.isNotEmpty(list)) {
//    //                    transService.transMore(list)
//    //                    excelWriter.write(list, writeSheet)
//    //                }
//    //                break
//    //        }
//    //
//    //        exportedCount += list.size()
//    //        println "导入成功${exportedCount}"
//    //
//    //    } while (list)
//    //    println interval.prettyPrint()
//    //}
//
//    @PostMapping('/{table}/import')
//    def 'import'(@PathVariable String table, MultipartFile file) {
//        def entityType = getEntityType(table)
//        IControllerHandler controllerHandler = getControllerHandler(entityType)
//        _import(entityType, controllerHandler, file)
//    }
//
//    protected Class getEntityType(String table) {
//        TableInfoHelper.getTableInfo(table).getEntityType()
//    }
//
//    protected def getEntity(Class entityType) {
//        BeanUtil.toBean(getParamMap(), entityType)
//    }
//
//    /**
//     * 获取控制器的处理器
//     * 参考hutool SpringUtil
//     * @see SpringUtil#getBean(TypeReference < T >)
//     * @param entityType 实体类型
//     * @return
//     */
//    protected static IControllerHandler getControllerHandler(Class entityType) {
//        final String[] beanNames = SpringUtil.getBeanFactory().getBeanNamesForType(ResolvableType.forClassWithGenerics(IControllerHandler, entityType))
//        beanNames ? SpringUtil.getBean(beanNames[0], IControllerHandler) : new IControllerHandler() {
//        }
//    }
//}
