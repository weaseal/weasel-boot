/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.base.controller

import com.alibaba.excel.EasyExcel
import com.alibaba.excel.ExcelWriter
import com.alibaba.excel.read.listener.PageReadListener
import com.alibaba.excel.write.metadata.WriteSheet
import com.mybatisflex.core.mybatis.Mappers
import com.mybatisflex.core.query.QueryColumn
import com.mybatisflex.core.query.QueryCondition
import com.mybatisflex.core.query.QueryWrapper
import com.mybatisflex.core.relation.RelationManager
import com.mybatisflex.core.row.Db
import com.mybatisflex.core.service.IService
import com.mybatisflex.core.table.TableInfoFactory
import com.weasel.common.base.entity.BaseEntity
import com.weasel.common.base.entity.PagedQueryWrapper
import com.weasel.common.base.entity.QueryParam
import com.weasel.common.base.entity.TreeEntity
import com.weasel.common.base.excel.form.ExportForm
import com.weasel.common.consts.Consts
import com.weasel.config.WeaselProperties
import groovy.util.logging.Slf4j
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.dromara.hutool.core.collection.CollUtil
import org.dromara.hutool.core.date.StopWatch
import org.dromara.hutool.core.net.url.UrlEncoder
import org.dromara.hutool.core.reflect.TypeUtil
import org.dromara.hutool.core.util.CharsetUtil
import org.dromara.hutool.extra.spring.SpringUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ResolvableType
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.multipart.MultipartFile

import javax.annotation.Resource
import javax.servlet.http.HttpServletResponse
import javax.validation.Validator
import java.nio.charset.StandardCharsets

/**
 * 控制器基类
 *
 * @author weasel
 * @date 2022/7/22 14:42
 * @version 1.0
 */
@Tag(name = '数据操作')
@Slf4j
class BaseController<S extends IService<T>, T extends BaseEntity> {
    @Resource
    WeaselProperties weaselProperties
    @Resource
    Validator validator
    // 注意：必须是@Autowired，@Resource不能精确找到bean
    @Autowired
    public S service
//    @Resource
//    ResponseFactory responseFactory

    Class<T> entityType = getEntityType()

    IControllerHandler<T> controllerHandler = getControllerHandler()

    Class<T> getEntityType() {
        TypeUtil.getTypeArgument(this.class, 1)
    }

    /**
     * 获取控制器的处理器
     * 参考hutool SpringUtil
     * @see SpringUtil#getBean(org.dromara.hutool.core.reflect.TypeReference < T >)
     * @param entityType 实体类型
     * @return
     */
    IControllerHandler getControllerHandler() {
        final String[] beanNames = SpringUtil.getBeanFactory().getBeanNamesForType(ResolvableType.forClassWithGenerics(IControllerHandler, entityType))
        beanNames ? SpringUtil.getBean(beanNames[0], IControllerHandler) : new DefaultControllerHandler<T>()
    }

    /**
     * 查询列表
     *
     * @param table 表名
     */
    @Operation(summary = '查询列表', description = '根据筛选条件查询多条数据，支持分页')
    @PostMapping('/list')
    Object list(@RequestBody(required = false) QueryParam<T> queryParam) {
        controllerHandler.beforeQuery(queryParam)
        PagedQueryWrapper searchParams = buildSearchParams(entityType, queryParam)
        def result

        if (TreeEntity.isAssignableFrom(entityType)) {
            def tableInfo = TableInfoFactory.ofEntityClass(entityType)
            searchParams.queryWrapper.isNull(tableInfo.getColumnByProperty(TreeEntity.Fields.parentId))
            RelationManager.addQueryRelations(TreeEntity.Fields.parentId, TreeEntity.Fields.children)
            def all = service.mapper.selectListWithRelationsByQuery(searchParams.queryWrapper)
            RelationManager.clearQueryRelations()
            switch (searchParams) {
                case { 'title' in it }:
                    all = all.findAll { it.title.contains(searchParams.title) }
                    break
                case { 'name' in it }:
                    all = all.findAll { it.name.contains(searchParams.name) }
                    break
                default:
                    break
            }
            result = all
        } else if (searchParams.getPage()) {
            def page = Mappers.ofEntityClass(entityType).paginate(searchParams.page, searchParams.queryWrapper)
            result = page
        } else {
            def all = Mappers.ofEntityClass(entityType).selectListByQuery(searchParams.queryWrapper)
            result = all
        }
        result
    }

    @GetMapping('/{id}')
    T getById(@PathVariable Long id) {
        service.mapper.selectOneWithRelationsById(id)
    }

    @PostMapping('/save')
    void save(@RequestBody T entity) {
        validator.validate(entity, Consts.ValidateGroup.CreateGroup)
        // step1: beforeSave
        controllerHandler.beforeSave(entity)
        // step2: save
        service.save(entity)
        // step3: afterSave
        controllerHandler.afterSave(entity)
    }

    @PostMapping('/saveBatch')
    void saveBatch(@RequestBody List<T> entities) {
        entities.each {
            validator.validate(it, Consts.ValidateGroup.CreateGroup)
        }
        // step1: beforeSaveBatch
        controllerHandler.beforeSaveBatch(entities)
        // step2: saveBatch
        service.saveBatch(entities)
        // step3: afterSaveBatch
        controllerHandler.afterSaveBatch(entities)
    }

    @PostMapping('/update')
    void update(@RequestBody T entity) {
        validator.validate(entity, Consts.ValidateGroup.UpdateGroup)
        // step1: beforeUpdate
        controllerHandler.beforeUpdate(entity)
        // step2: update
//        Mappers.ofEntityClass(entity.class).update(entity)
        service.updateById(entity)
        // step3: afterUpdate
        controllerHandler.afterUpdate(entity)
    }

    @PostMapping('/saveOrUpdateBatch')
    void saveOrUpdateBatch(@RequestBody List<T> entities) {
        // step1: beforeSaveOrUpdateBatch
        controllerHandler.beforeSaveOrUpdateBatch(entities)
        // step2: saveOrUpdateBatch
        service.saveOrUpdateBatch(entities)
        // step3: afterSaveOrUpdateBatch
        controllerHandler.afterSaveOrUpdateBatch(entities)
    }

    @PostMapping('/remove')
    void remove(@RequestBody T entity) {
        // step1: beforeDelete
        controllerHandler.beforeRemove(entity)
        // step2: delete
        service.removeById(entity.id)
        // step3: afterDelete
        controllerHandler.afterRemove(entity)
    }

    @PostMapping('/removeBatch')
    void removeBatch(@RequestBody List<T> entities) {
        // step1: beforeRemoveBatch
        controllerHandler.beforeRemoveBatch(entities)
        // step2: batchDelete
        service.removeByIds(entities*.id)
        // step3: afterRemoveBatch
        controllerHandler.afterRemoveBatch(entities)
    }

//    @ExcludeFromGracefulResponse
    @PostMapping('/export')
    void export(@RequestBody ExportForm exportForm, HttpServletResponse response) {
        try {
//            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
//            response.setCharacterEncoding("utf-8")
            // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = UrlEncoder.encodeAll(exportForm.filename)
            response.setHeader("Content-disposition", "attachment;filename=" + fileName)

            try (
                    ExcelWriter excelWriter = EasyExcel.write(response.getOutputStream(), entityType).autoCloseStream(Boolean.FALSE).excelType(exportForm.bookType).charset(CharsetUtil.UTF_8).build()
                    //if (exportForm.bookType == ExcelTypeEnum.CSV) {
                    //    excelWriterBuilder = excelWriterBuilder.charset(CharsetUtil.CHARSET_GBK)
                    //}
                    //excelWriter = excelWriterBuilder.build()
            ) {
                // 这里注意 如果同一个sheet只要创建一次
                WriteSheet writeSheet = EasyExcel.writerSheet()/*.includeColumnFieldNames(ListUtil.toList(exportForm.onlySelect))*/.build()
                writeData(entityType, exportForm, excelWriter, writeSheet)
//                int i = 1/0
            }
            //} finally {
            //    // 千万别忘记finish 会帮忙关闭流
            //    if (excelWriter != null) {
            //        excelWriter.finish()
            //    }
            //}
        } catch (Exception e) {
            e.printStackTrace()
            // 重置response
            response.reset()
            response.setContentType(MediaType.APPLICATION_JSON_VALUE)
            response.setCharacterEncoding(StandardCharsets.UTF_8.name())
//            response.getWriter().println(JSONUtil.toJsonStr(responseFactory.newFailInstance()))
        }
    }

    @PostMapping('/import')
    void 'import'(MultipartFile file) {
        EasyExcel.read(file.getInputStream(), entityType, { list ->
            service.saveOrUpdateBatch(list)
        } as PageReadListener<T>).sheet().doRead()
    }

    /**
     * 构建查询参数
     * @param entityType 实体类型
     * @param queryParam 前端按照指定格式传参
     * @return
     */
    PagedQueryWrapper buildSearchParams(def entityType, QueryParam<T> queryParam) {
        def queryCondition = QueryCondition.createEmpty()
        def tableInfo = TableInfoFactory.ofEntityClass(entityType)
        queryParam?.queries?.each {
            queryCondition.and(QueryCondition.create(new QueryColumn(tableInfo.getColumnByProperty(it.property)), it.operator, it.value))
        }
        def queryWrapper = QueryWrapper.create().where(queryCondition)
        queryParam?.sorters?.each {
            queryWrapper.orderBy(it.property, 'ASC'.equalsIgnoreCase(it.direction))
        }
        return new PagedQueryWrapper(page: queryParam?.page, queryWrapper: queryWrapper)
    }

    void writeData(Class entityType, ExportForm exportForm, ExcelWriter excelWriter, WriteSheet writeSheet) {
        def interval = StopWatch.of()
        int exportedCount = 0
        List list = []
        switch (exportForm.scopeType) {
            case ExportForm.ScopeType.all:
                Db.tx(() -> {
                    Mappers.ofEntityClass(entityType).selectCursorByQuery(QueryWrapper.create()).each {
                        excelWriter.write([it], writeSheet)
                        exportedCount++
                    }
                })
                break
            case ExportForm.ScopeType.selected:
                def ids = exportForm.ids
                list = Mappers.ofEntityClass(entityType).selectListByIds(ids)
                if (CollUtil.isNotEmpty(list)) {
                    excelWriter.write(list, writeSheet)
                }
                break
            case ExportForm.ScopeType.filtered:
                // todo 待实现
//                searchParams.putAll(buildSearchParams(entityType))
//                searchParams.put(pageField, page)
//                searchParams.put(sizeField, size)
//                list = Mappers.ofEntityClass(entityType).selectListByMap(searchParams)
//                if (CollUtil.isNotEmpty(list)) {
//                    excelWriter.write(list, writeSheet)
//                    page++
//                    writeData(entityType, exportForm, excelWriter, writeSheet, page)
//                }
                break
            case ExportForm.ScopeType.page:
                // todo 待实现
//                searchParams.putAll(buildSearchParams(entityType))
//                searchParams.put(pageField, page)
//                searchParams.put(sizeField, size)
//                list = Mappers.ofEntityClass(entityType).paginate(searchParams)
//                list = beanSearcher.searchList(entityType, searchParams)
//                if (CollUtil.isNotEmpty(list)) {
//                    excelWriter.write(list, writeSheet)
//                }
                break
        }

        exportedCount += list.size()
        println "导出成功${exportedCount}"

        println interval.prettyPrint()
    }
}

