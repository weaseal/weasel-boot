package com.weasel.common.base.controller
/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

//package com.weasel.common.base.controller
//
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.http.MediaType
//import org.springframework.web.bind.annotation.RequestMapping
//import org.springframework.web.bind.annotation.RequestMethod
//import org.springframework.web.servlet.mvc.method.RequestMappingInfo
//import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping
//import org.ssssssss.magicapi.core.model.ApiInfo
//import org.ssssssss.magicapi.core.service.MagicAPIService
//import org.ssssssss.magicapi.core.service.MagicResourceService
//
////@RestController
////@RequestMapping
//public class MagicApiController {
//    @Autowired
//    RequestMappingHandlerMapping requestMappingHandlerMapping;
//    @Autowired
//    MagicAPIService apiService;
//    @Autowired
//    MagicResourceService service;
//
//    @RequestMapping("ma/save")
//    Object save() {
////        service.saveGroup(new Group(name:  'default', type: 'api', parentId: '0'))
//        service.saveFile(new ApiInfo(groupId: '71fe0d423d4d43cda0c2d253a150c611', script: 'return 111', name: 'tt', path: 'tt'))
//    }
//
//    @RequestMapping("ma/get")
//    Object get() {
//        service.tree()
//    }
//
//    @RequestMapping("ma/execute")
//    Object execute() {
//        apiService.execute("GET", "/tt", [:]);
//    }
//
//    @RequestMapping("ma/handlerMapping")
//    Object handlerMapping() {
//        requestMappingHandlerMapping.registerMapping(
//                RequestMappingInfo.paths("/sys_role").methods(RequestMethod.GET)
//                        .produces(MediaType.APPLICATION_JSON_VALUE).build(),
//                CC.class,
//                // Method to be executed when above conditions apply, i.e.: when HTTP
//                // method and URL are called)
//                CC.class.getDeclaredMethod("list1"));
//    }
//}
