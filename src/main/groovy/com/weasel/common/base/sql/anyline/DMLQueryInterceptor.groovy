/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.common.base.sql.anyline

import org.anyline.data.interceptor.QueryInterceptor
import org.anyline.data.param.ConfigStore
import org.anyline.data.prepare.RunPrepare
import org.anyline.data.run.Run
import org.anyline.data.runtime.DataRuntime
import org.anyline.entity.DataRow
import org.anyline.entity.DataSet
import org.anyline.entity.PageNavi
import org.anyline.metadata.ACTION
import org.springframework.stereotype.Component


@Component
class DMLQueryInterceptor implements QueryInterceptor {

//    @Override
//    ACTION.SWITCH before(DataRuntime runtime, String random, Run run, PageNavi navi) {
//        return super.before(runtime, random, run, navi)
//    }

    @Override
    ACTION.SWITCH prepare(DataRuntime runtime, String random, RunPrepare prepare, ConfigStore configs, String... conditions) {
        configs.eq("tenant_id", 0)
        return ACTION.SWITCH.CONTINUE
    }

//    @Override
//    ACTION.SWITCH after(DataRuntime runtime, String random, Run run, boolean success, Object result, PageNavi navi, long millis){
//        String sql = run.getFinalQuery()
//        if(result instanceof DataRow){
//            DataRow row = (DataRow) result
//            row.put("ROW_ID", System.currentTimeMillis())
//        }else if(result instanceof DataSet){
//            DataSet set = (DataSet) result
//            set.put("ROW_ID", System.currentTimeMillis())
//        }
//        System.out.println(sql)
//        System.out.println(result)
//        return ACTION.SWITCH.CONTINUE
//    }


}
