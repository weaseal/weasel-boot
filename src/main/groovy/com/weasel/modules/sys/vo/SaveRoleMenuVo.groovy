/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.vo

import org.apache.poi.xwpf.usermodel.XWPFDocument
import org.apache.poi.xwpf.usermodel.XWPFParagraph

class SaveRoleMenuVo {
    Long roleId
    List<Long> menuIds

    static void convert(File inputFile, OutputStream outputStream) throws IOException {
        try (FileInputStream fis = new FileInputStream(inputFile)
             XWPFDocument document = new XWPFDocument(fis)) {
            StringBuilder htmlBuilder = new StringBuilder()
            htmlBuilder.append("<html><body>")

            // 转换每个段落
            List<XWPFParagraph> paragraphs = document.getParagraphs()
            for (XWPFParagraph para : paragraphs) {
                // 将段落内容转换为HTML
                String text = para.getText()
                htmlBuilder.append("<p>").append(text).append("</p>")
            }

            htmlBuilder.append("</body></html>")

            // 将HTML写入输出流
            outputStream.write(htmlBuilder.toString().getBytes())
            outputStream.flush()
        }
    }

    static void main(String[] args) {
//        try {
//            File inputFile = new File("C:\\Users\\40284\\Desktop\\承诺书.docx")
//            File outputFile = new File("C:\\Users\\40284\\Desktop\\承诺书a.html")
//            convert(inputFile, new FileOutputStream(outputFile))
//        } catch (IOException e) {
//            e.printStackTrace()
//        }
    }
}
