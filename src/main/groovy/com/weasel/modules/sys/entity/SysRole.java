/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.constant.TransType;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.RelationManyToMany;
import com.mybatisflex.annotation.Table;
import com.tangzc.autotable.annotation.TableIndex;
import com.tangzc.autotable.annotation.TableIndexes;
import com.tangzc.autotable.annotation.enums.IndexTypeEnum;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.common.base.entity.BaseEntity;
import com.weasel.common.base.entity.TreeTenantEntity;
import com.weasel.common.base.excel.convertor.BooleanStringConverter;
import com.weasel.common.consts.Consts;
import com.weasel.common.enums.DataScope;
import com.weasel.modules.sys.excel.convertor.DataScopeStringConverter;
import com.weasel.modules.sys.listener.TenantIdOnSetListener;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@FieldNameConstants
@Table(value = "sys_role", onSet = TenantIdOnSetListener.class, comment = "角色表")
@TableIndexes({
        @TableIndex(name = "uni_tenant_id_deleted_name", fields = {TreeTenantEntity.Fields.tenantId, BaseEntity.Fields.deleted, SysRole.Fields.name}, type = IndexTypeEnum.UNIQUE),
        @TableIndex(name = "uni_tenant_id_deleted_code", fields = {TreeTenantEntity.Fields.tenantId, BaseEntity.Fields.deleted, SysRole.Fields.code}, type = IndexTypeEnum.UNIQUE),
})
public class SysRole extends TreeTenantEntity<SysRole> {
    @ExcelProperty("名称")
    @ColumnDefine(comment = "名称", notNull = true)
    @NotBlank(message = "名称不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String name;
    @ExcelProperty("编码")
    @ColumnDefine(comment = "编码", notNull = true, length = 500)
    @NotBlank(message = "编码不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String code;
    @ExcelProperty("类型")
    @ColumnDefine(comment = "是否系统内置角色", notNull = true, defaultValue = "0")
    @NotNull(message = "类型不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private Boolean internal;
    @ExcelProperty("排序")
    @ColumnDefine(comment = "排序", notNull = true)
    @NotNull(message = "排序不能为空!")
    private Integer weight;
    @ExcelProperty(value = "首页")
    @ColumnDefine(comment = "首页", length = 2000)
    private String homePath;
    @ExcelProperty(value = "状态", converter = BooleanStringConverter.class)
    @ColumnDefine(comment = "是否禁用", notNull = true, defaultValue = "0")
    @Trans(type = TransType.DICTIONARY, key = Consts.AppCache.KEY_DISABLED, ref = "disabledName")
    private Boolean disabled;
    @ExcelIgnore
    @Column(ignore = true)
    private String disabledName;
    //    @ExcelIgnore
    @ExcelProperty(value = "数据权限", converter = DataScopeStringConverter.class)
    @ColumnDefine(comment = "数据权限", notNull = true, defaultValue = "SELF")
    @Trans(type = TransType.ENUM, key = "description", ref = "dataScopeName")
    @NotNull(message = "数据权限不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private DataScope dataScope;
    @ExcelIgnore
//    @ExcelProperty("数据权限")
    @Column(ignore = true)
    private String dataScopeName;

    @RelationManyToMany(
            joinTable = "sys_role_menu", // 中间表
            selfField = "id",
            joinSelfColumn = "role_id",
            targetTable = "sys_menu",
            targetField = "id",
            joinTargetColumn = "menu_id",
            valueField = "id",
            // 过滤出叶子节点
            // 这个查询的工作原理是：
            // 1.使用LEFT JOIN将每个节点与所有可能的子节点进行连接尝试。
            // 2.通过t2.id IS NULL条件筛选出那些没有匹配到子节点的记录，这些记录就是叶子节点
            extraCondition = "id in (SELECT t1.id\n" +
                    "FROM sys_menu t1\n" +
                    "LEFT JOIN sys_menu t2 ON t1.id = t2.parent_id\n" +
                    "WHERE t2.id IS NULL)"
    )
    private List<Long> menuIds;
}
