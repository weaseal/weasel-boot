/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.entity;

import com.mybatisflex.annotation.Table;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.common.base.entity.TenantEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Table(value = "sys_log", comment = "日志表"/*dsName = "shardingsphere", */)
public class SysLog extends TenantEntity {
    /**
     * @return 方法执行成功后的日志模版
     */
    @ColumnDefine(comment = "方法执行成功后的日志模版")
    String success;

    /**
     * @return 方法执行失败后的日志模版
     */
    @ColumnDefine(comment = "方法执行失败后的日志模版")
    String fail;

    /**
     * @return 日志的操作人
     */
    @ColumnDefine(comment = "日志的操作人")
    String operator;

    /**
     * @return 操作日志的类型，比如：订单类型、商品类型
     */
    @ColumnDefine(comment = "操作日志的类型，比如：订单类型、商品类型")
    String type;

    /**
     * @return 日志的子类型，比如订单的C端日志，和订单的B端日志，type都是订单类型，但是子类型不一样
     */
    @ColumnDefine(comment = "日志的子类型，比如订单的C端日志，和订单的B端日志，type都是订单类型，但是子类型不一样")
    String subType;

    /**
     * @return 日志绑定的业务标识
     */
    @ColumnDefine(comment = "日志绑定的业务标识")
    String bizNo;

    /**
     * @return 日志的额外信息
     */
    String extra;
}
