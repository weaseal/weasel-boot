/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.anno.TransDefaultSett;
import com.fhs.core.trans.constant.TransType;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import com.tangzc.autotable.annotation.TableIndex;
import com.tangzc.autotable.annotation.TableIndexes;
import com.tangzc.autotable.annotation.enums.IndexTypeEnum;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.common.base.entity.BaseEntity;
import com.weasel.common.base.entity.TenantEntity;
import com.weasel.common.base.excel.convertor.BooleanStringConverter;
import com.weasel.common.consts.Consts;
import com.weasel.common.enums.HomePathUsing;
import com.weasel.modules.sys.vo.ComponentVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@Table(value = "sys_user", comment = "用户表")
@TableIndexes({
        @TableIndex(name = "tenant_id_deleted_username", fields = {TenantEntity.Fields.tenantId, BaseEntity.Fields.deleted, "username"}, type = IndexTypeEnum.UNIQUE),
})
@TransDefaultSett(isUseCache = true, cacheSeconds = 5L, maxCache = 1000)
//@SearchBean(tables = "sys_user u " +
//        "left join sys_user_dept ud on u.id=ud.user_id left join sys_dept d on d.id=ud.dept_id " +
//        "left join sys_user_role ur on u.id=ur.user_id left join sys_role r on r.id=ur.role_id ", distinct = true, autoMapTo = "u")
public class SysUser extends TenantEntity {
    @ColumnDefine(comment = "账号", notNull = true)
    @NotBlank(message = "账号不能为空!")
    private String username;
    @ColumnDefine(comment = "真实姓名")
    private String realName;
    @ColumnDefine(comment = "密码", notNull = true)
    @NotBlank(message = "密码不能为空!")
    @JsonIgnore
    private String password;
    @ColumnDefine(comment = "密码盐", notNull = true)
    @JsonIgnore
    private String salt;
    @ColumnDefine(comment = "头像", length = 2000)
    private String avatar;
    @ColumnDefine(comment = "生日")
    @Past
    private Date birthday;
    @ColumnDefine(comment = "性别(0-默认未知,1-男,2-女)")
    @Trans(type = TransType.DICTIONARY, key = "sex", ref = "sexName")
    private Integer sex;
    @ExcelIgnore
//    @ExcelProperty("性别")
    @Column(ignore = true)
    private String sexName;
    @ColumnDefine(comment = "电子邮件")
    @Email(message = "邮箱格式不正确！", groups = {Consts.ValidateGroup.UpdateGroup.class, Consts.ValidateGroup.CreateGroup.class})
    private String email;
    @ColumnDefine(comment = "手机号码")
    @Pattern(regexp = "(?:0|86|\\+86)?1[3-9]\\d{9}", message = "手机号码格式不正确！", groups = {Consts.ValidateGroup.UpdateGroup.class, Consts.ValidateGroup.CreateGroup.class})
    private String phone;
    @ExcelProperty(value = "状态", converter = BooleanStringConverter.class)
    @ColumnDefine(comment = "是否禁用", notNull = true, defaultValue = "0")
    @Trans(type = TransType.DICTIONARY, key = Consts.AppCache.KEY_DISABLED, ref = "disabledName")
    private Boolean disabled;
    @ExcelIgnore
    @Column(ignore = true)
    private String disabledName;
    @ColumnDefine(comment = "首页", length = 2000)
    private String homePath;
    @ColumnDefine(comment = "首页使用", length = 2000)
    private HomePathUsing homePathUsing;

    @Column(ignore = true)
    private List<Long> roleIds;

    @Column(ignore = true)
    private List<SysRole> roles;

    @Column(ignore = true)
    private List<ComponentVo> depts;

}
