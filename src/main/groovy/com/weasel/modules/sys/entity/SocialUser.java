/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.entity;

import com.mybatisflex.annotation.Table;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.common.base.entity.TenantEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@Table(value = "social_user", comment = "社会化用户表")
public class SocialUser extends TenantEntity {

    @ColumnDefine(comment = "第三方系统的唯一ID", notNull = true)
    String uuid;
    @ColumnDefine(comment = "第三方用户来源", notNull = true)
    String source;
    @ColumnDefine(comment = "用户的授权令牌", notNull = true)
    String accessToken;
    @ColumnDefine(comment = "第三方用户的授权令牌的有效期")
    Integer expireIn;
    @ColumnDefine(comment = "刷新令牌")
    String refreshToken;
    @ColumnDefine(comment = "第三方用户的 open id")
    String openId;
    @ColumnDefine(comment = "第三方用户的 ID")
    String uid;
    @ColumnDefine(comment = "个别平台的授权信息")
    String accessCode;
    @ColumnDefine(comment = "第三方用户的 union id")
    String unionId;
    @ColumnDefine(comment = "第三方用户授予的权限")
    String scope;
    @ColumnDefine(comment = "个别平台的授权信息")
    String tokenType;
    @ColumnDefine(comment = "token")
    String idToken;
    @ColumnDefine(comment = "小米平台用户的附带属性")
    String macAlgorithm;
    @ColumnDefine(comment = "小米平台用户的附带属性")
    String macKey;
    @ColumnDefine(comment = "用户的授权code")
    String code;
    @ColumnDefine(comment = "Twitter平台用户的附带属性")
    String oauthToken;
    @ColumnDefine(comment = "Twitter平台用户的附带属性")
    String oauthTokenSecret;
}
