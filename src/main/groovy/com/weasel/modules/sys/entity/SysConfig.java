/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.anno.TransDefaultSett;
import com.fhs.core.trans.constant.TransType;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import com.tangzc.autotable.annotation.TableIndex;
import com.tangzc.autotable.annotation.TableIndexes;
import com.tangzc.autotable.annotation.enums.IndexTypeEnum;
import com.tangzc.autotable.annotation.mysql.MysqlTypeConstant;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.common.base.entity.BaseEntity;
import com.weasel.common.base.entity.TenantEntity;
import com.weasel.common.consts.Consts;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = true)
@Table(value = "sys_config", comment = "配置表")
@TableIndexes({
        @TableIndex(name = "uni_tenant_id_deleted_name", fields = {TenantEntity.Fields.tenantId, BaseEntity.Fields.deleted, "name"}, type = IndexTypeEnum.UNIQUE),
        @TableIndex(name = "uni_tenant_id_deleted_code", fields = {TenantEntity.Fields.tenantId, BaseEntity.Fields.deleted, "code"}, type = IndexTypeEnum.UNIQUE),
})
@TransDefaultSett(isUseCache = true)
public class SysConfig extends TenantEntity {
    @ExcelProperty(value = "名称")
    @ColumnDefine(comment = "名称", notNull = true, length = 500)
    @NotBlank(message = "名称不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String name;
    @ExcelProperty(value = "编码")
    @ColumnDefine(comment = "编码", notNull = true, length = 500)
    @NotBlank(message = "编码不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String code;
    @ExcelIgnore
    @ColumnDefine(comment = "数据类型", notNull = true)
    @Trans(type = TransType.SIMPLE, target = SysDict.class, fields = "name", ref = "dataTypeName")
    private Long dataType;
    @ExcelProperty(value = "数据类型")
    @Column(ignore = true)
    private transient String dataTypeName;
    @ExcelProperty(value = "值")
    @ColumnDefine(comment = "值", type = MysqlTypeConstant.TEXT)
    private String val;
    @ExcelProperty(value = "排序")
    @ColumnDefine(comment = "排序")
    private Integer weight;
    @ExcelIgnore
    @ColumnDefine(comment = "是否禁用", notNull = true, defaultValue = "0")
    @Trans(type = TransType.DICTIONARY, key = Consts.AppCache.KEY_DISABLED, ref = "disabledName")
    private Boolean disabled;
    @ExcelProperty(value = "状态")
    @Column(ignore = true)
    private String disabledName;
}
