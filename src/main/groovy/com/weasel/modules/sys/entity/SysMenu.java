/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.entity;

import com.mybatisflex.annotation.Table;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.common.base.entity.TreeEntity;
import com.weasel.common.base.entity.TreeTenantEntity;
import com.weasel.common.enums.MenuType;
import com.weasel.common.enums.UiPlatform;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@Table(value = "sys_menu", comment = "菜单表")
public class SysMenu extends TreeEntity<SysMenu> {
    @ColumnDefine(comment = "UI框架", length = 50)
    private UiPlatform uiPlatform;
    @ColumnDefine(comment = "国际化键", length = 100)
    private String i18nKey;
    @ColumnDefine(comment = "权限标识", length = 500)
    private String permission;
    @ColumnDefine(comment = "类型: CATALOG 目录,MENU 菜单,BUTTON 按钮", notNull = true)
    @NotNull(message = "类型不能为空!")
    private MenuType type;
    @ColumnDefine(comment = "是否禁用", notNull = true, defaultValue = "0")
    private Boolean disabled;
    @ColumnDefine(comment = "路由地址", length = 500)
    private String path;
    @ColumnDefine(comment = "组件路径", length = 500)
    private String component;
    @ColumnDefine(comment = "标题", length = 500, notNull = true)
//    @Unique(columns = {"tenant_id", "deleted", "parent_id", "title"})
//    @TableField(condition = SqlCondition.LIKE)
    @NotBlank(message = "标题不能为空!")
    private String title;
    @ColumnDefine(comment = "获取动态路由打开数，超过 0 即代表需要控制打开数")
    private Integer dynamicLevel;
    @ColumnDefine(comment = "是否忽略缓存", defaultValue = "1")
    private Boolean ignoreKeepAlive;
    @ColumnDefine(comment = "图标")
    private String icon;
    @ColumnDefine(comment = "外链地址", length = 500)
    private String frameSrc;
    @ColumnDefine(comment = "是否隐藏面包屑", defaultValue = "0")
    private Boolean hideBreadcrumb;
    @ColumnDefine(comment = "是否隐藏", defaultValue = "0")
    private Boolean hideMenu;
    @ColumnDefine(comment = "是否隐藏标签页", defaultValue = "0")
    private Boolean hideTab;
    @ColumnDefine(comment = "是否外链", defaultValue = "0")
    private Boolean isLink;
    @ColumnDefine(comment = "是否忽略路由", defaultValue = "0")
    private Boolean ignoreRoute;
    @ColumnDefine(comment = "跳转路由")
    private String redirect;
    @ColumnDefine(comment = "如果该路由会携带参数，且需要在tab页上面显示。则需要设置为true")
    private Boolean carryParam;
    @ColumnDefine(comment = "是否在子级菜单的完整path中忽略本级path。2.5.3以上版本有效")
    private Boolean hidePathForChildren;
    @ColumnDefine(comment = "当前激活的菜单。用于配置详情页时左侧激活的菜单路径")
    private String currentActiveMenu;
    @ColumnDefine(comment = "Soybean:默认情况下，相同路径的路由会共享一个标签页，若设置为true，则使用多个标签页")
    private Boolean multiTab;

    /**
     * 以下btn开头的属性为按钮属性
     */
    @ColumnDefine(comment = "按钮位置：INLINE | TOOLBAR | BATCH")
    private String btnPosition;
    @ColumnDefine(comment = "按钮类型：新增|编辑|删除|详情 等，对应===POST /{id}===DELETE /{id}===GET /{id}===PUT 等")
    private String btnAction;

    @ColumnDefine(comment = "按钮单击事件")
    private String btnOnClick;
    @ColumnDefine(comment = "按钮标签")
    private String btnLabel;
    @ColumnDefine(comment = "按钮颜色：success|error|warning")
    private String btnColor;
    @ColumnDefine(comment = "按钮弹出确认框事件")
    private String btnPopConfirm;
    @ColumnDefine(comment = "按钮后是否显示分隔符")
    private String btnDivider;
    @ColumnDefine(comment = "按钮是否显示")
    private String btnIfShow;
    @ColumnDefine(comment = "按钮提示字符")
    private String btnTooltip;
}
