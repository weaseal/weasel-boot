/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.anno.TransDefaultSett;
import com.fhs.core.trans.constant.TransType;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import com.tangzc.autotable.annotation.TableIndex;
import com.tangzc.autotable.annotation.TableIndexes;
import com.tangzc.autotable.annotation.enums.IndexTypeEnum;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.common.base.entity.BaseEntity;
import com.weasel.common.base.entity.TenantEntity;
import com.weasel.common.base.entity.TreeTenantEntity;
import com.weasel.common.base.excel.convertor.BooleanStringConverter;
import com.weasel.common.consts.Consts;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@FieldNameConstants
@Table(value = "sys_dict", comment = "字典表")
@TableIndexes({
        @TableIndex(name = "uni_tenant_id_deleted_name", fields = {TreeTenantEntity.Fields.tenantId, BaseEntity.Fields.deleted, SysDict.Fields.name}, type = IndexTypeEnum.UNIQUE),
})
@TransDefaultSett(isUseCache = true, cacheSeconds = 5L, maxCache = 1000)
public class SysDict extends TenantEntity {
    @ExcelProperty("名称")
    @ColumnDefine(comment = "名称", notNull = true)
    @NotBlank(message = "名称不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String name;
    @ExcelProperty("值")
    @ColumnDefine(comment = "值", notNull = true, length = 500)
    @NotBlank(message = "值不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String val;
    @ExcelProperty("字典类型")
    @ColumnDefine(comment = "字典类型id", notNull = true, defaultValue = "0")
    @NotNull(message = "字典类型不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private Long typeId;
    @ExcelProperty("排序")
    @ColumnDefine(comment = "排序", notNull = true)
    @NotNull(message = "排序不能为空!")
    private Integer weight;
    @ExcelProperty(value = "状态", converter = BooleanStringConverter.class)
    @ColumnDefine(comment = "是否禁用", notNull = true, defaultValue = "0")
    @Trans(type = TransType.DICTIONARY, key = Consts.AppCache.KEY_DISABLED, ref = "disabledName")
    private Boolean disabled;
    @ExcelIgnore
    @Column(ignore = true)
    private String disabledName;
}
