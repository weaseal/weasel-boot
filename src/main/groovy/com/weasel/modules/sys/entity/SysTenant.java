/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.*;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.anno.TransDefaultSett;
import com.fhs.core.trans.constant.TransType;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.RelationManyToOne;
import com.mybatisflex.annotation.RelationOneToMany;
import com.mybatisflex.annotation.Table;
import com.tangzc.autotable.annotation.TableIndex;
import com.tangzc.autotable.annotation.TableIndexes;
import com.tangzc.autotable.annotation.enums.IndexTypeEnum;
import com.tangzc.autotable.annotation.mysql.MysqlTypeConstant;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.common.base.entity.BaseEntity;
import com.weasel.common.base.entity.TreeEntity;
import com.weasel.common.consts.Consts;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@FieldNameConstants
@Table(value = "sys_tenant", comment = "租户表")
@TableIndexes({
        @TableIndex(name = "uni_deleted_name", fields = {BaseEntity.Fields.deleted, SysTenant.Fields.name}, type = IndexTypeEnum.UNIQUE),
        @TableIndex(name = "uni_deleted_code", fields = {BaseEntity.Fields.deleted, SysTenant.Fields.code}, type = IndexTypeEnum.UNIQUE),
})
@TransDefaultSett(isUseCache = true, cacheSeconds = 20)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = BaseEntity.Fields.id)
public class SysTenant extends TreeEntity<SysTenant> {
    @ExcelProperty(value = "名称")
    @ColumnDefine(comment = "名称", notNull = true, length = 500)
    @NotBlank(message = "名称不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String name;
    @ExcelProperty(value = "编码")
    @ColumnDefine(comment = "编码", length = 500)
    private String code;
    @ExcelProperty(value = "数据源")
    @ColumnDefine(comment = "数据源id")
    private Long datasourceId;
    @ExcelProperty(value = "Logo")
    @ColumnDefine(comment = "logo", type = MysqlTypeConstant.LONGTEXT)
    private String logo;
    @ExcelProperty(value = "首页")
    @ColumnDefine(comment = "首页", length = 2000, notNull = true, defaultValue = "/home")
    private String homePath;
    // @ExcelProperty(value = "状态", converter = BooleanStringConverter.class)
    @ColumnDefine(comment = "是否永久租户", notNull = true, defaultValue = "1")
    @Trans(type = TransType.DICTIONARY, key = Consts.AppCache.KEY_FOREVER, ref = "foreverName")
    private Boolean forever;
    @ExcelIgnore
    @Column(ignore = true)
    private String foreverName;
    @Column(comment = "到期时间")
    private LocalDate expireDate;
    // @ExcelProperty(value = "状态", converter = BooleanStringConverter.class)
    @ColumnDefine(comment = "是否禁用", notNull = true, defaultValue = "0")
    @Trans(type = TransType.DICTIONARY, key = Consts.AppCache.KEY_DISABLED, ref = "disabledName")
    private Boolean disabled;
    @ExcelIgnore
    @Column(ignore = true)
    private String disabledName;
    @RelationManyToOne(selfField = "parentId", targetField = "id")
    @JsonBackReference
    private SysTenant parent;
    //    @Column(ignore = true)
    @RelationOneToMany(selfField = "id", targetField = "parentId")
    @JsonManagedReference
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<SysTenant> children;
}
