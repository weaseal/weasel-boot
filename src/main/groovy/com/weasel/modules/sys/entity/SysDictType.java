/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.anno.TransDefaultSett;
import com.fhs.core.trans.constant.TransType;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import com.tangzc.autotable.annotation.TableIndex;
import com.tangzc.autotable.annotation.TableIndexes;
import com.tangzc.autotable.annotation.enums.IndexTypeEnum;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.tangzc.mybatisflex.core.EntityWriteListener;
import com.weasel.common.base.entity.BaseEntity;
import com.weasel.common.base.entity.TenantEntity;
import com.weasel.common.base.entity.TreeTenantEntity;
import com.weasel.common.consts.Consts;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@FieldNameConstants
@Table(value = "sys_dict_type", comment = "字典类型表", onUpdate = EntityWriteListener.class)
@TableIndexes({
        @TableIndex(name = "uni_tenant_id_deleted_name", fields = {TreeTenantEntity.Fields.tenantId, BaseEntity.Fields.deleted, SysDictType.Fields.name}, type = IndexTypeEnum.UNIQUE),
        @TableIndex(name = "uni_tenant_id_deleted_code", fields = {TreeTenantEntity.Fields.tenantId, BaseEntity.Fields.deleted, SysDictType.Fields.code}, type = IndexTypeEnum.UNIQUE),
})
public class SysDictType extends TenantEntity {
    @ExcelProperty("名称")
    @ColumnDefine(comment = "名称", notNull = true)
    @NotBlank(message = "名称不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String name;
    @ExcelProperty("编码")
    @ColumnDefine(comment = "编码", notNull = true, length = 500)
    @NotBlank(message = "编码不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String code;
    @ExcelProperty("排序")
    @ColumnDefine(comment = "排序", notNull = true)
    @NotNull(message = "排序不能为空!")
    private Integer weight;
    @ExcelIgnore
    @ColumnDefine(comment = "是否禁用", notNull = true, defaultValue = "0")
    @Trans(type = TransType.DICTIONARY, key = Consts.AppCache.KEY_DISABLED, ref = "disabledName")
    private Boolean disabled;
    @ExcelProperty(value = "状态")
    @Column(ignore = true)
    private String disabledName;
}
