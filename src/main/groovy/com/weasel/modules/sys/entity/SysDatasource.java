/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.mybatisflex.annotation.Table;
import com.tangzc.autotable.annotation.TableIndex;
import com.tangzc.autotable.annotation.TableIndexes;
import com.tangzc.autotable.annotation.enums.IndexTypeEnum;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.common.base.entity.BaseEntity;
import com.weasel.common.consts.Consts;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = true)
@Table(
        value = "sys_datasource",
        comment = "数据源表")
@TableIndexes({
        @TableIndex(
                name = "uni_deleted_name",
                fields = {BaseEntity.Fields.deleted, "name"},
                type = IndexTypeEnum.UNIQUE),
})
public class SysDatasource extends BaseEntity {
    @ExcelProperty("名称")
    @ColumnDefine(comment = "名称", notNull = true)
    @NotBlank(message = "名称不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String name;

    @ExcelProperty("驱动类")
    @ColumnDefine(comment = "驱动类", notNull = true)
    @NotBlank(message = "驱动类不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String driverClassName;

    @ExcelProperty("url")
    @ColumnDefine(comment = "url", notNull = true)
    @NotBlank(message = "url不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String url;

    @ColumnDefine(comment = "用户名", notNull = true)
    @NotBlank(message = "用户名不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String username;

    @ColumnDefine(comment = "密码", notNull = true)
    @NotBlank(message = "密码不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String password;
}
