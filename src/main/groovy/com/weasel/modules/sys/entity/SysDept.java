/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import com.tangzc.autotable.annotation.TableIndex;
import com.tangzc.autotable.annotation.TableIndexes;
import com.tangzc.autotable.annotation.enums.IndexTypeEnum;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.common.base.entity.BaseEntity;
import com.weasel.common.base.entity.TenantEntity;
import com.weasel.common.base.entity.TreeTenantEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

import javax.validation.constraints.NotBlank;

//@Schema(name = "部门")
@Data
@EqualsAndHashCode(callSuper = true)
@FieldNameConstants
@Table(value = "sys_dept", comment = "部门表")
@TableIndexes({
        @TableIndex(name = "uni_tenant_id_deleted_parent_id_name", fields = {TenantEntity.Fields.tenantId, BaseEntity.Fields.deleted, "parentId", "name"}, type = IndexTypeEnum
                .UNIQUE),
        @TableIndex(name = "uni_tenant_id_deleted_code", fields = {TenantEntity.Fields.tenantId, BaseEntity.Fields.deleted, "code"}, type = IndexTypeEnum.UNIQUE),
})
//@SearchBean(dataSource = "shardingsphere")
//@DS("shardingsphere")
public class SysDept extends TreeTenantEntity<SysDept> {
    @ColumnDefine(comment = "名称", length = 500, notNull = true)
    @NotBlank(message = "名称不能为空!")
    private String name;
    @ExcelProperty(value = "编码")
    @ColumnDefine(comment = "编码", length = 500)
    private String code;
    @ExcelProperty(value = "首页")
    @ColumnDefine(comment = "首页", length = 2000)
    private String homePath;
    // @ExcelProperty(value = "状态", converter = BooleanStringConverter.class)
    @ColumnDefine(comment = "是否禁用", notNull = true, defaultValue = "0")
    // @Trans(type = TransType.DICTIONARY, key = "disabled", ref = "disabledName")
    private Boolean disabled;
    @ExcelIgnore
    @Column(ignore = true)
    private String disabledName;
}
