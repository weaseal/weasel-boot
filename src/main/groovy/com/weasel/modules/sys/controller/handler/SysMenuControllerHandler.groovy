/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.controller.handler

import com.weasel.common.base.controller.DefaultControllerHandler
import com.weasel.common.base.controller.IControllerHandler
import com.weasel.common.base.entity.QueryParam
import com.weasel.common.util.PasswordUtil
import com.weasel.config.WeaselProperties
import com.weasel.modules.sys.entity.SysMenu
import com.weasel.modules.sys.entity.SysUser
import org.dromara.hutool.core.util.RandomUtil
import org.springframework.stereotype.Component

import javax.annotation.Resource

@Component
class SysMenuControllerHandler extends DefaultControllerHandler<SysMenu> {
    @Resource
    WeaselProperties weaselProperties

    @Override
    void initSaveEntity(SysMenu entity) {
        super.initEntity(entity)
        entity.setUiPlatform(weaselProperties.uiPlatform)
    }

    @Override
    void beforeQuery(QueryParam<SysMenu> queryParam) {
        queryParam.queries << new QueryParam.Query(property: 'uiPlatform', value: weaselProperties.uiPlatform)
    }
}
