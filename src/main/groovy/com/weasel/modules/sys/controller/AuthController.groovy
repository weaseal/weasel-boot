/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.controller

import cloud.tianai.captcha.spring.annotation.Captcha
import cn.dev33.satoken.SaManager
import cn.dev33.satoken.session.SaSession
import cn.dev33.satoken.session.TokenSign
import cn.dev33.satoken.stp.SaTokenInfo
import cn.dev33.satoken.stp.StpUtil
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fujieid.jap.core.result.JapResponse
import com.fujieid.jap.spring.boot.starter.JapTemplate
import com.mybatisflex.core.query.QueryWrapper
import com.mybatisflex.core.tenant.TenantManager
import com.mzt.logapi.starter.annotation.LogRecord
import com.weasel.common.base.adapter.ui.UiAdapter
import com.weasel.common.base.exception.BizException
import com.weasel.common.consts.Consts
import com.weasel.common.enums.LoginType
import com.weasel.common.util.PasswordUtil
import com.weasel.config.AuthContext
import com.weasel.config.WeaselProperties
import com.weasel.modules.sys.entity.SysMenu
import com.weasel.modules.sys.entity.SysUser
import com.weasel.modules.sys.mapper.SysTenantMapper
import com.weasel.modules.sys.mapper.SysUserMapper
import com.weasel.modules.sys.service.SysConfigService
import com.weasel.modules.sys.vo.ChangePasswordRequest
import com.weasel.modules.sys.vo.LoginRequest
import io.github.linpeilie.Converter
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.dromara.hutool.core.exception.StatefulException
import org.dromara.hutool.core.lang.Assert
import org.dromara.hutool.core.map.Dict
import org.dromara.hutool.core.text.StrUtil
import org.dromara.hutool.core.util.ObjUtil
import org.dromara.hutool.crypto.Mode
import org.dromara.hutool.crypto.Padding
import org.dromara.hutool.crypto.symmetric.AES
import org.springframework.web.bind.annotation.*

import javax.annotation.Resource

/**
 * @author weasel
 * @version 1.0
 * @date 2022/3/31 9:27
 */
@Tag(name = "权限")
@RestController
@RequestMapping
class AuthController {
    @Resource
    AuthContext authContext
    @Resource
    WeaselProperties weaselProperties
    @Resource
//    CacheManager cacheManager
//    Cache<?, ?> sysConfigCache
//    @Resource
////    CacheManager cacheManager
    SysConfigService sysConfigService
    @Resource
    Converter converter
    @Resource
    JapTemplate japTemplate
    @Resource
    ObjectMapper objectMapper
    @Resource
    UiAdapter uiAdapter
    @Resource
    SysUserMapper sysUserMapper
    @Resource
    SysTenantMapper sysTenantMapper


    @LogRecord(
            success = "登录成功", fail = "登录失败，失败原因：「{{#_errorMsg}}」",
            type = Consts.LogRecordType.LOGIN, bizNo = "11")
    @Operation(summary = "登录")
    @PostMapping("token")
    @Captcha
    Dict token(@RequestBody LoginRequest loginRequest) {
        SysUser sysUser = converter.convert(loginRequest, SysUser.class)
        // 校验指定账号是否已被封禁，如果被封禁则抛出异常 `DisableServiceException`
        StpUtil.checkDisable(sysUser.getUsername())
        // 设置忽略租户插件
        TenantManager.ignoreTenantCondition()
        // SysUser one = Database.execute(SysUser.class, (MPJBaseMapper<SysUser> m) ->
        // m.selectOne(Wrappers.<SysUser>lambdaQuery().eq(SysUser::getUsername,
        // sysUser.getUsername())));
        SysUser queryEntity = new SysUser()
        queryEntity.setUsername(sysUser.getUsername())
        SysUser one = sysUserMapper.selectOneByQuery(QueryWrapper.create(queryEntity))
        // 关闭忽略策略
        TenantManager.restoreTenantCondition()

        // SysUser bean = BeanUtil.toBean(one, SysUser.class);
        // DynaBean dynaBean = new DynaBean(one);
        // Object tenantId = dynaBean.get("tenantId");

        // R.throwOnFalse(Validator.isNotNull(one), SystemCode.FAILURE, "用户名或密码不正确1");
        // R.throwOnFalse(Validator.equal(PasswordUtil.digest(one.getSalt(), sysUser.getPassword(), "sha256"), one.getPassword()), SystemCode.FAILURE, "用户名或密码不正确1");
        // Validator.validateNotNull(one, "用户名或密码不正确");
        // Validator.validateEqual(PasswordUtil.digest(one.getSalt(), sysUser.getPassword(), "sha256"), one.getPassword(), "用户名或密码不正确");
        // 国际化文件中定义错误编码对应的消息：status.700001=用户名或密码不正确
        Assert.notNull(one, () -> new BizException(700001))
        Assert.equals(PasswordUtil.digest(one.getSalt(), sysUser.getPassword(), "sha256"), one.getPassword(), () -> new BizException(700001))
        WeaselProperties.TenantProperties tenantProperties = weaselProperties.getTenant()
        SaSession session
        Object captchaTypeKey
        if (tenantProperties.getEnabled()) {
            StpUtil.login(one.getId(), ObjUtil.equals(one.getTenantId(), tenantProperties.getPrivilegedTenantId()) ? LoginType.OPERATOR.getValue() : LoginType.USER.getValue())
            session = StpUtil.getSession()
            session.set(Consts.Session.CURRENT_TENANT, sysTenantMapper.selectOneById(one.getTenantId()))
            captchaTypeKey = one.getTenantId() + ":" + Consts.AppCache.KEY_SYS_CONFIG$CAPTCHA_TYPE
        } else {
            StpUtil.login(one.getId())
            session = StpUtil.getSession()
            captchaTypeKey = Consts.AppCache.KEY_SYS_CONFIG$CAPTCHA_TYPE
        }
//        Object captchaType = cacheManager
//                .getCache(Consts.AppCache.KEY_SYS_CONFIG)
//        Object captchaType = sysConfigCache.get(captchaTypeKey)
        Object captchaType = sysConfigService.getValByCode("captchaType")
        session.set(Consts.Session.CURRENT_USER, one)
        // StpUtil.login(id) 方法可以从后端控制往浏览器中Cookie写入 Token 值，无须主动向前端返回 Token 信息，参考https://sa-token.cc/doc.html#/use/login-auth
        // 兼容无 Cookie 模式：app、小程序等，参考https://sa-token.cc/doc.html#/up/not-cookie
        SaTokenInfo tokenInfo = StpUtil.getTokenInfo()
        // return R.success(Dict.ofKvs(tokenInfo.getTokenName(), tokenInfo.getTokenValue(), Consts.AppCache.KEY_SYS_CONFIG$CAPTCHA_TYPE, captchaType));
        return Dict.ofKvs(tokenInfo.getTokenName(), tokenInfo.getTokenValue(), Consts.AppCache.KEY_SYS_CONFIG$CAPTCHA_TYPE, captchaType)

    }

    @GetMapping("japlogin")
    Dict japlogin(/*@RequestBody LoginRequest loginRequest*//*CaptchaRequest<LoginRequest> captchaRequest*/) {
        JapResponse authenticate = japTemplate.opsForSimple().authenticate()
        if (authenticate.isSuccess()) {
            SaTokenInfo tokenInfo = StpUtil.getTokenInfo()
            return Dict.ofKvs(tokenInfo.getTokenName(), tokenInfo.getTokenValue())
        }
        return null
    }

    @LogRecord(
            success = "登录成功", fail = "登录失败，失败原因：「{{#_errorMsg}}」",
            type = Consts.LogRecordType.LOGIN, bizNo = "11")
    @Operation(summary = "登录")
    @PostMapping(value = ["login", "auth/login"])
    @Captcha
    Map login(@RequestBody LoginRequest loginRequest/*CaptchaRequest<LoginRequest> captchaRequest*/) {
        def result = [:]
        // Map<String, Object> mapModel1 = new HashMap<>();
        // mapModel1.put("username", "1jkf1ijkj3f");
        // mapModel1.put("password", "111");
        //
        // SysUser1 sysUser = converter.convert(mapModel1, SysUser1.class);
        // SysUser sysUser = converter.convert(mapModel1, SysUser.class);
//        LoginRequest loginRequest = captchaRequest.getForm();
        AES aes = new AES(Mode.ECB, Padding.PKCS5Padding,
                // 密钥，可以自定义
                "_11111000001111@".getBytes()/*,
                // iv加盐，按照实际需求添加
                "@11111000001111_".getBytes()*/
        )
//        sysUser.setUsername(aes.decryptStr(loginRequest.getUsername()));
//        sysUser.setPassword(aes.decryptStr(loginRequest.getPassword()));
        // 校验指定账号是否已被封禁，如果被封禁则抛出异常 `DisableServiceException`
        StpUtil.checkDisable(loginRequest.username)
        // 设置忽略租户插件
        TenantManager.ignoreTenantCondition()
        SysUser queryEntity = new SysUser()
        queryEntity.setUsername(loginRequest.getUsername())
        SysUser one = sysUserMapper.selectOneByQuery(QueryWrapper.create(queryEntity))
        // 关闭忽略策略
        TenantManager.restoreTenantCondition()

        // SysUser bean = BeanUtil.toBean(one, SysUser.class);
        // DynaBean dynaBean = new DynaBean(one);
        // Object tenantId = dynaBean.get("tenantId");

        // R.throwOnFalse(Validator.isNotNull(one), SystemCode.FAILURE, "用户名或密码不正确1");
        // R.throwOnFalse(Validator.equal(PasswordUtil.digest(one.getSalt(), sysUser.getPassword(), "sha256"), one.getPassword()), SystemCode.FAILURE, "用户名或密码不正确1");
        // Validator.validateNotNull(one, "用户名或密码不正确");
        // Validator.validateEqual(PasswordUtil.digest(one.getSalt(), sysUser.getPassword(), "sha256"), one.getPassword(), "用户名或密码不正确");
        // 国际化文件中定义错误编码对应的消息：status.700001=用户名或密码不正确
        Assert.notNull(one, () -> new BizException(700001))
        Assert.equals(PasswordUtil.digest(one.salt, loginRequest.password, "sha256"), one.password, () -> new BizException(700001))
        WeaselProperties.TenantProperties tenantProperties = weaselProperties.tenant
        def privilegedTenant = one.tenantId == tenantProperties.privilegedTenantId
        SaSession session
        Object captchaTypeKey = Consts.AppCache.KEY_SYS_CONFIG$CAPTCHA_TYPE
        if (tenantProperties.enabled) {
            StpUtil.login(one.id, privilegedTenant ? LoginType.OPERATOR.value : LoginType.USER.value)
            session = StpUtil.session
            def tenant = sysTenantMapper.selectOneById(one.tenantId)
            session.set(Consts.Session.CURRENT_TENANT, tenant)
            result.logo = tenant.logo
//            captchaTypeKey = "${one.tenantId}:${Consts.AppCache.KEY_SYS_CONFIG$CAPTCHA_TYPE}"
        } else {
            StpUtil.login(one.getId())
            session = StpUtil.session
//            captchaTypeKey = Consts.AppCache.KEY_SYS_CONFIG$CAPTCHA_TYPE
        }
        //        Object captchaType = cacheManager
//                .getCache(Consts.AppCache.KEY_SYS_CONFIG)
//        Object captchaType = sysConfigCache.get(captchaTypeKey)
        Object captchaType = sysConfigService.getValByCode("captchaType")
        session.set(Consts.Session.CURRENT_USER, one)
        // StpUtil.login(id) 方法可以从后端控制往浏览器中Cookie写入 Token 值，无须主动向前端返回 Token 信息，参考https://sa-token.cc/doc.html#/use/login-auth
        // 兼容无 Cookie 模式：app、小程序等，参考https://sa-token.cc/doc.html#/up/not-cookie
        SaTokenInfo tokenInfo = StpUtil.tokenInfo
        // return R.success(Dict.ofKvs(tokenInfo.getTokenName(), tokenInfo.getTokenValue(), Consts.AppCache.KEY_SYS_CONFIG$CAPTCHA_TYPE, captchaType));
        String tokenValue = tokenInfo.tokenValue
        String refreshToken = SaManager.saTemp.createToken(tokenValue, SaManager.config.timeout * 5)
//        return Dict.ofKvs(tokenInfo.getTokenName(), tokenValue, "refreshToken", refreshToken, Consts.AppCache.KEY_SYS_CONFIG$CAPTCHA_TYPE, captchaType);
        println sysConfigService.getValByCode("captchaType")
        result.putAll(["token": tokenValue, "refreshToken": refreshToken, "$captchaTypeKey": captchaType])
        return result

    }

    @Operation(summary = "获取用户信息")
    @GetMapping(["/getUserInfo", "/auth/getUserInfo"])
    SysUser getUserInfo() throws JsonProcessingException {
        println sysConfigService.getValByCode("captchaType")
        return StpUtil.getSession().getModel(Consts.Session.CURRENT_USER, SysUser.class)
    }

    @Operation(summary = "获取权限码")
    @GetMapping("/getPermCode")
    List<String> getPermCode() {
        return StpUtil.getPermissionList()
    }

    @Operation(summary = "获取权限对象")
    @GetMapping("/getPermObj")
    List<SysMenu> getPermObj() {
        return (authContext.getPermissionMenuList(StpUtil.getLoginId(), StpUtil.getLoginType()))
    }

    @Operation(summary = "登出")
    @GetMapping("/logout")
    void logout() {
        StpUtil.logout()
    }

    @Operation(summary = "强制注销")
    @GetMapping("/forceLogout/{loginId}")
    void forceLogout(@PathVariable Object loginId) {
        StpUtil.logout(loginId)
    }

    @Operation(summary = "踢人下线")
    @GetMapping("/kickout")
    void kickout(Object loginId) {
        StpUtil.kickout(loginId)
    }

    @Operation(summary = "disable")
    @GetMapping("/disable")
    void disable(Object loginId, String service, int level, long time) {
        if (StrUtil.isNotBlank(service)) {
            // 分类封禁
            // 参数1：要封禁的账号id。
            // 参数2：针对这个账号，要封禁的服务标识（可以是任意的自定义字符串）。
            // 参数3：要封禁的时间，单位：秒，此为 86400秒 = 1天（此值为 -1 时，代表永久封禁）。
            StpUtil.disable(loginId, service, time)
        } else if (StrUtil.isNotBlank(service)) {
            // 阶梯封禁
            // 参数1：要封禁的账号id。
            // 参数2：针对这个账号，要封禁的服务标识（可以是任意的自定义字符串）。
            // 参数3：要封禁的时间，单位：秒，此为 86400秒 = 1天（此值为 -1 时，代表永久封禁）。
            StpUtil.disableLevel(loginId, level, time)
        } else {
            // 封禁指定账号
            // 参数1：要封禁的账号id。
            // 参数2：封禁时间，单位：秒，此为 86400秒 = 1天（此值为 -1 时，代表永久封禁）。
            StpUtil.disable(loginId, time)
        }
    }

    /**
     * @return
     * @see <a href="https://sa-token.cc/doc.html#/up/search-session">sa-token会话查询</a>
     */
    @Operation(summary = "会话查询")
    @GetMapping("/sessions")
    List sessions() {
        def sessions = []
        List<String> sessionIdList = StpUtil.searchSessionId("", 0, -1, false)
        sessionIdList.each {
            def map = [:]
            // 根据会话id，查询对应的 SaSession 对象，此处一个 SaSession 对象即代表一个登录的账号
            SaSession session = StpUtil.getSessionBySessionId(it)
            map = [
                    id        : session.id,
                    loginId   : session.loginId,
                    loginType : session.loginType,
                    token     : session.token,
                    timeout   : session.timeout,
                    type      : session.type,
                    createTime: session.createTime,
            ]
            // 查询这个账号都在哪些设备登录了，依据上面的示例，账号A 的 tokenSign 数量是 3，账号B 的 tokenSign 数量是 2
            List<TokenSign> tokenSignList = session.tokenSignList
            map.tokenSignListSize = tokenSignList.size()
            System.out.println("会话id：" + it + "，共在 " + tokenSignList.size() + " 设备登录")
            sessions << map
//            sessions << session
        }
        sessions
    }

    @Operation(summary = "获取权限菜单")
    @GetMapping(["/getMenuList", "/route/getUserRoutes", "/get-async-routes"])
    Object getMenuList() {
        uiAdapter.userRoutes
    }

    @Operation(summary = "获取权限菜单")
    @GetMapping(["/route/getConstantRoutes"])
    Object getConstantRoutes() {
        uiAdapter.constantRoutes
    }

    @Operation(summary = "修改密码")
    @PostMapping(["/changePassword"])
    Object changePassword(@RequestBody ChangePasswordRequest request) {
        String passwordRuleKey = Consts.AppCache.KEY_SYS_CONFIG$PASSWORD_RULE
//        Object passwordRule = sysConfigCache.get(passwordRuleKey)
        Object passwordRule = sysConfigService.getValByCode("passwordRule")
        def matches = request.passwordNew.matches(passwordRule)
        Assert.isTrue(matches, () -> new StatefulException("密码必须长度至少为8位以上，且有2位以上数字、2位以上大写字母、2位以上小写字母"))
//        Assert.isTrue(matches, "密码必须长度至少为8位以上，且有2位以上数字、2位以上大写字母、2位以上小写字母")
        matches
    }
}
