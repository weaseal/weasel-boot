/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.controller;

import com.mybatisflex.core.mybatis.Mappers;
import com.mybatisflex.core.query.QueryWrapper;
import com.weasel.common.base.controller.BaseController;
import com.weasel.common.base.controller.IControllerHandler;
import com.weasel.common.base.entity.BaseEntity;
import com.weasel.common.base.entity.QueryParam;
import com.weasel.modules.sys.entity.SysDictType;
import com.weasel.modules.sys.entity.SysTenant;
import com.weasel.modules.sys.entity.table.SysTenantTableDef;
import com.weasel.modules.sys.mapper.SysDictTypeMapper;
import com.weasel.modules.sys.service.SysDictTypeService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Tag(name = "字典目录")
@RestController
@RequestMapping("/_d/sys/dictType")
class SysDictTypeController extends BaseController<SysDictTypeService, SysDictType> {
    @Resource
    SqlSessionFactory sqlSessionFactory;
    @Resource
    SysDictTypeMapper sysDictTypeMapper;

//    @Override
//    public Object list(Class entityType, QueryParam queryParam) {
//        Mappers.ofEntityClass(SysTenant.class)
//                .selectListWithRelationsByQuery(
//                        QueryWrapper.create().where(SysTenantTableDef.SYS_TENANT.PARENT_ID.eq(0L))
//                );
//        return sysDictTypeMapper.selectListWithRelationsByQuery(QueryWrapper.create());
//    }

//    @SneakyThrows
//    @Override
//    public <T extends BaseEntity> void update(IControllerHandler<T> controllerHandler, T entity) {
//        SysDictType sysDictType = new SysDictType();
//        sysDictType.setId(1793208426858602498L);
//        sysDictType.setDeleted(1793208426858602498L);
//        sysDictTypeMapper.updateById(sysDictType);
////        sysDictTypeMapper.updateById((SysDictType)entity);
////        super.update(controllerHandler, entity);
////        SqlRunner.db().update("UPDATE sys_dict_type\n" +
////                "SET name = '租户字典类型测试', code = 't', weight = 1, disabled = false, tenant_id = 1628294647571030018, version = 26, update_by = 1, update_time = '2024-05-29T16:09:01.210'\n" +
////                "WHERE id = 1793208426858602498\n" +
////                "\tAND version = 25\n" +
////                "\tAND tenant_id IS NOT NULL");
////        new SqlRunner(sqlSessionFactory.openSession().getConnection()).run("UPDATE sys_dict_type\n" +
////                "SET name = '租户字典类型测试', code = 't', weight = 1, disabled = false, tenant_id = 1628294647571030018, version = 26, update_by = 1, update_time = '2024-05-29T16:09:01.210'\n" +
////                "WHERE id = 1793208426858602498\n" +
////                "\tAND version = 25\n" +
////                "\tAND tenant_id IS NOT NULL");
////        sysDictTypeMapper.executeSql("UPDATE sys_dict_type\n" +
////                "SET name = '租户字典类型测试', code = 't', weight = 1, disabled = false, tenant_id = 1628294647571030018, version = 26, update_by = 1, update_time = '2024-05-29T16:09:01.210'\n" +
////                "WHERE id = 1793208426858602498\n" +
////                "\tAND version = (select 25)\n" +
////                "\tAND deleted = 1\n" +
//////                "\tAND EXISTS (select deleted from sys_dict_type where id = 1793208426858602498 and deleted = 0)\n" +
////                "\tAND tenant_id IS NOT NULL");
//
//
//    }
}
