/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.controller.captcha

import cloud.tianai.captcha.common.constant.CaptchaTypeConstant
import cloud.tianai.captcha.common.response.ApiResponse
import cloud.tianai.captcha.spring.application.ImageCaptchaApplication
import cloud.tianai.captcha.spring.plugins.secondary.SecondaryVerificationApplication
import cloud.tianai.captcha.spring.vo.CaptchaResponse
import cloud.tianai.captcha.spring.vo.ImageCaptchaVO
import cloud.tianai.captcha.validator.common.model.dto.ImageCaptchaTrack
import com.feiniaojin.gracefulresponse.api.ExcludeFromGracefulResponse
import org.apache.commons.lang3.StringUtils
import org.springframework.web.bind.annotation.*

import javax.annotation.Resource

@RestController
@RequestMapping("/captcha/tianai")
@ExcludeFromGracefulResponse
class TianaiCaptchaController {
    @Resource
    ImageCaptchaApplication imageCaptchaApplication

    @RequestMapping("/gen")
    @ResponseBody
    CaptchaResponse<ImageCaptchaVO> genCaptcha(@RequestParam(value = "type", required = false) String type) {
        if (StringUtils.isBlank(type)) {
            type = CaptchaTypeConstant.SLIDER
        }
        return imageCaptchaApplication.generateCaptcha(type)
    }

    @PostMapping("/check")
    @ResponseBody
    ApiResponse checkCaptcha(@RequestBody Data data) {
        return imageCaptchaApplication.matching(data.id, data.data)
    }

    /**
     * 二次验证，一般用于机器内部调用，这里为了方便测试
     *
     * @param id id
     * @return boolean
     */
    @GetMapping("/check2")
    @ResponseBody
    Boolean check2Captcha(@RequestParam("id") String id) {
        // 如果开启了二次验证
        if (imageCaptchaApplication instanceof SecondaryVerificationApplication) {
            return (((SecondaryVerificationApplication) imageCaptchaApplication).secondaryVerification(id))
        }
        return false
    }

    static class Data {
        String id;
        ImageCaptchaTrack data;
    }
}
