/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.controller;

import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.row.Db;
import com.weasel.common.base.controller.BaseController;
import com.weasel.common.base.entity.BaseEntity;
import com.weasel.modules.sys.entity.SysRoleMenu;
import com.weasel.modules.sys.mapper.SysRoleMenuMapper;
import com.weasel.modules.sys.service.SysRoleMenuService;
import com.weasel.modules.sys.vo.SaveRoleMenuVo;
import org.dromara.hutool.core.collection.CollUtil;
import org.dromara.hutool.core.data.id.IdUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/_d/sys/role/menu")
class SysRoleMenuController extends BaseController<SysRoleMenuService, SysRoleMenu> {

//    @PostMapping("saveRoleMenu")
//    public <T extends BaseEntity> void save(@RequestBody SaveRoleMenuVo vo) {
//        QueryWrapper queryWrapper = QueryWrapper.create().eq(SysRoleMenu::getRoleId, vo.getRoleId());
//        List<Long> menuIds = service.list(queryWrapper).stream().map(SysRoleMenu::getMenuId).collect(Collectors.toList());
//        Collection<Long> removeMenuIds = CollUtil.subtract(menuIds, vo.getMenuIds());
//        Collection<Long> addMenuIds = CollUtil.subtract(vo.getMenuIds(), menuIds);
//        Db.tx(() -> {
//            if (CollUtil.isNotEmpty(removeMenuIds)) {
//                service.remove(queryWrapper.in(SysRoleMenu::getMenuId, removeMenuIds));
//            }
//            if (CollUtil.isNotEmpty(addMenuIds)) {
//                List<SysRoleMenu> entities = new ArrayList<>();
//                addMenuIds.forEach(menuId -> {
//                    SysRoleMenu sysRoleMenu = new SysRoleMenu();
////                    sysRoleMenu.setId(IdUtil.getSnowflakeNextId());
//                    sysRoleMenu.setRoleId(vo.getRoleId());
//                    sysRoleMenu.setMenuId(menuId);
//                    entities.add(sysRoleMenu);
//                });
//                service.saveBatch(entities);
//            }
//            return true;
//        });
//    }
}
