/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.controller


import com.fujieid.jap.core.result.JapResponse
import com.fujieid.jap.core.store.JapUserStore
import com.fujieid.jap.spring.boot.starter.JapTemplate
import me.zhyd.oauth.model.AuthToken
import me.zhyd.oauth.model.AuthUser
import net.dreamlu.mica.core.result.R
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping

import javax.annotation.Resource
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Controller
@RequestMapping("/social")
class SocialController {
    @Resource
    JapTemplate japTemplate
    @Resource
    JapUserStore japUserStore

    @GetMapping("/login/{platform}")
    JapResponse login(@PathVariable String platform) {
        japTemplate.opsForSocial().authenticate(platform)
    }

    @GetMapping("/login/{platform}/callback")
    String callback(@PathVariable String platform, String code) {
        "redirect:http://localhost:3100/#/login?loginState=REGISTER&code=${code}&platform=${platform}"
    }

    @RequestMapping("/{platform}/user-info")
    R<JapResponse> userInfo(@PathVariable String platform, HttpServletRequest request, HttpServletResponse response) {
        // japUserStore实现类为SessionJapUserStore时，获取的是当前会话保存的AuthUser
        AuthUser authUser = (AuthUser) japUserStore.get(request, response).getAdditional()
        AuthToken token = authUser.getToken()
        return R.success(japTemplate.opsForSocial().getUserInfo(platform, token))
    }

    @RequestMapping("/{platform}/refresh-token")
    JapResponse refreshToken(@PathVariable String platform, HttpServletRequest request, HttpServletResponse response) {
        // 当前会话保存的AuthUser
        AuthUser authUser = (AuthUser) japUserStore.get(request, response).getAdditional()
        AuthToken token = authUser.getToken()
        // FIXME: 2021/10/10 AuthGiteeRequest中没有实现refresh方法，此处暂无法测试
        return japTemplate.opsForSocial().refreshToken(platform, token)
    }

    // FIXME: 2021/10/10 同refreshToken， AuthGiteeRequest（me.zhyd.oauth.request包下）中也没有实现revoke，故暂不测试revokeToken

}
