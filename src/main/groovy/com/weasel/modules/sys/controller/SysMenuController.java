/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.controller;

import com.weasel.common.base.adapter.ui.UiAdapter;
import com.weasel.common.base.controller.BaseController;
import com.weasel.common.base.entity.QueryParam;
import com.weasel.modules.sys.entity.SysMenu;
import com.weasel.modules.sys.service.SysMenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor(onConstructor_ = {@Autowired})
@RestController
@RequestMapping("/_d/sys/menu")
class SysMenuController extends BaseController<SysMenuService, SysMenu> {
//    private final UiAdapter uiAdapter;
//
//    @Override
////    @Lock4j(acquireTimeout = 0, expire = 5000, autoRelease = false, failStrategy = SysMenuLockFailureStrategy.class)
//    public Object list(@RequestBody QueryParam<SysMenu> queryParam) {
////        QueryParam.Query query = new QueryParam.Query();
////        query.setProperty("uiPlatform");
////        query.setValue(uiAdapter.platform());
////        queryParam.getQueries().add(query);
//        return super.list(queryParam);
//    }
}
