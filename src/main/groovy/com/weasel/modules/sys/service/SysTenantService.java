package com.weasel.modules.sys.service;

import com.mybatisflex.core.service.IService;
import com.weasel.modules.sys.entity.SysTenant;

import java.util.List;

/**
 * 租户表 服务层。
 *
 * @author weasel
 * @since 2024-05-09
 */
public interface SysTenantService extends IService<SysTenant> {
    List<Long> listChildrenIds(Long tenantId);
}
