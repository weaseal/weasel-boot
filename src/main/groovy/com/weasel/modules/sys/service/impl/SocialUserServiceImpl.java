/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.service.impl;

import cn.dev33.satoken.stp.SaLoginModel;
import cn.dev33.satoken.stp.StpUtil;
import com.fujieid.jap.core.JapUser;
import com.fujieid.jap.core.JapUserService;
import com.fujieid.jap.spring.boot.common.JapUserServiceType;
import com.weasel.common.consts.Consts;
import com.weasel.common.util.PasswordUtil;
import com.weasel.modules.sys.entity.SocialUser;
import com.weasel.modules.sys.entity.SocialUserAuth;
import com.weasel.modules.sys.entity.SysUser;
import com.weasel.modules.sys.mapper.SocialUserAuthMapper;
import com.weasel.modules.sys.mapper.SocialUserMapper;
import com.weasel.modules.sys.mapper.SysUserMapper;
import me.zhyd.oauth.model.AuthToken;
import me.zhyd.oauth.model.AuthUser;
import org.dromara.hutool.core.bean.BeanUtil;
import org.dromara.hutool.core.util.ObjUtil;
import org.dromara.hutool.core.util.RandomUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 *
 */
@Service(JapUserServiceType.SOCIAL)
public class SocialUserServiceImpl implements JapUserService {
    @Resource
    SocialUserMapper socialUserMapper;
    @Resource
    SocialUserAuthMapper socialUserAuthMapper;
    @Resource
    SysUserMapper sysUserMapper;

    /**
     * 根据第三方平台标识（platform）和第三方平台的用户 uid 查询数据库
     *
     * @param platform 第三方平台标识
     * @param uid      第三方平台的用户 uid
     * @return JapUser
     */
    @Override
    public JapUser getByPlatformAndUid(String platform, String uid) {
        // SocialUserAuth socialUserAuth = Database.execute(SocialUserAuth.class,
        // (MPJBaseMapper<SocialUserAuth> m) -> m.selectJoinOne(SocialUserAuth.class,
        // MPJWrappers.<SocialUserAuth>lambdaJoin().select(SysUser::getUsername,
        // SysUser::getPassword).select(SocialUser::getAccessToken).selectAssociation(SysUser.class,
        // SocialUserAuth::getSysUser).selectAssociation(SocialUser.class,
        // SocialUserAuth::getSocialUser).leftJoin(SysUser.class, SysUser::getId,
        // SocialUserAuth::getSysUserId).leftJoin(SocialUser.class, SocialUser::getId,
        // SocialUserAuth::getSocialUserId).eq(SocialUser::getSource,
        // platform).eq(SocialUser::getUuid, uid)));
        SocialUserAuth socialUserAuth = new SocialUserAuth();
        if (ObjUtil.isNotNull(socialUserAuth)) {
            SysUser sysUser = socialUserAuth.getSysUser();
            SocialUser socialUser = socialUserAuth.getSocialUser();
            StpUtil.login(sysUser.getId(), SaLoginModel.create().setToken(socialUser.getAccessToken()));
            StpUtil.getSession().set(Consts.Session.CURRENT_USER, sysUser);
            return new JapUser().setUserId(sysUser.getId().toString()).setUsername(sysUser.getUsername()).setPassword(sysUser.getPassword()).setToken(socialUser.getAccessToken());
        }
        return null;
    }

    /**
     * 创建并获取第三方用户，相当于第三方登录成功后，将授权关系保存到数据库（开发者业务系统中 social user -> sys user 的绑定关系）
     *
     * @param userInfo JustAuth 中的 AuthUser
     * @return JapUser
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public JapUser createAndGetSocialUser(Object userInfo) {
        AuthUser authUser = (AuthUser) userInfo;
        // 查询绑定关系，确定当前用户是否已经登录过业务系统
        JapUser japUser = this.getByPlatformAndUid(authUser.getSource(), authUser.getUuid());
        if (null == japUser) {
            AuthToken token = authUser.getToken();

            SocialUser socialUser = new SocialUser();
            BeanUtil.copyProperties(authUser, socialUser);
            BeanUtil.copyProperties(token, socialUser);
            socialUserMapper.insert(socialUser);

            SysUser sysUser = new SysUser();
            sysUser.setSalt(RandomUtil.randomString(6));
            sysUser.setPassword(PasswordUtil.digest(sysUser.getSalt(), "123456", "sha256"));
            BeanUtil.copyProperties(authUser, sysUser);
            sysUser.setUsername(authUser.getUsername().concat("-").concat(authUser.getSource()));
            sysUserMapper.insert(sysUser);
            StpUtil.login(sysUser.getId(), SaLoginModel.create().setToken(socialUser.getAccessToken()));
            StpUtil.getSession().set(Consts.Session.CURRENT_USER, sysUser);

            SocialUserAuth socialUserAuth = new SocialUserAuth();
            socialUserAuth.setSocialUserId(socialUser.getId());
            socialUserAuth.setSysUserId(sysUser.getId());
            socialUserAuthMapper.insert(socialUserAuth);

            japUser = new JapUser();
            japUser.setUserId(sysUser.getId().toString()).setUsername(sysUser.getUsername()).setPassword(sysUser.getPassword()).setToken(socialUser.getAccessToken()).setAdditional(authUser.getRawUserInfo());
        }
        return japUser;
    }
}
