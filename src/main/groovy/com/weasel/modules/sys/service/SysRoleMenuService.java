package com.weasel.modules.sys.service;

import com.mybatisflex.core.service.IService;
import com.weasel.modules.sys.entity.SysRoleMenu;

/**
 * 角色-菜单表 服务层。
 *
 * @author weasel
 * @since 2024-05-09
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

}
