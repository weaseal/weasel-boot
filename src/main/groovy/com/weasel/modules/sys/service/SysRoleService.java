package com.weasel.modules.sys.service;

import com.mybatisflex.core.service.IService;
import com.weasel.modules.sys.entity.SysRole;

/**
 * 角色表 服务层。
 *
 * @author weasel
 * @since 2024-05-09
 */
public interface SysRoleService extends IService<SysRole> {

}
