package com.weasel.modules.sys.service.impl;

import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.mybatisflex.core.query.QueryCondition;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.CacheableServiceImpl;
import com.weasel.common.base.mapper.BaseMapper;
import com.weasel.modules.sys.entity.SysTenant;
import com.weasel.modules.sys.entity.table.SysTenantTableDef;
import com.weasel.modules.sys.service.SysTenantService;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SysTenantServiceImpl extends CacheableServiceImpl<BaseMapper<SysTenant>, SysTenant> implements SysTenantService {

    private static final String CACHE_AREA = "sysTenant";
    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:", key = "#entity.id")
    public boolean saveOrUpdate(SysTenant entity) {
        return super.saveOrUpdate(entity);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:", key = "#entity.id")
    public boolean save(SysTenant entity) {
        return super.save(entity);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:")
    public boolean saveBatch(Collection<SysTenant> entities) {
        return super.saveBatch(entities);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:")
    public boolean saveBatch(Collection<SysTenant> entities, int batchSize) {
        return super.saveBatch(entities, batchSize);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:")
    public boolean saveOrUpdateBatch(Collection<SysTenant> entities) {
        return super.saveOrUpdateBatch(entities);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:")
    public boolean saveOrUpdateBatch(Collection<SysTenant> entities, int batchSize) {
        return super.saveOrUpdateBatch(entities, batchSize);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:")
    public boolean remove(QueryWrapper query) {
        return super.remove(query);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:")
    public boolean remove(QueryCondition condition) {
        return super.remove(condition);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:", key = "#entity.id")
    public boolean removeById(SysTenant entity) {
        return super.removeById(entity);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:", key = "#id")
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:")
    public boolean removeByIds(Collection<? extends Serializable> ids) {
        return super.removeByIds(ids);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:")
    public boolean removeByMap(Map<String, Object> query) {
        return super.removeByMap(query);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:", key = "#entity.id", condition = "#result==true")
    public boolean updateById(SysTenant entity) {
        return super.updateById(entity);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:", key = "#entity.id")
    public boolean updateById(SysTenant entity, boolean ignoreNulls) {
        return super.updateById(entity, ignoreNulls);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:", key = "#entity.id")
    public boolean update(SysTenant entity, Map<String, Object> query) {
        return super.update(entity, query);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:", key = "#entity.id")
    public boolean update(SysTenant entity, QueryWrapper query) {
        return super.update(entity, query);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:", key = "#entity.id")
    public boolean update(SysTenant entity, QueryCondition condition) {
        return super.update(entity, condition);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:")
    public boolean updateBatch(Collection<SysTenant> entities) {
        return super.updateBatch(entities);
    }

    @Override
    @CacheInvalidate(area = CACHE_AREA, name = "children:")
    public boolean updateBatch(Collection<SysTenant> entities, int batchSize) {
        return super.updateBatch(entities, batchSize);
    }

    @Override
    @Cached(area = CACHE_AREA, name = "children:", key = "#tenantId", cacheType = CacheType.REMOTE)
    public List<Long> listChildrenIds(Long tenantId) {
        return list(query().where(SysTenantTableDef.SYS_TENANT.PARENT_ID.eq(tenantId))).stream().map(SysTenant::getId).collect(Collectors.toList());
    }
}
