/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.fujieid.jap.core.JapUser;
import com.fujieid.jap.core.JapUserService;
import com.fujieid.jap.spring.boot.common.JapUserServiceType;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.tenant.TenantManager;
import com.weasel.common.base.exception.BizException;
import com.weasel.common.util.PasswordUtil;
import com.weasel.modules.sys.entity.SysUser;
import com.weasel.modules.sys.mapper.SysUserMapper;
import org.dromara.hutool.core.lang.Assert;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 *
 */
@Service(JapUserServiceType.SIMPLE)
public class SimpleUserServiceImpl implements JapUserService {
    @Resource
    SysUserMapper sysUserMapper;

    @Override
    public JapUser getByName(String username) {
        // 设置忽略租户插件
        TenantManager.withoutTenantCondition(() -> {
            SysUser queryEntity = new SysUser();
            queryEntity.setUsername(username);
            SysUser one = sysUserMapper.selectOneByQuery(QueryWrapper.create(queryEntity));
            Assert.notNull(one, () -> new BizException(700001));
            JapUser japUser = new JapUser();
            japUser.setUserId(one.getId().toString());
            japUser.setUsername(one.getUsername());
            japUser.setPassword(one.getPassword());
            japUser.setAdditional(one.getSalt());
            return japUser;
        });
        return null;
    }

    @Override
    public boolean validPassword(String password, JapUser user) {

        String sha256 = PasswordUtil.digest(user.getAdditional().toString(), password, "sha256");
        Assert.equals(sha256, user.getPassword(), () -> new BizException(700001));
        StpUtil.login(user.getUserId());
        return user.getPassword().equals(sha256);
    }
}
