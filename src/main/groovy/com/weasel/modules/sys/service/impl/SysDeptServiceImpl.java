package com.weasel.modules.sys.service.impl;

import com.mybatisflex.core.query.QueryCondition;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.CacheableServiceImpl;
import com.weasel.modules.sys.entity.SysDept;
import com.weasel.modules.sys.mapper.SysDeptMapper;
import com.weasel.modules.sys.service.SysDeptService;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * 部门表 服务层实现。
 *
 * @author weasel
 * @since 2024-05-09
 */
@Service
@CacheConfig(cacheNames = "sysDept")
public class SysDeptServiceImpl extends CacheableServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {

    @Override
    @CacheEvict(allEntries = true)
    public boolean remove(QueryWrapper query) {
        return super.remove(query);
    }

    @Override
    @CacheEvict(key = "#id")
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean removeByIds(Collection<? extends Serializable> ids) {
        return super.removeByIds(ids);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean update(SysDept entity, QueryWrapper query) {
        return super.update(entity, query);
    }

    @Override
    @CacheEvict(key = "#entity.id")
    public boolean updateById(SysDept entity, boolean ignoreNulls) {
        return super.updateById(entity, ignoreNulls);
    }

    @Override
    @CacheEvict(key = "#entity.id")
    public boolean updateById(SysDept entity) {
        return super.updateById(entity);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean updateBatch(Collection<SysDept> entities, int batchSize) {
        return super.updateBatch(entities, batchSize);
    }

    @Override
    @Cacheable(key = "#id")
    public SysDept getById(Serializable id) {
        return super.getById(id);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean saveOrUpdateBatch(Collection<SysDept> entities) {
        return super.saveOrUpdateBatch(entities);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean saveOrUpdateBatch(Collection<SysDept> entities, int batchSize) {
        return super.saveOrUpdateBatch(entities, batchSize);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean remove(QueryCondition condition) {
        return super.remove(condition);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean removeById(SysDept entity) {
        return super.removeById(entity);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean removeByMap(Map<String, Object> query) {
        return super.removeByMap(query);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean update(SysDept entity, Map<String, Object> query) {
        return super.update(entity, query);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean update(SysDept entity, QueryCondition condition) {
        return super.update(entity, condition);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean updateBatch(Collection<SysDept> entities) {
        return super.updateBatch(entities);
    }

}
