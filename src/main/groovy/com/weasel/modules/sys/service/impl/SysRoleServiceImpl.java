package com.weasel.modules.sys.service.impl;

import com.mybatisflex.core.query.QueryCondition;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.CacheableServiceImpl;
import com.weasel.modules.sys.entity.SysRole;
import com.weasel.modules.sys.entity.SysRoleMenu;
import com.weasel.modules.sys.mapper.SysRoleMapper;
import com.weasel.modules.sys.service.SysRoleMenuService;
import com.weasel.modules.sys.service.SysRoleService;
import lombok.RequiredArgsConstructor;
import org.dromara.hutool.core.collection.CollUtil;
import org.dromara.hutool.core.util.ObjUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 角色表 服务层实现。
 *
 * @author weasel
 * @since 2024-05-09
 */
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@CacheConfig(cacheNames = "sysRole")
public class SysRoleServiceImpl extends CacheableServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {
    private final SysRoleMenuService sysRoleMenuService;

    @Override
    @Transactional
    public boolean save(SysRole entity) {
        if (ObjUtil.isNotNull(entity.getMenuIds())) {
            boolean result = false;
            QueryWrapper queryWrapper = QueryWrapper.create().eq(SysRoleMenu::getRoleId, entity.getId());
            List<Long> menuIds = sysRoleMenuService.list(queryWrapper).stream().map(SysRoleMenu::getMenuId).collect(Collectors.toList());
            Collection<Long> removeMenuIds = CollUtil.subtract(menuIds, entity.getMenuIds());
            Collection<Long> addMenuIds = CollUtil.subtract(entity.getMenuIds(), menuIds);
            if (CollUtil.isNotEmpty(removeMenuIds)) {
                result = sysRoleMenuService.remove(queryWrapper.in(SysRoleMenu::getMenuId, removeMenuIds));
            }
            if (CollUtil.isNotEmpty(addMenuIds)) {
                List<SysRoleMenu> entities = new ArrayList<>();
                addMenuIds.forEach(menuId -> {
                    SysRoleMenu sysRoleMenu = new SysRoleMenu();
                    sysRoleMenu.setRoleId(entity.getId());
                    sysRoleMenu.setMenuId(menuId);
                    entities.add(sysRoleMenu);
                });
                result = sysRoleMenuService.saveBatch(entities);
            }
            return result;
        } else if (ObjUtil.isNotEmpty(entity.getName())) {
            return super.save(entity);
        }
        return false;
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean remove(QueryWrapper query) {
        return super.remove(query);
    }

    @Override
    @CacheEvict(key = "#id")
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean removeByIds(Collection<? extends Serializable> ids) {
        return super.removeByIds(ids);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean update(SysRole entity, QueryWrapper query) {
        return super.update(entity, query);
    }

    @Override
    @CacheEvict(key = "#entity.id")
    public boolean updateById(SysRole entity, boolean ignoreNulls) {
        return super.updateById(entity, ignoreNulls);
    }

    @Override
    @CacheEvict(key = "#entity.id")
    public boolean updateById(SysRole entity) {
        return super.updateById(entity);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean updateBatch(Collection<SysRole> entities, int batchSize) {
        return super.updateBatch(entities, batchSize);
    }

    @Override
    @Cacheable(key = "#id")
    public SysRole getById(Serializable id) {
        return super.getById(id);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean saveOrUpdateBatch(Collection<SysRole> entities) {
        return super.saveOrUpdateBatch(entities);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean saveOrUpdateBatch(Collection<SysRole> entities, int batchSize) {
        return super.saveOrUpdateBatch(entities, batchSize);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean remove(QueryCondition condition) {
        return super.remove(condition);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean removeById(SysRole entity) {
        return super.removeById(entity);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean removeByMap(Map<String, Object> query) {
        return super.removeByMap(query);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean update(SysRole entity, Map<String, Object> query) {
        return super.update(entity, query);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean update(SysRole entity, QueryCondition condition) {
        return super.update(entity, condition);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean updateBatch(Collection<SysRole> entities) {
        return super.updateBatch(entities);
    }

}
