package com.weasel.modules.sys.service;

import com.mybatisflex.core.service.IService;
import com.weasel.modules.sys.entity.SysConfig;

/**
 * 配置表 服务层。
 *
 * @author weasel
 * @since 2024-05-09
 */
public interface SysConfigService extends IService<SysConfig> {

    String getValByCode(String code);
}
