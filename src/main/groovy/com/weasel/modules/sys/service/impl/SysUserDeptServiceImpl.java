package com.weasel.modules.sys.service.impl;

import com.mybatisflex.core.query.QueryCondition;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.CacheableServiceImpl;
import com.weasel.modules.sys.entity.SysUserDept;
import com.weasel.modules.sys.mapper.SysUserDeptMapper;
import com.weasel.modules.sys.service.SysUserDeptService;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * 用户-部门表 服务层实现。
 *
 * @author weasel
 * @since 2024-05-09
 */
@Service
@CacheConfig(cacheNames = "sysUserDept")
public class SysUserDeptServiceImpl extends CacheableServiceImpl<SysUserDeptMapper, SysUserDept> implements SysUserDeptService {

    @Override
    @CacheEvict(allEntries = true)
    public boolean remove(QueryWrapper query) {
        return super.remove(query);
    }

    @Override
    @CacheEvict(key = "#id")
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean removeByIds(Collection<? extends Serializable> ids) {
        return super.removeByIds(ids);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean update(SysUserDept entity, QueryWrapper query) {
        return super.update(entity, query);
    }

    @Override
    @CacheEvict(key = "#entity.id")
    public boolean updateById(SysUserDept entity, boolean ignoreNulls) {
        return super.updateById(entity, ignoreNulls);
    }

    @Override
    @CacheEvict(key = "#entity.id")
    public boolean updateById(SysUserDept entity) {
        return super.updateById(entity);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean updateBatch(Collection<SysUserDept> entities, int batchSize) {
        return super.updateBatch(entities, batchSize);
    }

    @Override
    @Cacheable(key = "#id")
    public SysUserDept getById(Serializable id) {
        return super.getById(id);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean saveOrUpdateBatch(Collection<SysUserDept> entities) {
        return super.saveOrUpdateBatch(entities);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean saveOrUpdateBatch(Collection<SysUserDept> entities, int batchSize) {
        return super.saveOrUpdateBatch(entities, batchSize);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean remove(QueryCondition condition) {
        return super.remove(condition);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean removeById(SysUserDept entity) {
        return super.removeById(entity);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean removeByMap(Map<String, Object> query) {
        return super.removeByMap(query);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean update(SysUserDept entity, Map<String, Object> query) {
        return super.update(entity, query);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean update(SysUserDept entity, QueryCondition condition) {
        return super.update(entity, condition);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean updateBatch(Collection<SysUserDept> entities) {
        return super.updateBatch(entities);
    }

}
