package com.weasel.modules.sys.service.impl;

import com.mybatisflex.core.query.QueryCondition;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.CacheableServiceImpl;
import com.weasel.modules.sys.entity.SysRoleMenu;
import com.weasel.modules.sys.mapper.SysRoleMenuMapper;
import com.weasel.modules.sys.service.SysRoleMenuService;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * 角色-菜单表 服务层实现。
 *
 * @author weasel
 * @since 2024-05-09
 */
@Service
@CacheConfig(cacheNames = "sysRoleMenu")
public class SysRoleMenuServiceImpl extends CacheableServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

    @Override
    @CacheEvict(allEntries = true)
    public boolean remove(QueryWrapper query) {
        return super.remove(query);
    }

    @Override
    @CacheEvict(key = "#id")
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean removeByIds(Collection<? extends Serializable> ids) {
        return super.removeByIds(ids);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean update(SysRoleMenu entity, QueryWrapper query) {
        return super.update(entity, query);
    }

    @Override
    @CacheEvict(key = "#entity.id")
    public boolean updateById(SysRoleMenu entity, boolean ignoreNulls) {
        return super.updateById(entity, ignoreNulls);
    }

    @Override
    @CacheEvict(key = "#entity.id")
    public boolean updateById(SysRoleMenu entity) {
        return super.updateById(entity);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean updateBatch(Collection<SysRoleMenu> entities, int batchSize) {
        return super.updateBatch(entities, batchSize);
    }

    @Override
    @Cacheable(key = "#id")
    public SysRoleMenu getById(Serializable id) {
        return super.getById(id);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean saveOrUpdateBatch(Collection<SysRoleMenu> entities) {
        return super.saveOrUpdateBatch(entities);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean saveOrUpdateBatch(Collection<SysRoleMenu> entities, int batchSize) {
        return super.saveOrUpdateBatch(entities, batchSize);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean remove(QueryCondition condition) {
        return super.remove(condition);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean removeById(SysRoleMenu entity) {
        return super.removeById(entity);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean removeByMap(Map<String, Object> query) {
        return super.removeByMap(query);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean update(SysRoleMenu entity, Map<String, Object> query) {
        return super.update(entity, query);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean update(SysRoleMenu entity, QueryCondition condition) {
        return super.update(entity, condition);
    }

    @Override
    @CacheEvict(allEntries = true)
    public boolean updateBatch(Collection<SysRoleMenu> entities) {
        return super.updateBatch(entities);
    }

}
