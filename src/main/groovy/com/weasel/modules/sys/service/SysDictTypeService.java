package com.weasel.modules.sys.service;

import com.mybatisflex.core.service.IService;
import com.weasel.modules.sys.entity.SysDictType;

/**
 * 部门表 服务层。
 *
 * @author weasel
 * @since 2024-05-09
 */
public interface SysDictTypeService extends IService<SysDictType> {

}
