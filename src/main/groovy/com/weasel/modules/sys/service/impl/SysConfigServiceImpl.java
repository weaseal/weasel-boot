/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.service.impl;

import com.alicp.jetcache.anno.Cached;
import com.mybatisflex.core.query.QueryChain;
import com.mybatisflex.spring.service.impl.CacheableServiceImpl;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.weasel.common.base.mapper.BaseMapper;
import com.weasel.common.consts.Consts;
import com.weasel.modules.sys.entity.SysConfig;
import com.weasel.modules.sys.mapper.SysConfigMapper;
import com.weasel.modules.sys.service.SysConfigService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SysConfigServiceImpl extends CacheableServiceImpl<BaseMapper<SysConfig>, SysConfig> implements SysConfigService {
    @Resource
    SysConfigMapper sysConfigMapper;

    @Override
    @Cached(area = "sysConfig", name = Consts.AppCache.KEY_SYS_CONFIG, key = "#code")
    public String getValByCode(String code) {
        return QueryChain.of(SysConfig.class).eq(SysConfig::getCode, code).one().getVal();
    }
}
