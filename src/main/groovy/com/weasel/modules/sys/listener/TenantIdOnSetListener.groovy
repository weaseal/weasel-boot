/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.listener

import com.mybatisflex.annotation.SetListener
import com.mybatisflex.core.mybatis.Mappers
import com.mybatisflex.core.query.QueryWrapper
import com.weasel.common.util.AuthUtil
import com.weasel.modules.sys.entity.table.SysTenantTableDef
import com.weasel.modules.sys.mapper.SysTenantMapper

class TenantIdOnSetListener implements SetListener {
    @Override
    Object onSet(Object entity, String property, Object value) {
//        if (property == 'tenantId') {
//            def count = Mappers.ofMapperClass(SysTenantMapper).selectCountByQuery(QueryWrapper.create().where(SysTenantTableDef.SYS_TENANT.PARENT_ID.eq(AuthUtil.tenantId)))
//            count == 5 && (value = null)
//        }
        return value
    }
}
