/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sys.util


import com.weasel.common.enums.MenuType
import com.weasel.modules.sys.entity.SysMenu
import org.dromara.hutool.core.tree.TreeUtil

/**
 * @author weasel
 * @date 2022/3/29 14:45
 * @version 1.0
 */
final class MenuUtil {

    static buildRoute(SysMenu it) {
        def route = [:]
        route << [
                name      : it?.id,
                path      : it?.path,
                component : it?.component,
                id        : it?.id,
                createTime: it?.createTime,
                permission: it?.permission,
                children  : it?.children,
                redirect  : it?.redirect,
                meta      : [
                        orderNo            : it?.weight,
                        title              : it?.title,
                        dynamicLevel       : it?.dynamicLevel,
                        ignoreKeepAlive    : it?.ignoreKeepAlive,
                        icon               : it?.icon,
                        frameSrc           : it?.frameSrc,
                        hideBreadcrumb     : it?.hideBreadcrumb,
                        hideMenu           : it?.hideMenu,
                        hideTab            : it?.hideTab,
                        isLink             : it?.isLink,
                        ignoreRoute        : it?.ignoreRoute,
                        redirect           : it?.redirect,
                        carryParam         : it?.carryParam,
                        hidePathForChildren: it?.hidePathForChildren,
                ]
        ]
        route
    }

    static buildRoutes(List<SysMenu> menus) {
        def routes = []
        menus.findAll {
            it.type == MenuType.CATALOG
        }.each {
            it.children = getChildren4Route(it, menus)
            routes << buildRoute(it)
        }
        routes
    }

    /**
     * 递归查询子节点
     *
     * @param root 根节点
     * @param all 所有节点
     * @return 根节点信息
     */
    static getChildren4Route(SysMenu root, List<SysMenu> all) {
        def routes = []
        all.findAll {
            it.type == MenuType.MENU && it.parentId == root.id
        }.each {
            it.children = getChildren4Route(it, all)
            routes << buildRoute(it)
        }
        routes
    }

    static buildMenus(List<SysMenu> menus) {
        menus.findAll {
            it.type == MenuType.CATALOG
        }.each {
            def children = getChildren(it, menus)
            if (children) {
                it.children = children
            }
        }
    }

    /**
     * 递归查询子节点
     *
     * @param root 根节点
     * @param all 所有节点
     * @return 根节点信息
     */
    static getChildren(SysMenu root, List<SysMenu> all) {
        all.findAll {
            it.parentId == root.id
        }.each {
            def children = getChildren(it, all)
            if (children) {
                it.children = children
            }
        }
    }

    static void main(String[] args) {
        def nodes = [
                new SysMenu(id: 1, parentId: 0, type: MenuType.CATALOG, weight: 1, title: "1", dynamicLevel: 1, ignoreKeepAlive: false, icon: "1", frameSrc: "1", hideBreadcrumb: false, hideMenu: false, hideTab: false, isLink: false),
                new SysMenu(id: 2, parentId: 1, type: MenuType.MENU, weight: 1, title: "2", dynamicLevel: 1, ignoreKeepAlive: false, icon: "2", frameSrc: "2", hideBreadcrumb: false, hideMenu: false, hideTab: false, isLink: false),
                new SysMenu(id: 3, parentId: 1, type: MenuType.MENU, weight: 2, title: "3", dynamicLevel: 1, ignoreKeepAlive: false, icon: "3", frameSrc: "3", hideBreadcrumb: false, hideMenu: false, hideTab: false, isLink: false),
                new SysMenu(id: 4, parentId: 2, type: MenuType.MENU, weight: 1, title: "4", dynamicLevel: 1, ignoreKeepAlive: false, icon: "4", frameSrc: "4", hideBreadcrumb: false, hideMenu: false, hideTab: false, isLink: false),
                new SysMenu(id: 5, parentId: 2, type: MenuType.MENU, weight: 2, title: "5", dynamicLevel: 1, ignoreKeepAlive: false, icon: "5", frameSrc: "5", hideBreadcrumb: false, hideMenu: false, hideTab: false, isLink: false),
                new SysMenu(id: 6, parentId: 3, type: MenuType.MENU, weight: 1, title: "6", dynamicLevel: 1, ignoreKeepAlive: false, icon: "6", frameSrc: "6", hideBreadcrumb: false, hideMenu: false, hideTab: false, isLink: false),
                new SysMenu(id: 7, parentId: 3, type: MenuType.MENU, weight: 2, title: "7", dynamicLevel: 1, ignoreKeepAlive: false, icon: "7", frameSrc: "7", hideBreadcrumb: false, hideMenu: false, hideTab: false, isLink: false),
                new SysMenu(id: 8, parentId: 4, type: MenuType.MENU, weight: 1, title: "8", dynamicLevel: 1, ignoreKeepAlive: false, icon: "8", frameSrc: "8", hideBreadcrumb: false, hideMenu: false, hideTab: false, isLink: false),
                new SysMenu(id: 9, parentId: 4, type: MenuType.MENU, weight: 2, title: "9", dynamicLevel: 1, ignoreKeepAlive: false, icon: "9", frameSrc: "9", hideBreadcrumb: false, hideMenu: false, hideTab: false, isLink: false),
                new SysMenu(id: 10, parentId: 5, type: MenuType.MENU, weight: 1, title: "10", dynamicLevel: 1, ignoreKeepAlive: false, icon: "10", frameSrc: "10", hideBreadcrumb: false, hideMenu: false, hideTab: false, isLink: false),
                new SysMenu(id: 11, parentId: 5, type: MenuType.MENU, weight: 2, title: "11", dynamicLevel: 1, ignoreKeepAlive: false, icon: "11", frameSrc: "11", hideBreadcrumb: false, hideMenu: false, hideTab: false, isLink: false),
                new SysMenu(id: 12, parentId: 6, type: MenuType.MENU, weight: 1, title: "12", dynamicLevel: 1, ignoreKeepAlive: false, icon: "12", frameSrc: "12", hideBreadcrumb: false, hideMenu: false, hideTab: false, isLink: false),
        ]

        def tree = TreeUtil.build(nodes, 0L, (treeNode, tree) -> {
            tree.setId(treeNode.getId());
            tree.setParentId(treeNode.getParentId());
            tree.setWeight(treeNode.getWeight());
            tree.setName(treeNode.getTitle());
        })

//        def treeHelper = TreeHelper.of(TreeEntity::getId, TreeEntity::getParentId, 1L, TreeEntity::getChildren)
//        def tree = treeHelper.toTree(nodes)
        println tree
        println tree.size()
    }
}
