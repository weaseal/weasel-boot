/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.file.controller

import cn.dev33.satoken.annotation.SaIgnore
import cn.hutool.core.date.TimeInterval
import com.feiniaojin.gracefulresponse.api.ExcludeFromGracefulResponse
import groovy.util.logging.Slf4j
import org.dromara.hutool.core.io.IoUtil
import org.dromara.x.file.storage.core.FileInfo
import org.dromara.x.file.storage.core.FileStorageService
import org.dromara.x.file.storage.core.ProgressListener
import org.dromara.x.file.storage.core.file.HttpServletRequestFileWrapper
import org.dromara.x.file.storage.core.file.MultipartFormDataReader
import org.dromara.x.file.storage.core.platform.FileStorage
import org.dromara.x.file.storage.core.platform.MultipartUploadSupportInfo
import org.dromara.x.file.storage.core.platform.UpyunUssFileStorage
import org.dromara.x.file.storage.core.upload.FilePartInfo
import org.dromara.x.file.storage.core.upload.FilePartInfoList
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.view.RedirectView

import javax.annotation.Resource
import javax.servlet.http.HttpServletRequest

@Slf4j
@RestController
@ConditionalOnBean(FileStorageService)
// todo: 复制;移动（重命名）
class FileDetailController {

    @Resource
    private FileStorageService fileStorageService

//    @ExcludeFromGracefulResponse
//    @GetMapping("/file/**")
//    RedirectView file() {
//        new RedirectView("http://baidu.com")
//    }

    @GetMapping("/info")
    FileInfo info(String url) {
        fileStorageService.getFileInfoByUrl(url)
    }

    @GetMapping("/exists")
    Boolean existsByUrl(String url) {
        fileStorageService.exists(url)
    }

    @PostMapping("/exists")
    Boolean existsByFileInfo(@RequestBody FileInfo fileInfo) {
        fileStorageService.exists(fileInfo)
    }

    // todo: 多种下载方式;监听下载进度
    @GetMapping("/download")
    byte[] downloadByUrl(String url) {
        fileStorageService.download(url).bytes()
    }

    @PostMapping("/download")
    byte[] downloadByFileInfo(@RequestBody FileInfo fileInfo) {
        fileStorageService.download(fileInfo).bytes()
    }

    @GetMapping("/delete")
    Boolean deleteByUrl(String url) {
        fileStorageService.delete(url)
    }

    @PostMapping("/delete")
    Boolean deleteByFileInfo(FileInfo fileInfo) {
        fileStorageService.delete(fileInfo)
    }

    /**
     * 上传文件，成功返回文件 url。支持大文件自动分片上传
     * 图片处理使用的是 https://github.com/coobird/thumbnailator
     */
    @PostMapping("/upload")
    FileInfo upload(HttpServletRequest request) {
        TimeInterval timeInterval = new TimeInterval()
        HttpServletRequestFileWrapper wrapper = (HttpServletRequestFileWrapper) fileStorageService.wrapper(request)

        //获取指定参数，注意无法获取文件类型的参数
        String platform = wrapper.getParameter("platform") ?: fileStorageService.fileStorage.platform
        Integer width = wrapper.getParameter("width") as Integer
        Integer height = wrapper.getParameter("height") as Integer
        Integer thWidth = wrapper.getParameter("thWidth") as Integer
        Integer thHeight = wrapper.getParameter("thHeight") as Integer

        //获取全部参数，注意无法获取文件类型的参数
        MultipartFormDataReader.MultipartFormData formData = wrapper.getMultipartFormData()
//        def exists = fileStorageService.exists(new FileInfo(platform:platform,objectId: formData.fileOriginalFilename))
//        if (exists) {
//            return new FileInfo(objectId: formData.fileOriginalFilename)
//        }

        def isImage = formData.fileContentType.startsWithIgnoreCase("image/")

        // 自动分片上传阈值，达到此大小则使用分片上传，默认 128MB todo:x-file暂未支持此配置，等待更新
        if (formData.fileSize > 128 * 1024 * 1024) {
            MultipartUploadSupportInfo supportInfo = fileStorageService.isSupportMultipartUpload(platform)
            if (supportInfo.isSupport) {
                FileStorage fileStorage = fileStorageService.getFileStorage(platform)
                // 自动分片上传时每个分片大小，默认 32MB todo:x-file暂未支持此配置，等待更新
                int partSize = 32 * 1024 * 1024 // 每个分片大小 5MB
                try (InputStream inputStream = formData.getInputStream()) {
                    FileInfo fileInfo = fileStorageService
                            .initiateMultipartUpload()
                            .setContentType(formData.fileContentType)
                            .setObjectType()
//                .setPath("test/")
                            .setOriginalFilename(formData.getFileOriginalFilename())
//                .setSaveFilename(formData.getFileOriginalFilename())
//                .setSize(formData.getFileSize())
//                            .setObjectId(SecureUtil.md5(inputStream))
//                .setObjectType("user")
//                .putAttr("user", "admin")
//                .putMetadata(Constant.Metadata.CONTENT_DISPOSITION, "attachment;filename=DownloadFileName.mp4")
                    // 又拍云 USS 比较特殊，需要传入分片大小，虽然已有默认值，但为了方便测试还是单独设置一下
                            .putMetadata(
                                    fileStorage instanceof UpyunUssFileStorage, "X-Upyun-Multi-Part-Size", String.valueOf(partSize))
//                .putMetadata("Test-Not-Support", "123456") // 测试不支持的元数据
//                .putUserMetadata("role", "666")
//                .setFileAcl(Constant.ACL.PRIVATE)
                            .init()

                    log.info("手动分片上传文件初始化成功：{}", fileInfo)
//                    def page = PageUtil.totalPage(formData.fileSize, partSize)
//                    CompletableFuture<FilePartInfo>[] futures = new CompletableFuture<FilePartInfo>[page];
////                    List<CompletableFuture<FilePartInfo>> futures = Lists.newArrayList();
////                    List<ListenableFuture<FilePartInfo>> futures = Lists.newArrayList();
//                    ExecutorService executorService = ThreadUtil.newExecutor();

                    /*for (int i in 0..<page) {
//                        def future = CompletableFuture.supplyAsync({

                        def start = PageUtil.getStart(i, partSize)
                        inputStream.skip(start)
//                            byte[] bytes = new byte[partSize]
                        byte[] bytes = inputStream.readNBytes(partSize)
//                             inputStream.read(bytes, 0, partSize)
                            //                        byte[] bytes = IoUtil.readBytes(inputStream, partSize) // 每个分片大小
//                             if (bytes == null || bytes.length == 0) return

                            int partNumber = i + 1
                            System.err.println("===========" + partNumber)
                            FilePartInfo filePartInfo = uploadPart(fileInfo, partNumber, bytes)
//                            return filePartInfo;
//                        }, executorService);
//                        futures[i] = future;
//                        future.join()
//                         ListenableFuture<FilePartInfo> future = Futures.submit({
//                             byte[] bytes = new byte[partSize]
//                             inputStream.readNBytes(bytes, PageUtil.getStart(0, partSize), partSize)
// //                        byte[] bytes = IoUtil.readBytes(inputStream, partSize) // 每个分片大小
////                             if (bytes == null || bytes.length == 0) return
//
//                             int partNumber = i+1
//                             System.err.println("==========="+partNumber)
//                             FilePartInfo filePartInfo = uploadPart(fileInfo, partNumber, bytes)
//                             return filePartInfo;
//                         }, executorService);
//                         futures.add(future);
//                         futures[i] = future;
                    }

//                    CompletableFuture.allOf(futures).whenComplete((m,k)->{
//                        System.out.println("finish");
//                        System.out.println(m);
//                    });
//                    CompletableFuture.allOf(futures).thenApply { mergePart(fileInfo) }
//                    def successfulAsList = Futures.successfulAsList(futures)
//                    Futures.whenAllSucceed(futures).run({println 'uuuuuuuuuuuuuuuu'}, executorService)
//                    Futures.addCallback(successfulAsList, new FutureCallback<FilePartInfo>() {
//                        @Override
//                        void onSuccess(@ParametricNullness FilePartInfo result) {
//                            println 'uuuuuuuuuuuuuuuu'
//                        }
//
//                        @Override
//                        void onFailure(Throwable t) {
//                            println 'aaauuuuuuuuuuuuuuuu'
//                        }
//                    }, executorService)
//                    successfulAsList.addListener({
//                        println 'uuuuuuuuuuuuuuuu'
//                    }, executorService)
//                    List<@Nullable FilePartInfo> booleans = successfulAsList.get();
//                     System.out.println(booleans);
//                    executorService.shutdown();*/

                    for (int partNumber = 1; ; partNumber++) {
//                        byte[] bytes = inputStream.readNBytes(partSize)
                        byte[] bytes = IoUtil.readBytes(inputStream, partSize) // 每个分片大小
                        if (bytes == null || bytes.length == 0) break

                        uploadPart(fileInfo, partNumber, bytes)
                    }

                    mergePart(fileInfo)
                    println timeInterval.intervalPretty()
                    return fileInfo
                }
            }
        }
        return fileStorageService.of(wrapper)
                .setPlatform(!!platform, platform)
                .setObjectId(formData.fileOriginalFilename)
                .setOriginalFilename(formData.fileOriginalFilename)
                .image(isImage && width && height, img -> img.size(width, height))  //将图片大小调整到 1000*1000
                .thumbnail(isImage && thWidth && thHeight, th -> th.size(thWidth, thHeight))  //再生成一张 200*200 的缩略图
                .thumbnail(isImage)  // 不设置压缩比例上传
                .setHashCalculatorMd5()
                .upload()
    }

    /**
     * 手动分片上传-列举已上传的分片
     */
    @PostMapping("/parts")
    FilePartInfoList parts(FileInfo fileInfo) {
        String platform = fileInfo.platform ?: fileStorageService.fileStorage.platform
        MultipartUploadSupportInfo supportInfo = fileStorageService.isSupportMultipartUpload(platform)
        if (supportInfo.isSupportListParts) {
            return fileStorageService.listParts(fileInfo).listParts()
        }
        return null
    }

    /**
     * 手动分片上传后取消
     */
    @PostMapping("/abort")
    FileInfo abort(FileInfo fileInfo) {
        String platform = fileInfo.platform ?: fileStorageService.fileStorage.platform
        MultipartUploadSupportInfo supportInfo = fileStorageService.isSupportMultipartUpload(platform)
        if (supportInfo.getIsSupportAbort()) {
            return fileStorageService.abortMultipartUpload(fileInfo).abort()
        }
        return fileInfo
    }

    private FilePartInfo uploadPart(FileInfo fileInfo, int partNumber, byte[] bytes) {
        FilePartInfo filePartInfo = fileStorageService
                .uploadPart(fileInfo, partNumber, bytes)
                .setProgressListener(new ProgressListener() {
                    @Override
                    void start() {
                        System.out.println("分片 " + partNumber + " 上传开始")
                    }

                    @Override
                    void progress(long progressSize, Long allSize) {
                        if (allSize == null) {
                            System.out.println("分片 " + partNumber + " 已上传 " + progressSize + " 总大小未知")
                        } else {
                            System.out.println("分片 " + partNumber + " 已上传 " + progressSize + " 总大小"
                                    + allSize + " " + (progressSize * 10000 / allSize * 0.01) + "%")
                        }
                    }

                    @Override
                    void finish() {
                        System.out.println("分片 " + partNumber + " 上传结束")
                    }
                })
                .setHashCalculatorMd5()
                .setHashCalculatorSha256()
                .upload()
        log.info("手动分片上传-分片上传成功：{}", filePartInfo)
        return filePartInfo
    }

    /**
     * 合并分片
     * @param fileInfo
     */
    private void mergePart(FileInfo fileInfo) {
        fileStorageService
                .completeMultipartUpload(fileInfo)
        //                .setPartInfoList(partList)
                .setProgressListener(new ProgressListener() {
                    @Override
                    void start() {
                        System.out.println("文件合并开始")
                    }

                    @Override
                    void progress(long progressSize, Long allSize) {
                        if (allSize == null) {
                            System.out.println("文件已合并 " + progressSize + " 总大小未知")
                        } else {
                            System.out.println("文件已合并 " + progressSize + " 总大小" + allSize + " "
                                    + (progressSize * 10000 / allSize * 0.01) + "%")
                        }
                    }

                    @Override
                    void finish() {
                        System.out.println("文件合并结束")
                    }
                })
                .complete()
        log.info("手动分片上传文件完成成功：{}", fileInfo)
    }

    static void main(String[] args) {
//        def md5 = SecureUtil.md5(FileUtil.file("D:\\Temp\\local-plus\\a.mp4"))
//        def md5 = SecureUtil.md5(FileUtil.file("D:\\Temp\\local-plus\\upload\\65a4e94d327e84c273b1d5ff.mp4"))
//        def md5 = SecureUtil.md5(FileUtil.file("C:\\Users\\40284\\Desktop\\微信截图_20240105093907.png"))
//        println md5
//        println HexUtil.encodeHexStr(md5.getBytes())
//        MessageDigest messageDigest = MessageDigest.getInstance("MD5")
//        byte[] bytes = FileUtil.readBytes(FileUtil.file("C:\\Users\\40284\\Desktop\\微信截图_20240105093907.png"))
//        println HexUtil.encodeHexStr(messageDigest.digest(bytes))
        for (int i in 0..<1) {
            println i
        }

    }

}
