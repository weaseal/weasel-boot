package com.weasel.modules.file.entity;

import com.mybatisflex.annotation.Table;
import com.weasel.common.base.entity.TenantEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

/**
 * 文件分片信息表，仅在手动分片上传时使用
 */
@Data
@EqualsAndHashCode(callSuper = true)
@FieldNameConstants
@Table(value = "file_part_detail", comment = "文件分片信息表")
public class FilePartDetail extends TenantEntity {

    /**
     * 存储平台
     */
    private String platform;

    /**
     * 上传ID，仅在手动分片上传时使用
     */
    private String uploadId;

    /**
     * 分片 ETag
     */
    private String eTag;

    /**
     * 分片号。每一个上传的分片都有一个分片号，一般情况下取值范围是1~10000
     */
    private Integer partNumber;

    /**
     * 文件大小，单位字节
     */
    private Long partSize;

    /**
     * 哈希信息
     */
    private String hashInfo;
}
