/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.sms.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.constant.TransType;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import com.tangzc.autotable.annotation.mysql.MysqlTypeConstant;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.common.base.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@Table(
        value = "sms_config_info",
        comment = "短信渠道配置表")
public class SmsConfigInfo extends BaseEntity {
    @ColumnDefine(comment = "短信厂商", notNull = true)
    @NotNull(message = "短信厂商不能为空!")
    private String supplier;

    @ColumnDefine(comment = "配置内容", notNull = true, type = MysqlTypeConstant.TEXT)
    @NotNull(message = "配置内容不能为空!")
    private String content;

    @ColumnDefine(comment = "是否禁用", notNull = true, defaultValue = "0")
    @Trans(type = TransType.DICTIONARY, key = "disabled", ref = "disabledName")
    private Boolean disabled;

    @ExcelIgnore
    @Column(ignore = true)
    private String disabledName;
}
