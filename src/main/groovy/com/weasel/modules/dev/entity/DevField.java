/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.dev.entity;

import com.mybatisflex.annotation.Table;
import com.tangzc.mybatisflex.autotable.annotation.ColumnDefine;
import com.weasel.common.base.entity.TenantEntity;
import com.weasel.common.consts.Consts;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@Table(value = "dev_field", comment = "在线开发表单字段定义表")
public class DevField extends TenantEntity {

    @ColumnDefine(comment = "表名", notNull = true)
    @NotBlank(message = "表名不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String tableName;
    @ColumnDefine(comment = "列名", notNull = true)
    @NotBlank(message = "列名不能为空!", groups = Consts.ValidateGroup.CreateGroup.class)
    private String colName;
    @ColumnDefine(comment = "字段描述")
    private String comment;
    @ColumnDefine(comment = "标题", notNull = true)
    private String title;
    @NotNull(message = "排序不能为空!")
    private Integer orderNo;
    /**
     * 以下为列表属性
     */
    @ColumnDefine(comment = "列表单元格展示组件")
    private String viewComponent;
    @ColumnDefine(comment = "是否转码")
    private Boolean shouldConvert;
    @ColumnDefine(comment = "宽度")
    private String width;
    @ColumnDefine(comment = "是否查询条件")
    private Boolean search;
    @ColumnDefine(comment = "列表查询表单项组件")
    private String searchComponent;
    /**
     * 以下为表单属性
     */
    @ColumnDefine(comment = "编辑组件")
    private String editComponent;
}
