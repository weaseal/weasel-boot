/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.dev.controller.handler


//package com.weasel.modules.dev.controller.handler
//
//import com.baomidou.dynamic.datasource.DynamicRoutingDataSource
//import com.baomidou.dynamic.datasource.creator.DataSourceProperty
//import com.baomidou.dynamic.datasource.creator.druid.DruidDataSourceCreator
//import com.weasel.common.base.controller.IControllerHandler
//import com.weasel.modules.dev.entity.DevDatasource
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.web.bind.annotation.RequestMapping
//import org.springframework.web.bind.annotation.RestController
//
//import javax.sql.DataSource
//
///**
// * @author weasel
// * @date 2022/7/25 9:45
// * @version 1.0
// */
//@RestController
//@RequestMapping("/dev/datasource")
//class DevDatasourceControllerHandler implements IControllerHandler<DevDatasource> {
//    @Autowired
//    DataSource dataSource
//    @Autowired
//    DruidDataSourceCreator dataSourceCreator
//
//    @Override
//    void beforeSave(DevDatasource entity) {
//        addDataSource(entity)
////        Assert.isTrue(0 == new SysRole().selectCount(Wrappers.<SysRole>lambdaQuery().eq(SysRole::getName, entity.getName())), "已存在相同名称记录！");
////        Assert.isTrue(0 == new SysRole().selectCount(Wrappers.<SysRole>lambdaQuery().eq(SysRole::getCode, entity.getCode())), "已存在相同值记录！");
//    }
//
//    @Override
//    void beforeUpdate(DevDatasource entity) {
//        addDataSource(entity)
////        Assert.isFalse(EnumUtil.contains(DefaultRole.class, entity.getCode()), "系统默认角色不允许编辑！");
////        CommonControllerHandler.super.beforeUpdate(entity);
////        Assert.isTrue(0 == new SysRole().selectCount(Wrappers.<SysRole>lambdaQuery().ne(SysRole::getId, entity.getId()).eq(SysRole::getName, entity.getName())), "已存在相同名称记录！");
////        Assert.isTrue(0 == new SysRole().selectCount(Wrappers.<SysRole>lambdaQuery().ne(SysRole::getId, entity.getId()).eq(SysRole::getCode, entity.getCode())), "已存在编码名称记录！");
//    }
//
//    @Override
//    void beforeRemove(DevDatasource entity) {
////        Assert.isTrue(0 == new SysRole().selectCount(Wrappers.<SysRole>lambdaQuery().eq(SysRole::getId, id).in(SysRole::getCode, EnumUtil.getFieldValues(DefaultRole.class, "value"))), "所选角色为系统默认角色，不能删除！");
////        Assert.isTrue(0 == new SysRoleMenu().selectCount(Wrappers.<SysRoleMenu>lambdaQuery().eq(SysRoleMenu::getRoleId, id)), "所选角色已存在关联菜单，请先删除关联菜单！");
////        Assert.isTrue(0 == new SysUserRole().selectCount(Wrappers.<SysUserRole>lambdaQuery().eq(SysUserRole::getRoleId, id)), "所选角色已存在关联用户，请先删除关联用户！");
//    }
//
//    @Override
//    void afterRemove(DevDatasource entity) {
//        def dynamicRoutingDataSource = dataSource as DynamicRoutingDataSource
//        dynamicRoutingDataSource.removeDataSource(entity.name)
//    }
//
//    @Override
//    void beforeRemoveBatch(List<DevDatasource> entities) {
////        Assert.isTrue(0 == new SysRole().selectCount(Wrappers.<SysRole>lambdaQuery().in(SysRole::getId, ids).in(SysRole::getCode, EnumUtil.getFieldValues(DefaultRole.class, "value"))), "所选记录中包含系统默认角色，不能删除！");
////        Assert.isTrue(0 == new SysRoleMenu().selectCount(Wrappers.<SysRoleMenu>lambdaQuery().in(SysRoleMenu::getRoleId, ids)), "所选角色中已存在关联菜单，请先删除关联菜单！");
////        Assert.isTrue(0 == new SysUserRole().selectCount(Wrappers.<SysUserRole>lambdaQuery().in(SysUserRole::getRoleId, ids)), "所选角色中已存在关联用户，请先删除关联用户！");
//    }
//
//    @Override
//    void afterRemoveBatch(List<DevDatasource> entities) {
//        def dynamicRoutingDataSource = dataSource as DynamicRoutingDataSource
//        entities*.name.each { name ->
//            dynamicRoutingDataSource.removeDataSource(name)
//        }
//    }
//
//    void addDataSource(DevDatasource entity) {
//        def dynamicRoutingDataSource = dataSource as DynamicRoutingDataSource
//        dynamicRoutingDataSource.removeDataSource(entity.name)
//        DataSourceProperty dataSourceProperty = new DataSourceProperty()
//        dataSourceProperty.setUsername(entity.getUsername())
//        dataSourceProperty.setPassword(entity.getPassword())
//        dataSourceProperty.setUrl(entity.getUrl())
//        dataSourceProperty.setDriverClassName(entity.getDriverClassName())
//        DataSource createDataSource = dataSourceCreator.createDataSource(dataSourceProperty)
//        dynamicRoutingDataSource.addDataSource(entity.name, createDataSource)
//    }
//}
//
