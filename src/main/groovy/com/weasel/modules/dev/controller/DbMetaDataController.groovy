/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.dev.controller

import cn.dev33.satoken.annotation.SaIgnore
import com.feiniaojin.gracefulresponse.api.ExcludeFromGracefulResponse
import com.mybatisflex.core.datasource.FlexDataSource
import com.mybatisflex.core.mybatis.Mappers
import com.mybatisflex.core.row.Db
import com.weasel.modules.sys.entity.SysTenant
import com.weasel.modules.sys.mapper.SysTenantMapper
import org.anyline.entity.DataSet
import org.anyline.metadata.Table
import org.anyline.proxy.ServiceProxy
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import javax.sql.DataSource
import java.sql.DatabaseMetaData
import java.sql.ResultSet

/**
 * @author weasel
 * @date 2022/8/2 9:05
 * @version 1.0
 */
@RestController
@SaIgnore
@RequestMapping('/_d/dbMetaData')
class DbMetaDataController {
    @Autowired
    DataSource dataSource

    DataSource getDataSource(String name) {
        if (dataSource instanceof FlexDataSource) {
            def dynamicRoutingDataSource = dataSource as FlexDataSource
            dynamicRoutingDataSource.dataSourceMap[name]
        } else {
            dataSource
        }
    }

    @PostMapping('list')
    List<Table> tables(@RequestBody params) {
        ServiceProxy.metadata().tables(false)
    }

    @GetMapping('data')
    Object data(String name) {
        Db.selectAll('auto_table_execute_sql_log')
//        Db.selectAll('sys_user')
//        Mappers.ofMapperClass(SysTenantMapper).selectAll()
//        ServiceProxy.querys(name).toLowerKey().camel()
    }

    @GetMapping('sql')
    DataSet sql(String name) {
        ServiceProxy.service().querys('select * from sys_user_role ur left join sys_role r on ur.role_id = r.id')
    }

    @GetMapping('columns')
    Object columns(String name) {
        ServiceProxy.metadata().columns(name)
    }
}
