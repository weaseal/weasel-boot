/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel.modules.dev.controller
//package com.weasel.modules.dev.controller
//
//import ai.yue.library.base.view.R
//import ai.yue.library.base.view.Result
//import com.alibaba.druid.pool.DruidDataSource
//import com.baomidou.dynamic.datasource.DynamicRoutingDataSource
//import com.baomidou.dynamic.datasource.ds.ItemDataSource
//import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceProperties
//import com.baomidou.mybatisplus.core.toolkit.Wrappers
//import com.weasel.common.base.com.weasel.common.base.controller.BaseController
//import com.weasel.modules.dev.entity.DevDatasource
//import com.weasel.modules.dev.service.DevDatasourceService
//import org.springframework.beans.BeanUtils
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.web.bind.annotation.GetMapping
//import org.springframework.web.bind.annotation.PathVariable
//import org.springframework.web.bind.annotation.RequestMapping
//import org.springframework.web.bind.annotation.RestController
//
//import javax.sql.DataSource
//
//@RestController
//@RequestMapping('${weasel.dynamicRouterPrefix:/_d}/dev_datasource')
//class DevDatasourceController extends com.weasel.common.base.controller.BaseController<DevDatasource> {
//    @Autowired
//    DynamicRoutingDataSource dynamicRoutingDataSource
//    @Autowired
//    DynamicDataSourceProperties dynamicDataSourceProperties
//    @Autowired
//    DevDatasourceService devDatasourceService
//
////    public DevDatasourceController(DynamicRoutingDataSource dynamicRoutingDataSource) {
////        this.dynamicRoutingDataSource = dynamicRoutingDataSource;
////    }
//
//    @Override
//    @GetMapping
///*(value = "${weasel.dynamicRouterPrefix:/_d}/dev_datasource")*/
//    Result list() {
//        super.list()
//        List<DevDatasource> devDatasources = new ArrayList<>()
//        //        return R.success(CollUtil.newArrayList(sysRole));
//        Map<String, DataSource> dataSources = dynamicRoutingDataSource.getDataSources()
//        def keys = dynamicDataSourceProperties.datasource*.getKey()
//        keys.each {
//            DruidDataSource druidDataSource = ((DruidDataSource) ((ItemDataSource) dataSources[it]).getRealDataSource())
//            DevDatasource devDatasource = new DevDatasource()
////            devDatasource.setName(druidDataSource.getName())
////            devDatasource.setDriverClassName(druidDataSource.getDriverClassName())
////            devDatasource.setUrl(druidDataSource.getUrl())
////            devDatasource.setUsername(druidDataSource.getUsername())
////            devDatasource.setPassword(druidDataSource.getPassword())
////            BeanUtil.copyProperties(druidDataSource, devDatasource)
//            BeanUtils.copyProperties(druidDataSource, devDatasource)
////            BeanUtil.copyProperties(druidDataSource, devDatasource)
//            devDatasource.setInternal(true)
//            devDatasources.add(devDatasource)
//        }
//        devDatasourceService.list(Wrappers.query().in('name', dataSources*.getKey() - keys)).each {
//            DruidDataSource druidDataSource = ((DruidDataSource) ((ItemDataSource) dataSources[it.name]).getRealDataSource())
//            DevDatasource devDatasource = new DevDatasource()
////            devDatasource.setName(druidDataSource.getName())
////            devDatasource.setDriverClassName(druidDataSource.getDriverClassName())
////            devDatasource.setUrl(druidDataSource.getUrl())
////            devDatasource.setUsername(druidDataSource.getUsername())
////            devDatasource.setPassword(druidDataSource.getPassword())
//            BeanUtils.copyProperties(druidDataSource, devDatasource)
//            devDatasource.setId(it.id)
//            devDatasource.setInternal(false)
//            devDatasources.add(devDatasource)
//        }
//        return R.success(devDatasources)
//    }
//
//    @Override
//    @GetMapping('/{id}')
//    Result getById(@PathVariable Long id) {
//        return super.getById(id)
//    }
//
//    @Override
//    String getTable() {
//        return "dev_datasource"
//    }
//}
