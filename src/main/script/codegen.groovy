import com.mybatisflex.processor.entity.TableInfo

/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */


String[] templateDirs = [properties["collection.template.dir"],
                         properties["collection.template.test.dir"]]
String[] outputDirs = [properties["collection.src.dir"],
                       properties["collection.testsrc.dir"]]

templateDirs.eachWithIndex { templateDir, i ->
    convertSources templateDir, outputDirs[i]
}

void convertSources(String templateDir, String outputDir) {
    String[] keyPrimitives = ["byte", "char", "short", "int", "long"]
    String[] keyObjects = ["Byte", "Character", "Short", "Integer", "Long"]
    String[] keyNumberMethod = ["byteValue", "charValue", "shortValue", "intValue", "longValue"]

    keyPrimitives.eachWithIndex { keyPrimitive, i ->
        convertTemplates templateDir, outputDir, keyPrimitive, keyObjects[i], keyNumberMethod[i]
    }
}

void convertTemplates(String templateDir,
                      String outputDir,
                      String keyPrimitive,
                      String keyObject,
                      String keyNumberMethod) {
    TableInfo tableInfo = new TableInfo()
    def keyName = keyPrimitive.capitalize()
    def replaceFrom = "(^.*)K([^.]+)\\.template\$"
    def replaceTo = "\\1" + keyName + "\\2.java"
    def hashCodeFn = keyPrimitive.equals("long") ? "(int) (key ^ (key >>> 32))" : "(int) key"
//    ant.copy(todir: outputDir) {
//        fileset(dir: templateDir) {
//            include(name: "**/*.template")
//        }
//        filterset() {
//            filter(token: "K", value: keyName)
//            filter(token: "k", value: keyPrimitive)
//            filter(token: "O", value: keyObject)
//            filter(token: "KEY_NUMBER_METHOD", value: keyNumberMethod)
//            filter(token: "HASH_CODE", value: hashCodeFn)
//        }
//        regexpmapper(from: replaceFrom, to: replaceTo)
//    }
}
