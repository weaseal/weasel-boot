/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

//package com.weasel
//
//import com.github.davidfantasy.mybatisplus.generatorui.GeneratorConfig
//import com.github.davidfantasy.mybatisplus.generatorui.MybatisPlusToolsApplication
//import com.github.davidfantasy.mybatisplus.generatorui.mbp.NameConverter
//
//class GeberatorUIServer {
//    public static void main(String[] args) {
//        GeneratorConfig config = GeneratorConfig.builder().jdbcUrl("jdbc:mysql://localhost:3306/weasel")
//                .userName("root")
//                .password("root")
//                .driverClassName("com.mysql.cj.jdbc.Driver")
//        //数据库schema，MSSQL,PGSQL,ORACLE,DB2类型的数据库需要指定
//                .schemaName("weasel")
//        //如果需要修改entity及其属性的命名规则，以及自定义各类生成文件的命名规则，可自定义一个NameConverter实例，覆盖相应的名称转换方法，详细可查看该接口的说明：
//                .nameConverter(new NameConverter() {
//                    /**
//                     * 自定义Service类文件的名称规则
//                     */
//                    @Override
//                    public String serviceNameConvert(String tableName) {
//                        return this.entityNameConvert(tableName) + "Service";
//                    }
//
//                    /**
//                     * 自定义Controller类文件的名称规则
//                     */
//                    @Override
//                    public String controllerNameConvert(String tableName) {
//                        return this.entityNameConvert(tableName) + "Action";
//                    }
//                })
//        //所有生成的java文件的父包名，后续也可单独在界面上设置
//                .basePackage("com.github.davidfantasy.mybatisplustools.example")
//                .port(8068)
//                .build();
//        MybatisPlusToolsApplication.run(config);
//    }
//
//}
