// package com.weasel;
//
// import com.baomidou.mybatisplus.generator.FastAutoGenerator;
// import com.baomidou.mybatisplus.generator.config.OutputFile;
// import com.baomidou.mybatisplus.generator.config.po.LikeTable;
// import com.baomidou.mybatisplus.generator.engine.BeetlTemplateEngine;
// import com.github.yulichang.base.MPJBaseMapper;
// import com.github.yulichang.base.MPJBaseService;
// import com.github.yulichang.base.MPJBaseServiceImpl;
// import com.weasel.common.base.controller.BaseController;
// import com.weasel.common.base.entity.BaseEntity;
// import com.weasel.common.base.mapper.BaseMapper;
// import org.apache.ibatis.annotations.Mapper;
// import org.junit.jupiter.api.Test;
//
// import java.util.Collections;
//
// class CodeGen {
//     @Test
//     public void gen() {
//         FastAutoGenerator.create("jdbc:mysql://127.0.0.1:3306/weasel", "root", "root")
//                 .globalConfig(builder -> {
//                     builder.author("weasel") // 设置作者
//                             .enableSwagger() // 开启 swagger 模式
//                             .enableKotlin()
//                             .outputDir("D:\\codegen"); // 指定输出目录
//                 })
//                 .packageConfig(builder -> {
//                     builder.parent("com.weasel.modules") // 设置父包名
//                             .moduleName("sys") // 设置父包模块名
//                             .pathInfo(Collections.singletonMap(OutputFile.xml, "D://")); // 设置mapperXml生成路径
//                 })
//                 .strategyConfig(builder -> {
//                     builder.likeTable(new LikeTable("sys_%"))
//                             // .addInclude("sys_datasource","sys_app","sys_app_menu","sys_tenant_app") // 设置需要生成的表名
// //                            .addTablePrefix("sys_", "c_") // 设置过滤表前缀
//                             .entityBuilder()
//                             .superClass(BaseEntity.class)
//                             .enableLombok()
//                             .enableFileOverride()
//                             .mapperBuilder()
//                             .superClass(BaseMapper.class)
//                             .mapperAnnotation(Mapper.class)
//                             .enableFileOverride()
//                             .serviceBuilder()
//                             .superServiceClass(MPJBaseService.class)
//                             .superServiceImplClass(MPJBaseServiceImpl.class)
//                             .formatServiceFileName("%sService")
//                             .controllerBuilder()
//                             .superClass(BaseController.class)
//                             // .enableHyphenStyle()
//                             .enableFileOverride() // 覆盖已生成文件
//                     ;
//                 })
//                 .templateEngine(new BeetlTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
//                 .execute();
//
// //        /**
// //         * 注入到条件查询中
// //         */
// //        int CONDITION = 1;
// //        /**
// //         * 注入插入内容中
// //         */
// //        int INSERT = 1<<1;
// //        /**
// //         * 注入到更新内容中
// //         */
// //        int UPDATE = 1<<2;
// //
// //        /**
// //         * 注入到查询内容中
// //         */
// //        int SELECT_ITEM=1<<3;
// //
// //        println INSERT
// //        println UPDATE
// //        println SELECT_ITEM
// //        println '----------------------'
// //        println 1 & CONDITION
// //        println 1 | UPDATE
// //        println CONDITION | INSERT
// //        println 1 | 2
// //        println 1 | 7
//     }
// }
