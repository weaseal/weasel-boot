///*
// * Copyright (c) 2023-present weasel
// *    weasel-boot is licensed under Mulan PSL v2.
// *    You can use this software according to the terms and conditions of the Mulan PSL v2.
// *    You may obtain a copy of Mulan PSL v2 at:
// *                http://license.coscl.org.cn/MulanPSL2
// *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// *    See the Mulan PSL v2 for more details.
// */
//
//package com.weasel;
//
//import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceAutoConfiguration;
//import com.baomidou.mybatisplus.test.autoconfigure.MybatisPlusTest;
//import com.weasel.config.DynamicDataSourceConfig;
//import com.weasel.modules.sys.entity.SysConfig;
//import com.weasel.modules.sys.entity.SysConfigMapper;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
//
//@MybatisPlusTest
////@ImportAutoConfiguration(value = {DynamicDataSourceAutoConfiguration.class}, exclude = {DataSourceAutoConfiguration.class})
//public class MybatisPlusSampleTest {
//    @Autowired
//    private SysConfigMapper sysConfigMapper;
//
//    @Test
//    void testInsert() {
//        sysConfigMapper.selectList(null, resultContext -> {
//            System.out.println(resultContext.getResultObject());
//        });
//    }
//}
