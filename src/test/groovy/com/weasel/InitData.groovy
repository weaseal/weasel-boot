/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

//package com.weasel
//
//import org.dromara.hutool.core.bean.BeanUtil
//import org.dromara.hutool.core.util.RandomUtil
//import org.dromara.hutool.http.HttpUtil
//import com.weasel.modules.sys.entity.*
//import com.weasel.modules.sys.enums.DataScope
//import com.weasel.modules.sys.enums.MenuType
//import com.weasel.modules.sys.service.SysMenuService
//import com.weasel.modules.sys.service.SysRoleService
//import com.weasel.modules.sys.service.SysUserService
//import org.junit.jupiter.api.MethodOrderer
//import org.junit.jupiter.api.Order
//import org.junit.jupiter.api.Test
//import org.junit.jupiter.api.TestMethodOrder
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.boot.test.context.SpringBootTest
//
//import java.time.LocalDateTime
//
///**
// * @author weasel
// * @date 2022/3/29 15:32
// * @version 1.0
// */
//@SpringBootTest
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
//class InitData {
//
//    @Autowired
//    SysUserService sysUserService
//    @Autowired
//    SysRoleService sysRoleService
//    @Autowired
//    SysMenuService sysMenuService
//
//    @Order(1)
//    @Test
//    void menu() {
//
//        def menu = new SysMenu()
//        menu.with {
//            permission = null
//            type = MenuType.CATALOG
//            disabled = false
//            parentId = null
//            path = '/dashboard'
//            component = 'LAYOUT'
//            weight = 1
//            title = 'Dashboard'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:layers-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 2
//            permission = null
//            type = MenuType.CATALOG
//            disabled = false
//            parentId = null
//            path = '/sys'
//            component = 'LAYOUT'
//            weight = 2
//            title = '系统管理'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 21
//            permission = '/sys/config'
//            type = MenuType.MENU
//            disabled = false
//            parentId = 2
//            path = 'config'
//            component = '/sys/config/index'
//            weight = 1
//            title = '配置管理'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 211
//            permission = '/sys/config/{id}===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '详情'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 212
//            permission = '/sys/config===POST===TOOLBAR'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 2
//            title = '新增'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 213
//            permission = '/sys/config===PUT===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '编辑'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 214
//            permission = '/sys/config===DELETE===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '删除'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 22
//            permission = '/sys/dict'
//            type = MenuType.MENU
//            disabled = false
//            parentId = 2
//            path = 'dict'
//            component = '/sys/dict/index'
//            weight = 2
//            title = '字典管理'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 221
//            permission = '/sys/dict/{id}===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '详情'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 222
//            permission = '/sys/dict===POST===TOOLBAR'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 2
//            title = '新增'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 223
//            permission = '/sys/dict===PUT===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '编辑'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 224
//            permission = '/sys/dict===DELETE===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '删除'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 23
//            permission = '/sys/menu'
//            type = MenuType.MENU
//            disabled = false
//            parentId = 2
//            path = 'menu'
//            component = '/sys/menu/index'
//            weight = 3
//            title = '菜单管理'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 231
//            permission = '/sys/menu/{id}===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '详情'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 232
//            permission = '/sys/menu===POST===TOOLBAR'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 2
//            title = '新增'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 233
//            permission = '/sys/menu===PUT===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '编辑'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 234
//            permission = '/sys/menu===DELETE===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '删除'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 24
//            permission = '/sys/role'
//            type = MenuType.MENU
//            disabled = false
//            parentId = 2
//            path = 'role'
//            component = '/sys/role/index'
//            weight = 4
//            title = '角色管理'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 241
//            permission = '/sys/role/{id}===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '详情'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 242
//            permission = '/sys/role===POST===TOOLBAR'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 2
//            title = '新增'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 243
//            permission = '/sys/role===PUT===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '编辑'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 244
//            permission = '/sys/role===DELETE===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '删除'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 25
//            permission = '/sys/dept'
//            type = MenuType.MENU
//            disabled = false
//            parentId = 2
//            path = 'dept'
//            component = '/sys/dept/index'
//            weight = 5
//            title = '部门管理'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 251
//            permission = '/sys/dept/{id}===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '详情'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 252
//            permission = '/sys/dept===POST===TOOLBAR'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 2
//            title = '新增'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 253
//            permission = '/sys/dept===PUT===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '编辑'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 254
//            permission = '/sys/dept===DELETE===BATCH'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '删除'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 26
//            permission = '/sys/user'
//            type = MenuType.MENU
//            disabled = false
//            parentId = 2
//            path = 'user'
//            component = '/sys/user/index'
//            weight = 6
//            title = '用户管理'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 261
//            permission = '/sys/user/{id}===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '详情'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 262
//            permission = '/sys/user===POST===TOOLBAR'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 2
//            title = '新增'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 263
//            permission = '/sys/user===PUT===INLINE'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '编辑'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 264
//            permission = '/sys/user===DELETE===BATCH'
//            type = MenuType.BUTTON
//            disabled = false
//            parentId = 21
//            path = null
//            component = null
//            weight = 1
//            title = '删除'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//        menu = new SysMenu()
//        menu.with {
//            id = 3
//            permission = null
//            type = MenuType.CATALOG
//            disabled = false
//            parentId = null
//            path = '/dev'
//            component = 'LAYOUT'
//            weight = 2
//            title = '低代码'
//            dynamicLevel = 1
//            ignoreKeepAlive = true
//            icon = 'ion:settings-outline'
//            frameSrc = null
//            hideBreadcrumb = false
//            hideMenu = false
//            isLink = false
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/menu", BeanUtil.beanToMap(menu))
//
//    }
//
//    @Order(2)
//    @Test
//    void role() {
//
//        def role = new SysRole()
//        role.with {
//            id = 1
//            name = '超级管理员'
//            code = 'SUPER_ADMIN'
//            dataScope = DataScope.ALL
//            weight = 1
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/role", BeanUtil.beanToMap(role))
//
//        role = new SysRole()
//        role.with {
//            id = 2
//            name = '管理员'
//            code = 'admin'
//            dataScope = DataScope.ALL
//            weight = 2
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/role", BeanUtil.beanToMap(role))
//
//        role = new SysRole()
//        role.with {
//            id = 3
//            name = '普通用户'
//            code = 'user'
//            dataScope = DataScope.SELF
//            weight = 3
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/role", BeanUtil.beanToMap(role))
//    }
//
//    @Order(3)
//    @Test
//    void user() {
//        def user = new SysUser()
//        user.with {
//            id = 1
//            username = 'sa'
//            realName = 'sa'
//            password = '123456'
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/user", BeanUtil.beanToMap(user))
//
//        user = new SysUser()
//        user.with {
//            id = 2
//            username = 'admin'
//            realName = 'admin'
//            password = '123456'
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/user", BeanUtil.beanToMap(user))
//
//        for (i in 0..<100) {
//            user = new SysUser()
//            user.with {
//                username = RandomUtil.randomString(6)
//                realName = RandomUtil.randomString(6)
//                password = '123456'
//                description = null
//                deleted = false
//                version = 0
//                createTime = LocalDateTime.now()
//                createBy = 0
//            }
//            HttpUtil.post("http://localhost:3000/_d/sys/user", BeanUtil.beanToMap(user))
//        }
//    }
//
//    @Order(4)
//    @Test
//    void userRole() {
//        def userRole = new SysUserRole()
//        userRole.with {
//            userId = 1
//            roleId = 1
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/userRole", BeanUtil.beanToMap(userRole))
//
//        userRole = new SysUserRole()
//        userRole.with {
//            userId = 2
//            roleId = 2
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/userRole", BeanUtil.beanToMap(userRole))
//
//        def userIds = sysUserService.list()*.id - 1 - 2
//        def roleIds = sysRoleService.list()*.id - 1 - 2
//        for (i in 0..<100) {
//            userRole = new SysUserRole()
//            userRole.with {
//                userId = RandomUtil.randomEle(userIds, userIds.size())
//                roleId = RandomUtil.randomEle(roleIds, roleIds.size())
//                description = null
//                deleted = false
//                version = 0
//                createTime = LocalDateTime.now()
//                createBy = 0
//            }
//            HttpUtil.post("http://localhost:3000/_d/sys/userRole", BeanUtil.beanToMap(userRole))
//        }
//    }
//
//    @Order(5)
//    @Test
//    void roleMenu() {
//        def menuIds = sysMenuService.list()*.id - 21 - 211 - 212 - 213 - 214 - 22 - 221 - 222 - 223 - 224 - 3
//        def roleMenu = new SysRoleMenu()
//        menuIds.each { _menuId ->
//            roleMenu.with {
//                menuId = _menuId
//                roleId = 2
//                description = null
//                deleted = false
//                version = 0
//                createTime = LocalDateTime.now()
//                createBy = 0
//            }
//            HttpUtil.post("http://localhost:3000/_d/sys/roleMenu", BeanUtil.beanToMap(roleMenu))
//        }
//
//        def roleIds = sysRoleService.list()*.id - 1 - 2
//
//        for (i in 0..<100) {
//            roleMenu = new SysRoleMenu()
//            roleMenu.with {
//                menuId = RandomUtil.randomEle(menuIds, menuIds.size())
//                roleId = RandomUtil.randomEle(roleIds, roleIds.size())
//                description = null
//                deleted = false
//                version = 0
//                createTime = LocalDateTime.now()
//                createBy = 0
//            }
//            HttpUtil.post("http://localhost:3000/_d/sys/roleMenu", BeanUtil.beanToMap(roleMenu))
//        }
//    }
//
//    @Order(6)
//    @Test
//    void dept() {
//
//        def dept = new SysDept()
//        dept.with {
//            disabled = false
//            parentId = null
//            weight = 1
//            name = '部门1'
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/dept", BeanUtil.beanToMap(dept))
//
//        dept = new SysDept()
//        dept.with {
//            disabled = false
//            parentId = null
//            weight = 2
//            name = '部门2'
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/dept", BeanUtil.beanToMap(dept))
//
//        dept = new SysDept()
//        dept.with {
//            disabled = false
//            parentId = null
//            weight = 3
//            name = '部门3'
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/dept", BeanUtil.beanToMap(dept))
//
//        dept = new SysDept()
//        dept.with {
//            disabled = false
//            parentId = null
//            weight = 4
//            name = '部门4'
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/dept", BeanUtil.beanToMap(dept))
//
//        dept = new SysDept()
//        dept.with {
//            disabled = false
//            parentId = null
//            weight = 5
//            name = '部门5'
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/dept", BeanUtil.beanToMap(dept))
//
//        dept = new SysDept()
//        dept.with {
//            disabled = false
//            parentId = null
//            weight = 6
//            name = '部门6'
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/dept", BeanUtil.beanToMap(dept))
//
//        dept = new SysDept()
//        dept.with {
//            disabled = false
//            parentId = null
//            weight = 7
//            name = '部门7'
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/dept", BeanUtil.beanToMap(dept))
//
//        dept = new SysDept()
//        dept.with {
//            disabled = false
//            parentId = null
//            weight = 8
//            name = '部门8'
//            description = null
//            deleted = false
//            version = 0
//            createTime = LocalDateTime.now()
//            createBy = 0
//
//        }
//        HttpUtil.post("http://localhost:3000/_d/sys/dept", BeanUtil.beanToMap(dept))
//
//    }
//}
