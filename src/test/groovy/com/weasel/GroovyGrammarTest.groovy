/*
 * Copyright (c) 2023-present weasel
 *    weasel-boot is licensed under Mulan PSL v2.
 *    You can use this software according to the terms and conditions of the Mulan PSL v2.
 *    You may obtain a copy of Mulan PSL v2 at:
 *                http://license.coscl.org.cn/MulanPSL2
 *    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 *    See the Mulan PSL v2 for more details.
 */

package com.weasel

class GroovyGrammarTest {
    static void main(String[] args) {
        def start = '2023-11-01'
        def end = '2023-11-11'
        def devices
        def workdays = []
        def holidays = []
        def deviceIds = []
        def deviceId
        def deptId = 33

        if (deviceId) {
            deviceIds = deviceId
        } else if (deptId) {
            deviceIds = """
	select
		id
	from
		equiprise_device ed
	where
		ed.project_id in (
		select
			id
		from
			equiprise_project ep
		where
			id in (
			select
				dept_id
			from
				sys_dept sd
			where
				dept_id = ${deptId}
		union
			select
				dept_id
			from
				(
				select
					t1.dept_id,
					t1.parent_id,
					if(find_in_set(parent_id, @pids) > 0,
					@pids := concat(@pids, ',', dept_id),
					0) as ischild
				from
					(
					select
						dept_id,
						parent_id
					from
						sys_dept t
					where
						t.del_flag = '0'
					order by
						parent_id,
						dept_id
                  ) t1,
					(
					select
						@pids := ${deptId}) t2
             ) t3
			where
				ischild != 0
)
)
	"""
        }
        println deviceIds
    }
}
